FROM node:lts-alpine as base

FROM base as builder 
WORKDIR /app/website

COPY ./website /app/website
RUN yarn install

FROM base 
COPY --from=builder /app/website /app/website
EXPOSE 3000 35729
WORKDIR /app/website
ENTRYPOINT ["yarn", "serve"]
