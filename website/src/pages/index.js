import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Développement personnel',
    imageUrl: 'img/day95-app-development.png',
    description: (
      <>
        La capacité de développement informatique est une coupe qu'on remplit jour après jours. Pour palier au syndrôme de l'imposteur ?
      </>
    ),
  },
  {
    title: 'Centres d\'intérêts variés',
    imageUrl: 'img/112-installing.png',
    description: (
      <>
        En fonction du moment et de mes influences, les thématiques pourront s'avérer très variées et parfois entâchées d'amateurisme.
      </>
    ),
  },
  {
    title: 'L\'IT au service',
    imageUrl: 'img/111-coding.png',
    description: (
      <>
        La techno doit véhiculer les envies, mais ce qu'elle forge reste un outil, développe un service, et apporte de la valeur à l'humain.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3 className="text--center">{title}</h3>
      <p className="text--justify">{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/home')}>
              C'est parti 🔥
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
