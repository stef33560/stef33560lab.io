import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Développement personnel',
    Svg: require('@site/static/img/day95-app-development.png').default,
    description: (
      <>
        La capacité de développement informatique est une coupe qu'on remplit jour après jours. Pour palier au syndrôme de l'imposteur ?
      </>
    ),
  },
  {
    title: 'Centres d\'intérêts variés',
    Svg: require('@site/static/img/112-installing.png').default,
    description: (
      <>
        En fonction du moment et de mes influences, les thématiques pourront s'avérer très variées et parfois entâchées d'amateurisme.
      </>
    ),
  },
  {
    title: 'L\'IT au service',
    Svg: require('@site/static/img/111-coding.png').default,
    description: (
      <>
         La techno doit véhiculer les envies, mais ce qu'elle forge reste un outil, développe un service, et apporte de la valeur à l'humain.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
