---
title: Ansible
tags:
 - ansible
---

## Introduction

Ansible est un outil open-source de gestion de configuration, adossé simplement à Python et SSH, permettant d'automatiser :

- le provisionning des ressources d'une infrastructure
- la production et le stockage des artefacts (Continuous delivery)
- le déploiement des applications à partir des artefacts (Continuous Deployment)

![Ansible](https://upload.wikimedia.org/wikipedia/commons/0/05/Ansible_Logo.png 'Ansible')

Vieux de désormais [12 ans](https://fr.wikipedia.org/wiki/Ansible_(logiciel)), il s'agit d'une solution s'adossant à un processus d'authentification (principalement SSH mais aussi Kerberos) et à Python pour exécuter des séries de tâches de façon [indemptotente](https://fr.wikipedia.org/wiki/Idempotence). Ses [principes de design](https://github.com/ansible/ansible/blob/devel/README.rst#design-principles) que je retiendrais sont les suivants :

- Avoir un processus de configuration extrêmement simple avec une courbe d'apprentissage minimale.
- Le système d'automatisation informatique le plus facile à utiliser qui soit.
- Décrire l'infrastructure dans un langage à la fois convivial pour les machines et pour les humains.
- Gérez de nouvelles machines distantes instantanément, sans démarrer aucun logiciel.
- Être utilisable en tant que non root.

Ansible s'appuie sur quelques éléments clé :

- Control node : un système sur lequel Ansible est installé, à partir duquel les tâches sont lancées sur les systèmes cibles  
  Ce noeud est une machine très sensible car elle aura accès à l'ensembe du parc et des éventuels dépots.
- Managed node : un système cible, distant, sur lequel Ansible va exécuter des tâches  
  L'utilisateur unix utilisé devra être capable d'escalader ses privilèges pour installer les composants.
- Inventory : fichier INI organisé qui identifie les systèmes cibles, ainsi que les variables.
  Ces inventaires peuvent être statiques (fichier) ou dynamique (script), et regroupent des systèmes par fonctionnalités ('stack'). Il convient des variables qui pourront été avoir été définires globalement (`group_vars`) ou unitairement (`host_vars`).
- Rôle : fichiers YAML décrivant une liste de tâche regroupée par fonctionnalité.  
  Un rôle devra être versionné dans un repo GitLab.
- Playbook : ficher YAML décrivant une liste de tâches ou de rôles à exécuter  
  Il devra le moins possible de définir des variables, tâches ou conditions.
- Task : action unitaire atomique permettant de réaliser une (et une seule) action
- Module : fonctionnalité d'Ansible utilisée par les playbooks ou les rôles (ex: manipuler une base MariaDB)[^1]
- Template : extrait de fichier de configuration modèle qui sera repris par le moteur [Jinja](https://fr.wikipedia.org/wiki/Jinja_(moteur_de_template)) d'Ansible pour être décliné sur un système cible

Il s'agit donc d'une surcouche qui fonctionne de manière très simple, masquant la complexité d'adaptation d'un OS à l'autre ; il convient toutefois pour le concepteur de playbook de bien maitriser les composants qu'il déploie.

## Premiers pas

### Installation

Bien que le control node puisse adresser n'importe quel OS, il ne peut être que sur un OS Linux ; utilisez donc votre gestionnaire de paquet préféré, dans mon cas `apt`:

```bash
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

Rien de plus agréble que d'avoir une syntaxe reconnue sous un shell ; donc à l'aide de [`pip`](https://pip.pypa.io/en/stable/installation/)

```bash
sudo apt install python3-pip
pip3 install argcomplete --user
pip3 install ansible --user
```

Test de Hello World assuré par `ansible all -i "localhost," -c local -m shell -a 'echo Hello World !'`

Tel qu'installé, ansible ne contient que les [programmes de base](#principales-commandes) et le noyau de modules/plugin strictement essentiel à son fonctionnement minimal. Les fonctions sont assurées par des modules, appelés **collections**. 

### Principales commandes

A retenir :

- `ansible` : Permet d'exécuter un simple module ansible sur un inventaire. 
- `ansible-console` : Exécute une console en mode interactif (utile pour lancer plusieurs actions sur un inventaire).
- `ansible-playbook` : Exécute un playbook Ansible sur un inventaire. 
- `ansible-vault` : Permet de chiffrer des données (ex: mots de passe).
- `ansible-inventory` : Affiche l’ensemble des données d’un inventaire Ansible, y compris les données chiffrées avec Vault.
- `ansible-galaxy` : permet d’installer des roles et des collections Ansible
- `ansible-doc` : version CLI de la documentation des collections

### Configuration

Ces commandes s'appuient sur un fichier de configuration, consultable par `ansible --version` qui vous donnera entre autres le chemein vers le fichier de config de base. Il peut être défini :

- à l'endroit défini dans la variable `ANSIBLE_CONFIG` (surcharge spécifique)
- dans le dossier du playbook (spécifique à un run de playbook)
- dans `~/.ansible/ansible.cfg` 
- dans `/etc/ansible/ansible.cfg`

L'ensemble des paramètres est globalement par défaut désactivé, comme par exemple l'emplacement des modules, du cache et d'autres relatifs à l'élévation de privilège et les performances.

`ansible-config` nous aidera de manipuler cette configuration, `ansible-config --list` nous permettra par exemple de voir le paramétrage appliqué à l'endoit où le playbook va être lancer. Quelques options interessantes :

- section `defaults` > `host_key_checking = False` : Si vous souhaitez qu'ansible ne vérifie pas le [fingerprint](https://docs.ansible.com/ansible/2.4/intro_getting_started.html#host-key-checking) sur la première connection SSH, 
- section `defaults` > `callback_whitelist = profile_tasks` : ce paramètre [donne](https://docs.ansible.com/ansible/2.4/intro_configuration.html#callback-whitelist) le [temps d'exécution](https://docs.ansible.com/ansible/latest/collections/ansible/posix/profile_tasks_callback.html)
- section `ssh_connection` > `pipelining = True` : le [pipelining](https://docs.ansible.com/ansible/2.4/intro_configuration.html#pipelining) permet d'éviter de devoir envoyer le fichier à exécuter par SSH, mais l'interpréter à distance
- section `ssh_connection` > `ssh_args =` permet de passer [des paramètres](https://docs.ansible.com/ansible/2.4/intro_configuration.html#openssh-specific-settings) à SSH ; exemples :
  - `-o ControlMaster=auto -o ControlPersist=60s` : permet de mettre en place une connection SSH permanente pendant 60s (mysql_pconnect pour les amateurs PHP 🤗)
  - `-o PreferredAuthentications=publickey` : permet de ne pas demander à SSH de déterminer comment se connecter mais forcer l'usage de la clé
- section `defaults` > `forks = 30` : permet de paralléliser jusqu'à 30 serveurs. Cette variable est à adapter en fonction de vos besoins.
- section `defaults` > le [fact checking](https://docs.ansible.com/ansible/2.4/intro_configuration.html#fact-caching) premet de collecter des `gather_facts` permettent de remonter les variables décrivant le serveurs (OS, heure, etc.) aussi il n'est pas toujours très pertinent des les désactiver mais à défaut ils peuvent être mis en cache pendant un temps déterminé : 

>fact_caching = jsonfile
fact_caching_timeout = 3600
fact_caching_connection = /tmp/ansible/cache/gather

Tous ces paramètres peuvent être gérés manuellement, mais aussi de façon très simple mais plus "boite noire" à l'aide de l'outil [Mitogen](https://mitogen.networkgenomics.com/ansible_detailed.html).

A noter : Ansible fonctionne aussi en `pull`, et pas en `push` comme nous venons le voir ; En l'installant sur un serveur, Ansible va alors récupérer un dépot GIT pour exécuter une série de tâche (utile pour installer un environnement, nous le verrons plus tard).

## Premier Lancement CLI

Nous allons prendre comme illustration la construction de mon Rikiki-homelab ; je pars du postulat que les 3 RPIs sont installés sur la base de raspbian et avec le démon SSH opérationnel, que leur IP est connue d'une manière ou d'une autre, et que la clé SSH définie ne soit autorisée que depuis le node control sans transfert de X (directive `from:'<nodecentral>', no-X11-forwarding <clé publique>` dans `.ssh/authorized_keys`).

```mermaid
graph TD
    A[fa:fa-bullhorn RPI 3B+ r1.3] -->|SSH| B[fa:fa-server RPI 1B Rev 2]
    A -->|SSH| C[fa:fa-server RPI 2B r1.1]
```

L'inventaire doit donc être à ce stade le suivant :

```ini title='ansible/premier-pas/inventory/homelab/rpis.yml
[rpis]
rpi[2:3]
```

Les RPI sont bien accessibles, vérifiable par l'utilisation du module [ping](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/ping_module.html) lancée sur mon node control (le RPI1) :

```bash
ansible all -i inventory/homelab/rpis.yml -m ping -v
```

>rpi3 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
rpi2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}

Concernant les <a name="options">options</a> :

- `-i` premet de préciser l'inventaire des machines visées  
  Nous y plongerons plus en détail dans [une section dédiée](./inventaire.md)
- `-m` permet d'appeler des modules, en l'occurence `ping`  
   Cette option permet d'appeler [tous les modules](https://docs.ansible.com/ansible/2.9/modules/list_of_all_modules.html) à l'instar de ceux qui pourront être utilisés plus tard dans nos playbooks ; exemples :
  - [shell](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html) permet d'exécuter un shell  
    Exemple : lister les paquets python installés, `-m shell -a  "dpkg -l python*|grep ^i|sed -r 's/ii\s*([a-z0-9\.-]*)\s*[0-9].*/\1/'"` 
  - [raw](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/raw_module.html) permet d'exécuter une commande  
    Exemple : installer python via shell `-m raw -a "apt install python"`
  - [shell](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html) permet d'exécuter d'installer par apt un paquet 
    Exemple : installer python `-m apt -a "name=python"`
  - [copy](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html) permet de copier un fichier
    Exemple : installer python `-m copy -a  "src=/tmp/fichier dest=/tmp/destination"`
  - [fetch](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/fetch_module.html) permet de récupérer un fichier
    Exemple : installer python `-m copy -a  "src=/tmp/destination dest=/tmp/fichier-recupéré"`
- `-v` permettra de débugger le fonctionnement de Ansible ; en cas de besoin, on peut augmenter le niveau en augmentant le nombre de lettres v (`-vv` voir `-vvv`)

La connection SSH se réalise ici avec le compte employé depuis le node central ; en cas de besoin, un `-u <user>` peut être employé pour spécifier avec quel user se connecter (et ainsi avoir les bonnes élévations de privilèges). A ce sujet, `-k` pourra permettre de passer le mot de passe pour se connecter ou `--key-file` pour la clé SSH.

Il est possible de demander explicitement de faire un sudo ('Become') avec `-b`, à associer le cas échéant avec un `-K` pour le mot de passe du sudo.

A noter :

- `--check` permet de faire un dry-run
- `--diff` met en exergue les modifications réalisées

Si d'aventure des fichiers chiffrés devaient être employés, `ask-vault-pass` ou `--vault-password-file` vont permettre de récupérer le mot de passe de déchiffrement

## Notes

[^1] Liste exhaustive de modules ansibles disponible [ici](https://docs.ansible.com/ansible/latest/collections/index_module.html)


