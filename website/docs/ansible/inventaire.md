---
title: L'inventaire
tags:
 - ansible
 - inventaire
 - inventory
---
L'inventaire statique est un fichier recensant les machines (hosts) regroupés par typologie (ex: frontaux web) flanqué de dossiers contenant des variables. Cet inventaire va être utilisé par ansible pour définir la cible sur laquelle les tâches ou les rôles vont être exécutées.

## Fichier d'inventaire

Ce fichier présente en sa racine un groupe `all` qui contient des hôtes ou des groupes. Les groupes pourront être hierarchisés (relation parent/enfants).
Prenons comme exemple ce groupe de serveurs : un serveur isolé, deux frontaux web et deux bases de données.

```mermaid
graph TD
    Z[all] --> A[lonely]
    Z --> B[fa:fa-server webservers]
    Z --> C[fa:fa-database dbservers]
    B --> srv1
    B --> srv2    
    C --> srv3
    C --> srv4
```

Sa traduction <a name="initiale">initiale</a> est la suivante :

```yaml title='./env/dev/inventory.yml'
all:
  hosts:
    lonely.docker.lan:
  children:
    web.docker.lans:
      hosts:
        srv1.docker.lan:
        srv2.docker.lan:
    db.docker.lans:
      hosts:
        srv[3:4].docker.lan:
```

Notes :

- Il est possible d'utiliser des expressions régulières pour définir les systèmes cibles
- Un hote pourra être présent dans plusieurs groupes
- Ce fichier est ici au format YAML, mais pourra être aussi au format INI ou JSON si vous en éprouvez le besoin, l'envie ou la folie 😊.

## Arborescence

Le fichier inventaire est déposé dans dossier qui contient une arborescence dont le nommage devra être strictement respecté tel que l'attends Ansible, au travers de deux dossiers : `host_vars` et `group_vars`. L'arborescence de ce dossier est le suivant :

```mermaid
flowchart TB
%% Colors %%
classDef blue fill:#66deff,stroke:#000,color:#000
classDef red fill:#f88,stroke:#000,color:#000

root(./) --> inventory_dev(fa:fa-file inventory.yml):::blue
root ---> group_vars_dev(fa:fa-folder group_vars)
root ---> host_vars_dev(fa:fa-folder host_vars)

group_vars_dev --> all_dev(fa:fa-file all.yml):::blue
group_vars_dev --> dbservers_dev(fa:fa-file dbservers.yml):::blue
group_vars_dev --> group_vars_dev_web(fa:fa-folder webservers)
group_vars_dev_web --> web1_dev(fa:fa-file web1.yml):::blue
group_vars_dev_web --> vault_dev(fa:fa-lock vault.yml):::red

host_vars_dev--> web2_dev(fa:fa-folder web2)
web2_dev --> web2_dev_yml(fa:fa-file web2.yml):::blue
host_vars_dev --> db_dev_yml(fa:fa-file db2.yml):::blue
```

A coté du fichier d'inventaire, nous retrouvons deux dossiers relatifs aux spécificités des groupes (`group_vars`) ou de serveurs (`host_vars`). A l'intérieur, un ensemble de fichier YAML viennent adjoindre une configuration ou des mots de passes à utiliser (`vault`, nous verrons cela plus tard).

Ce squelette pourra être répliqué afin d'être rendu à l'identique pour chaque type d'environnement :
```mermaid
flowchart TB
%% Colors %%
classDef blue fill:#66deff,stroke:#000,color:#000
classDef yellow fill:yellow,stroke:#000,color:#000
classDef green fill:#6ad98b,stroke:#000,color:#000
classDef red fill:#f88,stroke:#000,color:#000

root(./env) --> dev
root --> prod

dev(dev):::green ---> inventory_dev(fa:fa-file inventory.yml):::green 
dev ---> group_vars_dev(fa:fa-folder group_vars):::green 
dev ---> host_vars_dev(fa:fa-folder host_vars):::green 

group_vars_dev --> all_dev(fa:fa-file all.yml):::blue
group_vars_dev --> dbservers_dev(fa:fa-file dbservers.yml):::blue
group_vars_dev --> group_vars_dev_web(fa:fa-folder webservers):::green 
group_vars_dev_web --> web1_dev(fa:fa-file web1.yml):::blue
group_vars_dev_web --> vault_dev(fa:fa-lock vault.yml):::red

host_vars_dev--> web2_dev(fa:fa-folder web2):::green 
web2_dev --> web2_dev_yml(fa:fa-file web2.yml):::blue
host_vars_dev --> db_dev_yml(fa:fa-file db2.yml):::blue


prod(prod):::yellow ---> inventory_prod(fa:fa-file inventory.yml):::yellow
prod ---> group_vars_prod(fa:fa-folder group_vars):::yellow
prod ---> host_vars_prod(fa:fa-folder host_vars):::yellow

group_vars_prod --> all_prod(fa:fa-file all.yml):::blue
group_vars_prod --> dbservers_prod(fa:fa-file dbservers.yml):::blue
group_vars_prod --> group_vars_prod_web(fa:fa-folder webservers):::yellow
group_vars_prod_web --> web1_prod(fa:fa-file web10.yml):::blue
group_vars_prod_web --> vault_prod(fa:fa-lock vault.yml):::red

host_vars_prod--> web2_prod(fa:fa-folder web20):::yellow
web2_prod --> web2_prod_yml(fa:fa-file web20.yml):::blue
host_vars_prod --> db_prod_yml(fa:fa-file db20.yml):::blue
```

## Variables

Des variables peuvent être positionnées à différents niveaux.

### Au niveau de l'inventaire

[Dans le fichier inventaire](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#adding-variables-to-inventory), en reprenant le YAML précédent, les variables vont se définir directement sous leur nom quand elles sont appliquées à un host, ou bien à l'aide du mot clé `vars` lorsqu'elles sont associées à un groupe. Une même variable peut être définie à différents niveaux ; exemple dans notre dossier `./env/dev-vars` :

```yaml title='./env/dev-vars/inventory-vars.yml'
all:
  hosts:
    lonely.docker.lan:
  children:
    webservers:
      hosts:
        srv1.docker.lan:
        srv2.docker.lan:
          maVariable: "webserver SRV2"
      vars:
        maVariable: "webserver"
    dbservers:
      hosts:
        srv[3:4].docker.lan:
      vars:
        maVariable: "dbservers"
```

:::danger Définition des variables
Mieux vaut définir les variables au travers de l'arborescence décrite [plus haut](#arborescence), `host_var` et `group_vars`
:::

### Dans les dossiers reconnus (méthode recommandée)

Nous allons pour cela rependre dans un dossier `./env/dev` [notre inventaire initial](#initiale) et rajouter quelques fichiers :

```yaml title='./env/dev/group_vars/dbservers/variables.yml'
maVariable: "dbservers"
```

```yaml title='./env/dev/group_vars/webservers.yml'
maVariable: "webserver"
```

```yaml title='./env/dev/host_vars/srv2.docker.lan.yml'
maVariable: "webserver SRV2"
```

Pour observer le comportement de cet inventaire, je vais avoir besoin de simuler ces 4 serveurs. Pour cela je vais utiliser l'image [inuxserver/openssh-server:latest](https://docs.linuxserver.io/images/docker-openssh-server)([sources](https://hub.docker.com/r/linuxserver/openssh-server)). J'ai juste pris soin de d'[y rajouter python3](https://github.com/linuxserver/docker-openssh-server/blob/e8332c3062fc3bc77d3b4c66762a83fd89fd1bf4/Dockerfile#L13) et modifier [le port d'écoute](https://github.com/linuxserver/docker-openssh-server/blob/ce455b639cf51cd022dab0e7b2fbf467a8c4cb38/root/etc/s6-overlay/s6-rc.d/svc-openssh-server/run#L6) sur le port 22. De cette façon, sans binder le port sur l'hôte, je pourrais sans [option particulière](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#connecting-to-hosts-behavioral-inventory-parameters) me connecter sur le conteneur ; cette image modifiée est disponible [ici sur DockerHub](https://hub.docker.com/repository/docker/stef33560/openssh-server).

[Un petit script](./inventaire/dockerssh_up.sh) plus loin, mes targets sont up ! je vais juste positionner la variable `ANSIBLE_HOST_KEY_CHECKING` à `False` (car mes conteneurs sont très volatiles et j'aimerai que Ansible évite de vérifier leur fingerprints) avant d'exécuter <a name="ansible_cli">ansible</a>.

```bash
export ANSIBLE_HOST_KEY_CHECKING=False
ansible -i env/dev-vars/inventory-vars.yml -m debug -a 'msg={{maVariable}}' all
ansible -i env/dev/inventory.yml -m debug -a 'msg={{maVariable}}' all
```

>lonely.docker.lan | FAILED! => {
    "msg": "The task includes an option with an undefined variable. The error was: 'maVariable' is undefined. 'maVariable' is undefined"
}
srv1.docker.lan | SUCCESS => {
    "msg": "webserver"
}
srv2.docker.lan | SUCCESS => {
    "msg": "webserver SRV2"
}
srv3.docker.lan | SUCCESS => {
    "msg": "dbservers"
}
srv4.docker.lan | SUCCESS => {
    "msg": "dbservers"
}

Dans ce cas, nous constatons que :

- `maVariable` n'existe pas pour le serveur `lonely.docker.lan`  
N'ayant aucune définition de variable de près ou de loin pour ce serveur, difficile en effet d'avoir une sortie ! En effet, ce serveur n'appartient pas non plus aux groupes que nous avons défini : il appartient à `all`. Qu'à cela ne tienne, rajoutons un dossier `all` dans le `group_vars` !

```yaml title='./env/dev/group_vars/all/variables.yml'
maVariable: "lonely youuuu"
```

- `maVariable` a été mergée selon l'ordre suivant des groupes/hôtes :

```mermaid
flowchart LR
all --> parent --> child --> host --> root(ligne de commande)
```

En effet, si nous avions précisé un `-e "maVariable=CékiLPatron"`, tous les serveurs auraient vu la variable surchargée par la valeur alors définie dans la ligne de commande.

:::tip Arborescence des environnements
Il est impératif de respecter l'arborescence de définition des variables et des invenatires [prévu dans Ansible](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#directory-layout). C'est un gage de qualité, de maintenabilité, mais aussi de capacité à onboarder plus facilement de nouveaux collaborateurs de par la standardisation ainsi apportée.
:::

## Vous êtes ici

Si d'aventure vous vous retrouveriez perdu dans votre propre arborescence, `ansible-inventory` pourra vous aider à la debugger.

Pour moi deux commandes à retenir :

- Liste arborescente avec les valeurs des variables : `ansible-inventory -i env/dev/inventory.yml --graph --vars`
- Liste JSON avec les valeurs des variables : `ansible-inventory -i env/dev/inventory.yml --list --yaml`
