---
title: Le playbook
tags:
 - ansible
 - ansible-playbook
---

## Introduction

Le playbook est un fichier qui déclenche une série d'actions (`tasks`) qui seront réalisées sur des cibles définies dans l'inventaire. Pour l'instant, nous commencerons pas une approche basique mais nous verrons plus tard que ces actions pourront, et devront même plutôt, être piochées dans des collections appelés `roles`.

:::tip Ségrégation des actions
Les actions doivent généralement être implémentées dans des rôles pour une meilleure réutilisabilité
:::

`ansible-playbook` présente globalement les même options déjà vues sur le CLI [`ansible`](./ansible.md#options) en introduction (-b, -k, -K, etc.), et s'applique donc sur un inventaire (`-i`). Le playbook suivant ([dispo ici](playbook/premierbook.yml)) donnera le résultat équivalent à la [commande initiale](./inventaire.md#ansible_cli) ; sur tous les hôtes (`hosts: all`), nous actionnons la tache nommée `DebuggeMoiLeYameul` qui consiste à utiliser le module `debug` pour affichier la variable `maVariable` sur [notre précédent inventaire](playbook/env/dev/inventory.yml) :

```playbook title='premierbook.yml'
- name: "Play Book De Bug"
  hosts: all
  
  tasks:
  - name: DebuggeMoiLeYameul
    debug:
      msg: "{{ maVariable }}"
```

On lance le playbook :

```bash
ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook -i ./env/dev/inventory.yml premierbook.yml 
```

Qui nous réponds in fine :

>ok: [lonely.docker.lan] => {
    "msg": "lonely youuuu"
}
ok: [srv1.docker.lan] => {
    "msg": "webservers"
}
ok: [srv2.docker.lan] => {
    "msg": "webserver SRV2"
}
ok: [srv3.docker.lan] => {
    "msg": "dbservers"
}
ok: [srv4.docker.lan] => {
    "msg": "dbservers"
}


En terme d'options à retenir pour lancer le playbook, nous pourrons notamment retenir les suivantes :

- `--list-tasks` pour lister les tasks retenues à l'exécution
- `--step` pour dérouler le playbook task par task
- n'exécuter que certaines actions, en filtrant :
  - par groupes/serveurs ciblés (`-l` compatible avec les regexp).
  - par tag sur les actions identifiées par un mot clé particulier (`-t`)

## Quelques modules

### Module package

Des packages, il y en a de tous poils : apt, dpkg, yum, dnf, apk. Il y en donc autant de modules [apt](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#ansible-collections-ansible-builtin-apt-module), [dpkg](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dpkg_selections_module.html#ansible-collections-ansible-builtin-dpkg-selections-module), [yum](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html), [dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)...

Or si vous êtes amenés à créer des playbooks pour plusieurs OS, c'est loin d'être idéal et c'est là qu'intervient le module [Generic OS Package Manager](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html). Ce module gère les packages sur une cible en permettant de ne pas se soucier (ou presque) du gestionnaire de packages sous-jacent en faisant office de "proxy" vers le gestionnaire de packages légitime de l'OS visé. Quand je dis presque, c'est que si l'empaqueteur est proxyfié, ce n'est pas le cas des noms des modules qui peuvent très largement différer d'un OS à un autre ; il conviendra alors simplement de déterminer les modules dans une variable passée à ce proxy. 

Illustrons un exemple autour de RHEL 8 et Debian 11. Deux fichiers [`vars/RedHat-8.yml`](playbook/vars/RedHat-8.yml) et [`vars/Debian-11.yml`](playbook/vars/RedHat-8.yml) sont créés pour définir les noms de paquets selon chaque OS. [Le playbook](playbook/install-apache.yml) consistera en deux tâches :

- à l'aide des facts récupérés par Ansible au moment du lancement, importer le fichier [en fonction de l'OS détecté](https://docs.ansible.com/ansible/2.6/user_guide/playbooks_conditionals.html#ansible-os-family)
- utiliser le module generic package pour lancer l'install

```yaml title='vars/RedHat-8.yml'
apachephp_packages:
  - httpd
  - php81-php-fpm
```

```yaml title='vars/Debian-11.yml'
apachephp_packages:
  - apache2
  - libapache2-mod-php8.1
```

```yaml title='install-apache.yml'
- name: "Apache PHP install"
  hosts: lonely.docker.lan
  
  tasks:
    - name: Include OS-specific variables
      include_vars: "vars/{{ ansible_os_family }}-{{ ansible_distribution_version.split('.')[0] }}.yml"

    - name: Install Apache & PHP packages 
      ansible.builtin.package:
        name: "{{ apachephp_packages }}"
        state: present
      become: true
```

### Module file

Ce module [file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html) permet de manipuler les fichiers, dossiers et liens symboliques soft ou hard tant au niveau de leurs attributs ([sticky bit](https://fr.wikipedia.org/wiki/Permissions_UNIX#Sticky_Bit) & co), de leurs propriétaires (`chown`) ou du mode (`chmod`), ou de leur état.

[Un exemple](playbook/demo_mod_files.yml) de création de dossier `/tmp/demofiles` appartenant à `root` :

```yaml title='demo_mod_files.yml'
- name: "Démo Files"
  hosts: lonely.docker.lan
  
  tasks:
    - name: FolderCreate1
      file:
        path: "/tmp/demofiles"
        state: directory # un dossier doit être présent
        owner: root
        mode: 0755
      become: yes # élévation de privilèges
      tags: folder

    - name: FileCreate
      file:
        path: "/tmp/demofiles/fichier"
        state: touch
      become: yes # élévation de privilèges
      tags: file

    - name: FolderCreate2
      file:
        path: "/tmp/demofiles2/3/4"
        recurse: yes # équivalent mkdir -p
        state: directory 
      tags: folder

    - name: Symlink
      file:
        src: "/tmp/demofiles/fichier"w  
        dest: "/tmp/demofiles2/3/4/lien"
        state: link ## equivalent de ln -s
      tags: symlink

    - name: FileDelete
      file:
        path: "/tmp/demofiles/fichier"
        state: absent # supprime le fichier
      become: yes 
      tags: file
```

Nous pourrons noter que nous devons forcer l'élévation pour attribuer à root ce folder de ce fait si l'utilisateur employé ansible pour exécuter le playbook n'est pas sudoer [`NOPASSWD`](https://doc.ubuntu-fr.org/sudoers#executer_des_taches_d_administration_sans_mot_de_passe), ansible nous retournera un joli petit `"msg": "Missing sudo password"}` ; il sera donc nécessaire dans ce cas d'utiliser l'option -K pour demander le mot de passe sudoer.

:::danger NOPASSWD, at one's own risk
Faites extrêmement attention lorsque vous autorisez un utilisateur ou un groupe NOPASSWD. Ceci pourrait causer des brèches de sécurité si les commandes autorisées sont potentiellement dangereuses.
:::

En terme d'[idempotence](https://fr.wikipedia.org/wiki/Idempotence), si nous rejouons plusieurs fois ce playbook, 2 tâches seront réalisées (`changed`): FileCreate et FileDelete. 

>lonely.docker.lan          : ok=6    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

Ansible ne connait en effet pas l'état précédent de l'environnment aussi il va vérifier l'état actuel pour exécuter (ou non) la tâche  ; il est dit stateless.
