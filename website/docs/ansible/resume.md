---
title: Best practices
tags:
 -  ansible
 -  bonnes pratiques
---

Un résumé de ce qui s'est dit sur les autres pages :
## YAML
-  Débuter le fichier avec `---` sur la première ligne et `...` sur la dernière ligne.
-  Deux espaces pour l’indentation et des espaces globalement cohérents (siouplé, c'est déjà suffisament source d'erreur)
-  Un linter utilisé pour vérifier le code écrit([ansible-lint](https://marketplace.visualstudio.com/items?itemName=redhat.ansible) ou [spotter](https://marketplace.visualstudio.com/items?itemName=xlab-steampunk.steampunk-spotter).
 -  Des nommages cohérents pour tout (variables, playbooks, taches, rôles et modules).
 -  Une même extension partout (`.yml` et pas autre chose)

## Variables
-  Une valeur par défaut toujours définie (ajout de `| default('valeur')`)
-  Les variables utilisent une convention ([Camel case](https://fr.wikipedia.org/wiki/Camel_case), [Snake case](https://fr.wikipedia.org/wiki/Snake_case), mais une convention !).
-  Les variables en la ligne de commande débutent par `cli_`.
-  Les variables globales débutent par `g_`.
-  Les variables globales des rôles sont préfixées par le nom du rôle
-  Les variables pouvant être modifié sont placé dans `default/main.yml`, les autres dans `vars/main.yml`
-  toutes les variables se trouvant dans `vars/main.yml` commencent par `_`
-  Les noms de variables Jinja sont encadrés par des espaces `{{ variable }}`.
-  L’inventaire ne contient que des variables globales propre à l’infrastructure, les autres sont au niveau du playbook (`ansible-inventory --list` est ton ami)

## Inventaires
-  Des inventaires séparés pour chaque environnement
-  Un regroupement dynamique utilisé au besoin (module `group_by`).
-  Des groupes d’inventaires propres bien séparés

## Tâches et rôles
-  Ajouter un commentaire de contexte au besoin.
-  KISS. Encore KISS. Toujours KISS. 
-  Des tâches bien séparées pour améliorer la lisibilité (laisser des lignes vierges n'est pas interdit !).
-  Des rôles factorisés (un rôle = objectif) 
-  Des modules génériques employés, ceux spécifiques comme shell & co bannis autant que possible (assurer pas l'indépotence)
-  Un enchainement des tâches assurés avec des `when` ou des notifiers (`notify: TacheX`) permettant d'appeler des tâches définies en handlers

## Sécurité
-  Vaulter tout ce qui doit l'être (passwords, token d'API, clés diverses et variées)
-  Mutualiser les rôles dans un même référentiel et les rendre au plus génériques
-  Rôles disponibles "sur étagère" : les lire ET les comprendre AVANT de les réutiliser (`--list-hosts`, `--list-tasks` et `--check` ne sont pas tes ennemis)
