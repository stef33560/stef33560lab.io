#!/bin/bash
usage() {
  cat<<EOF
Usage: $(basename "$0") nbServers cléPublique [userName] [suffixeFQDN]

Options:
 - nbServers      Le nombre de serveurs à instancier
 - cléPublique    La clé publique SSH autorisée
 - userName       Le nom d'utilisateur employé pour se connecter, par défaut celui qui lance le script
 - suffixeFQDN    Le suffixe du nom du serveur, par défaut .docker.lan
EOF
  exit 1
}

SUFFIX=".docker.lan"
if ! [ -z "$4" ]
then
      SUFFIX="$4"
fi

USERNAME=$(whoami)
if ! [ -z "$3" ]
then
      USERNAME="$3"
fi

if [ ! -f "$2" ]
then
    echo "Erreur : La clé publique $2 n'existe pas\n"
    usage
fi

if ! [ "$1" -eq "$1" ]]
then
  echo "Erreur : le nombre de serveur doit être un nombre\n"
  usage
fi


MAX=4
HOSTS=""
echo "Setting up $MAX servers" 
for SRV in $(seq 1 $MAX); do
  docker run -d --name=srv$SRV -e PUID=1000 -e PGID=1000 -e TZ=Europe/London -v $2:/key.pub -e PUBLIC_KEY_FILE=/key.pub -e SUDO_ACCESS=false -e USER_NAME=steph stef33560/openssh-server:latest
  SRV_IP="$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' srv$SRV)"
  HOSTS="$HOSTS\n$SRV_IP srv$SRV$SUFFIX"
done
echo "Serveurs SSH créés : $HOSTS"
