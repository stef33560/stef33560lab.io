---
title: Sécurité et Paramétrage
tags:
 - docker
 - sécurité
 - security
 - user
 - namespace
 - docker bench for security
---

Se baser sur la dernière version disponible fait le plus souvent sens, de façon à récupérer les derniers hotfix et se protéger des failles connues. Mais récupérer une image docker est aussi aisé que d'en publier une, il suffit de [créer un repository associé votre compte](https://hub.docker.com/repositories) et y pousser votre image :

```bash
docker tag local-image:tagname new-repo:tagname
docker push new-repo:tagname
```

Cela signifie par extension que n'importe qui peut donc aussi pousser une image forkée d'une autre en y rajoutant n'importe quoi :) vous devez donc être très attentif à l'image que vous récupérez et à cet effet DockerHub a d'ailleurs le moyen de filtrer les images sur des "Trusted Content".

![Filtres Dockerhub](./securite/DockerHub-ExploreFilters.png 'Filtres Dockerhub')

Docker met aussi des outils de scan de vulnérabilité online, et permet aussi d'upgrader le binaire pour ajouter [la commande scan](https://github.com/docker/scan-cli-plugin). Mais cela ne fait pas tout, à l'image de la [faille de sécu chez SolarWinds](https://www.techtarget.com/whatis/feature/SolarWinds-hack-explained-Everything-you-need-to-know) ou encore de ce qui aurait pu se passer chez [Microsoft](https://www.usine-digitale.fr/article/les-hackers-de-solarwinds-ont-accede-au-code-source-de-microsoft-azure-intune-et-exchange.N1062819). L'injection de code malveillant est une pratique répandue et [surveillée](https://unit42.paloaltonetworks.com/cryptojacking-docker-images-for-mining-monero/) et il convient de [l'avoir en tête](https://medium.com/@cyberprotect/malicious-dockerfiles-3c4410c1fed2) avant tout pull d'image et toujours s'interroger sur la supply chain logicielle qui a construit l'image ; l'ajout d'une clé privée dans la liste des clés de root est un exemple assez savoureux...

[![sysdig research team scenario](./securite/Triaging_malicious_docker.png 'sysdig research team scenario')](https://sysdig.com/blog/triaging-malicious-docker-container/)

##  📣 La base

Quelques règles de bonne conduite auxquelles déjà se référer :

:::tip Règles de sécurité

1. Vérifier la signature des images
1. Trouver, corriger et suivre les vulnérabilités
1. Chercher les labels dans les métadatas
1. Se référer au Dockerfile
1. Surveiller l'usage ADD (plutôt que COPY)
:::

A l'instar des bonnes vielles empreintes MD5, utiliser une image officielle ou *correctement* signée, c'est déjà mieux ; Typiquement on s'assure un peu plus par ce biais de ce qu'on déploie même si cela ne nous assure pas à 100% d'être protégé d'attaques type [Man In The Middle](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu). Docker propose [Notary](https://docs.docker.com/engine/security/trust/#signing-images-with-docker-content-trust) qu'il met naturellement en avant sur DockerHub.

Concernant l'analyse des images, nous verrons [les outillages](./outillage.md) comme [trivy](https://github.com/aquasecurity/trivy), [Grype](https://github.com/anchore/grype) ou [syft](https://github.com/anchore/syft) permettant de vérifier les images avant de les instancier.

Je l'ai [déjà évoqué](./fichierdockerfile.md#taggage), les labels fournissent des informations utiles, notamment des détails de securité, au travers d'une policy stockée dans SECURITY.TXT. Ca vaut le coup d'y jeter un oeil si elles sont présentes.

In fine, le huge de paix, c'est le Dockerfile. Ce fichier permet aussi

- de comprendre la façon dont a été construite l'image.  
- de s'assurer de l'utilisation d'un utilisateur non privilégié : créer un user et un group sur l'image, avec des persomissions stricement utiles pour faire tourner l'application, fonctionnant dans le même pid. Exemple typique : un user node pour faire tourner NodeJS, pas root !  

Note :  Associé à un linter type [Hasker Dockerfile Linter](https://github.com/hadolint/hadolint), les bonnes pratiques de rédaction seront respectées (voir la page [outillage](./outillage.md)).

Cela n'empêche pas quand même d'être un peu parano car rien ne nous assure que l'image réupérée correspond bien au Dockerfile que vous avez validé : cf supra, il est donc indispensable de d'analyser les couches.

Des URLs arbitraires peuvent aussi être spécifiées par l'instruction `ADD`, et injecter du code externe pour en faire resulter des attaques MITM ou pire, un cryptovirus. De surcroit `ADD` décompacte implicitement les archives locales à des endroits pas toujours souahité ([path traversal](https://owasp.org/www-community/attacks/Path_Traversal), [Zip Slip](https://security.snyk.io/research/zip-slip-vulnerability)). Il est donc préférable d'y jeter un oeil, et sans doute aussi builder les images dans un espace totalement déconnecté.

## Un peu de config

### Le user namespace, point Godwin de Docker

Le principal reproche que l'on fait à Docker est que les conteneurs tournent en root. En cas d'escalade, par le biais de Docker, un pirate aura accès à toutes les ressources systemes. Pour contrer cela, Docker permet de [remapper](https://docs.docker.com/engine/security/userns-remap/) les utilisateurs.

```bash title='docker-remap.sh'
groupadd -g 500000 dockremap && \
groupadd -g 501000 dockremap-user && \
useradd -u 500000 -g dockremap -s /bin/false dockremap && \
useradd -u 501000 -g dockremap-user -s /bin/false dockremap-user

echo "dockremap:500000:65536"|sudo tee -a /etc/subuid
echo "dockremap:500000:65536"|sudo tee -a /etc/subgid

echo '
   {
      "userns-remap": "default"
   }
'| tee /etc/docker/daemon.json

systemctl daemon-reload && systemctl restart docker
```

De cette façon, tous les conteneurs sont lancés par l'utilisateur `dockremap` à la place de root 😎. En s’appuyant sur les utilisateur et groupe de référence dockremap (id 500000) et sur la configuration des “subordinate files“ (`/etc/sub(uid|gid)`), le mapping se fait comme suit:
>ID machine hôte |	ID conteneur
500000	0
501000	1000
…	…
565536	65536

On peut donc se contenter d'avoir nos conteneurs `root` puisque ce user sera automatiquement converti en `dockremap` sur l’hôte, tout en gardant les avantages d’être root à l’intérieur de nos conteneurs 😎

:::danger Activation remapping a posteriori
Si vous avez déjà des VM et des conteneurs, les images et les conteneurs disparaissent ! Si vous redémarrez votre démon avec les espaces de noms d'utilisateur désactivés, tout le contenu précédent est là comme vous l'avez noté.
:::tip

Ceci s'explique simplement parceque la ségrégation du contenu des images et des calques séparés par le mappage utilisateur/groupe fait que les UID/GID utilisés dans votre processus d'espace de noms d'utilisateur n'ont plus d'accès en écriture à la plupart des répertoires et plus d'accès en lecture à beaucoup d'autres ressources en raison des autorisation du contenu original. Au lieu d'essayer de faire toutes sortes de trucs au début amusants (genre `chown`) mais au final assez pénibles, il faut recréer un nouveau cache "racine" l'orsqu'on redémarre avec le user remappé...

### La clause USER, on en parle ?

Une clause du Dockerfile n'a pas été encore abordée, USER, permettant de modérément restreindre les droits du conteneur.
Globalement, si un service peut fonctionner sans les privilèges root, autant le faire et utiliser USER pour changer l'utilisateur du conteneur vers un utilisateur non root.

```dockerfile
FROM alpine:3.16.2
RUN mkdir -p /app/website
RUN addgroup zegroup && adduser -D -H -s /bin/false -G zegroup zeuser
WORKDIR /app/website
COPY . /app/website
RUN chown -R zeuser:zegroup /app
USER zeuser
CMD id && ls -alrt /app
```

Créons un fichier avec root sur la machine hôte `sudo touch /tmp/effacemoi`
```bash
docker run -v /tmp:/tmp -it --rm user rm -f /tmp/tst
```

> rm: can't remove '/tmp/tst': Operation not permitted

Normal, zeuser n'est pas root. Or il est possible de contourner USER en explictant le user au lancement du conteneur `docker run -v /tmp:/tmp -it --rm -u root user rm -f /tmp/tst` réussi à la manoeuvre ; cette primitive n'a donc de sens que pour apporter un peu de sécurité sur les applications qui tournent au sein d'un conteneur.


### cap ou pas cap ?

Sous Linux, le kernel sait nativement segmenter les privilèges de l’utilisateur root petits morceaux distincts, les capabilities. Deux librairies sont à retenir : `libcap` pour manipuler les capabilities et `libcap-ng` pour les auditer.

Elles sont principalement les suivantes ([liste exhaustive](https://man7.org/linux/man-pages/man7/capabilities.7.html)) :
- `CHOWN` : Make arbitrary changes to file UIDs and GIDs
- `DAC_OVERRIDE` : Discretionary access control (DAC) - Bypass file read, write, and execute permission checks
- `FSETID` : Don’t clear set-user-ID and set-group-ID mode bits when a file is modified; set the set-group-ID bit for a file whose GID does not match the file system or any of the supplementary GIDs of the calling process.
- `FOWNER` : Bypass permission checks on operations that normally require the file system UID of the process to match the UID of the file, excluding those operations covered by CAP_DAC_OVERRIDE and CAP_DAC_READ_SEARCH.
- `MKNOD` : Create special files using mknod(2)
- `NET_RAW` : Use RAW and PACKET sockets; bind to any address for transparent proxying.
- `SETGID` : Make arbitrary manipulations of process GIDs and supplementary GID list; forge GID when passing socket credentials via UNIX domain sockets; write a group ID mapping in a user namespace.
- `SETUID` : Make arbitrary manipulations of process UIDs; forge UID when passing socket credentials via UNIX domain sockets; write a user ID mapping in a user namespace.
- `SETFCAP` : Set file capabilities.
- `SETPCAP` : If file capabilities are not supported: grant or remove any capability in the caller’s permitted capability set to or from any other process.
- `NET_BIND_SERVICE` : Bind a socket to Internet domain privileged ports (port numbers less than 1024).
- `SYS_CHROOT` : Use chroot(2) to change to a different root directory.
- `KILL` : Bypass permission checks for sending signals. This includes use of the ioctl(2) KDSIGACCEPT operation.
- `AUDIT_WRITE` : Write records to kernel auditing log.

Il suffit de faire son marché avec les options `--cap-drop` et `--ca⁻add` du `docker run` pour modifier le comportement du conteneur. Typquement pour la plupart des containers, on peut dropper AUDIT_WRITE, MKNOD, SETFCAP, SETPCAP ; Les autres ne sont pas actives par défaut et peuvent être simplement activées au besoin mais attention à ne pas faire n'importe quoi avec ces directives qui peuvent facilement augmenter la surface d'attaque.

### quoi ? "C'est con" ?!?

`seccomp` !!

Cette feature du kernel, normalement activée par défaut (`grep SECCOMP /boot/config-$(uname -r)`), permet de filtrer les appels systemes d'un PID. Il fournit un contrôle plus fin que les capacités, donnant en plus à un attaquant un nombre limité d'appels système à partir du conteneur.

![seccomp](./securite/seccomp.png 'filtrage seccomp')

La configuration par défaut de Docker est visibile [ici](https://github.com/docker/docker/blob/master/profiles/seccomp/default.json), elle bloque 44 appels système sur les 300 possibles et descendre en dessous de ce nombre est vraiement à tester avec le fonctionnement de l'applicatif. On peut jouer avec [cet extrait](./securite/seccomp_extrait.json) et :

```bash
docker run --security-opt seccomp=extrait.json alpine:3.16
```

>/ # mkdir truc
mkdir: can't create directory 'truc': Operation not permitted
/ # ping www.google.fr
ping: bad address 'www.google.fr'
/ # ping localhost
PING localhost (::1): 56 data bytes
ping: permission denied (are you root?)
/ # id
uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)

Cela mériterait un article à part entière aussi si vous voulez vous y investir, je vous invite à aller voir [cette page DockerLabs](https://dockerlabs.collabnix.com/advanced/security/seccomp/) et [ce labo AttackDefense](https://www.attackdefense.com/challengedetails?cid=1827)

### Dans ma bench bench bench

A l'instar des pilotes avant un décollage, il est toujours nécessaire de vérifier sa checklist des points de sécu. Docker ne transige pas à la règle et comme tous les outils modernes il fournit un outil, [Docker Bench for Security](https://github.com/docker/docker-bench-security); Se basant sur les précos du Center for Internet Security (CIS), cet outil sort une lonnnnngue TODO-LIST (INFO, NOTE, PASS et WARN) des modifications à apporter au système pour le rendre plus fiable.

Bien entendu, quand vous avez installé Docker vous avez bien sagement [RTFM](https://docs.docker.com/engine/install/debian/) aussi cette liste devrait déjà avoir être limitée 👼

Pour l'obtenir, c'est plus rapide que pour régler tous les points qu'il va vous remonter 👷‍♀️:

```bash
docker run --rm --net host --pid host --userns host --cap-add audit_control \
    -e DOCKER_CONTENT_TRUST=$DOCKER_CONTENT_TRUST \
    -v /etc:/etc:ro \
    -v /usr/bin/containerd:/usr/bin/containerd:ro \
    -v /usr/bin/runc:/usr/bin/runc:ro \
    -v /usr/lib/systemd:/usr/lib/systemd:ro \
    -v /var/lib:/var/lib:ro \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    --label docker_bench_security \
    docker/docker-bench-security
```

Points aiséments régables :

- Ensure auditing is configured for various Docker files : installer `auditd`, rajouter les éléments suivants à surveiller, puis redémarrer ce service qui sert à enregistrer les opérations systèmes sur les fichiers, répertoires et sockets
- Ensure a separate partition for containers has been created : vous n'avez pas de partition dédiée pour docker !
- Ensure the container host has been Hardened : avez-vous bien un mot de passe robuste ?
- Ensure Docker is up to date : dette technique, vous comprendrez :)
- Ensure only trusted users are allowed to control Docker daemon : des utilisateurs ont accès au groupe docker... `gpasswd -d <username> docker`
- Container running without ([memory](https://docs.docker.com/config/containers/resource_constraints/#limit-a-containers-access-to-memory)|[CPU](https://docs.docker.com/config/containers/resource_constraints/#limit-a-containers-access-to-memory)|[root FS](#le-user-namespace-point-godwin-de-docker)) restrictions : c'est un peu open bar chez vous :) Un petit cout de `docker stats` et quelques directives [resources](https://docs.docker.com/compose/compose-file/compose-file-v3/#resources) sur vos docker-compose devraient faire l'affaire !

```bash title='/etc/audit/rules.d/audit.rules'
echo "
## Docker audit
-w /usr/bin/dockerd -p rwxa -k docker
-w /etc/docker -p rwxa -k docker
-w /etc/default/docker -p rwxa -k docker
-w /etc/docker/daemon.json -p rwxa -k docker
-w /var/lib/docker -p rwxa -k docker
-w /usr/lib/systemd/system/docker.service -p rwxa -k docker
-w /usr/lib/systemd/system/docker.socket -p rwxa -k docker
-w /usr/bin/docker-runc -p rwxa -k docker
-w /usr/bin/docker-containerd -p rwxa -k docker
-w /usr/bin/containerd -p rwxa -k docker" | sudo tee -a /etc/audit/rules.d/audit.rules
```

A toute fins utiles, en milieu professionnel, une console centralisées des logs type Splunk ou [concurents libres](https://geekflare.com/fr/open-source-centralized-logging/) est un must-have auquel on ne peut se passer... Attention toutefois à la tarification qui est parfois proportionnel au volume de logs centralisés...

### secret services

// TODO parler de la gestion des secrets
### rootless, en route vers podman

//TODO introduire podman