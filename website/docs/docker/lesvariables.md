---
title: Les variables
tags:
 - docker
 - docker variables
---

<details><summary>&#128172; Synthèse rapide pour les gens &#127939;</summary>

- Gestion des variables :
  - par ligne de commande : directive `--env <clé>=<valeur>` à insérer dans le `docker run` 
  - par fichier de configuration : directive `--env-file <fichierDeVariables>` à insérer dans le `docker run`

</details>

Il sera souvent utile de passer des variables pour contextualiser le fonctionnement des conteneurs. 

Pour cela, rien de plus simple avec la directive `--env` pour fixer la variable `DB_PWD` dans le conteneur :

```bash
docker run -tid --name web3 --env DB_PWD="123NousIronsAuBois" nginx:stable-alpine 
docker exec -ti web3 env
```

>PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin  
HOSTNAME=b69c6d2666a1  
TERM=xterm  
*DB_PWD=123NousIronsAuBois*  
NGINX_VERSION=1.22.1  
NJS_VERSION=0.7.7  
PKG_RELEASE=1  
HOME=/root  

Cette méthode n'est pas des plus idéale (de surcroit pas des plus secure quand il s'agit d'un mot de passe). 

Créons maintenant un fichier `myvars` dans lequel sera simplement conteneu l'association `DB_PWD="123NousIronsAuBois"`, puis supprimons le précédent conteneur (`docker rm -f web3`) avant de le recréer avec la directive `env-file`.

```bash
docker run -tid --name web3 --env-file myvars nginx:stable-alpine 
docker exec -ti web3 env
```
docker enf
On retrouve alors notre variable tout aussi bien instanciée !
