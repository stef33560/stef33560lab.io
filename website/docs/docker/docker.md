---
title: Docker
tags:
 - docker
 - docker images
---

<details><summary>&#128172; Synthèse rapide pour les gens &#127939;</summary>

- Afficher
  - les images : `docker images`
  - les conteneurs : `docker ps` ou `docker ps -a` pour voir également les conteneurs arrétés 
- Récupérer une image : `docker pull`
- Exécuter un conteneur de façon détachée : `docker run -di --name <NomDuConteneur>`
- Exécuter une commande dans un conteneur :  `docker exec -it --name <NomDuConteneur> <commande>`
- Exposer des ports : `-p p_hote:p_conteneur`, d'un coté le port ouvert sur l'hôte et de l'autre celui du conteneur
- Etat réseau du conteneur :
  - ports exposés : `docker port <nomDuConteneur>`
  - IP : `docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <nomDuConteneur>`

</details>


## Premiers pas

Commençons rapidement par expliquer la différence entre les hyperviseurs "traditionnels" et Docker.

[![Différence VM et conteneur, Crédit Wikimedia](./premiers-pas/dockervsvm.jpg)](https://commons.wikimedia.org/wiki/File:Dockervsvm.jpg 'Différence VM et conteneur, (c) Wikimedia')

Les VM reposent sur un hyperviseur qui permet d'émuler "des machines", avec des élements physiques, sur lesquelles sont présent un OS et tous les applicatifs adéquats.

Les conteneurs sont plus "légers" dans la mesure où c'est un simple serveur qui utilise un moteur, Docker, avec des applications installées dans chaque conteneur. Il n'y a donc qu'une machine hôte qui partage ses ressources, son noyaux, aux différents conteneurs. Lorsque le conteneur s’exécute, un environnement d’exécution lui est attribué et il est isolé du reste de la machine sur lequel il s’exécute.

Si les VM font habituellement plusieurs gigaoctects, un conteneur est la plupart de taille réduite (< 500 Mo>) car nous sommes avec Docker dans une approche "[micro-services](https://www.tibco.com/fr/reference-center/what-are-microservices)" : l'applicatif monolithique est scindé en de multiples petits services, chacun situé dans un petit conteneurs:
![microservice](./premiers-pas/microservices-diagram.svg)

Soyons clairs finalement sur le fait que Docker n'a pas inventé les conteneurs ! il en a "juste" popularisé le concept et a rendu simple leur utilisation. En effet, [chroot](https://debian-facile.org/doc:systeme:chroot) ne date pas d'hier (1979), les [jails](https://linuxhint.com/setup-linux-chroot-jails/) encore moins (2000), et les deux pilliers fondateurs que sont les [namespaces](https://linuxembedded.fr/2020/11/namespaces-la-brique-de-base-des-conteneurs) (2002) et les [cgroups](https://www.linuxembedded.fr/2011/03/bien-utiliser-les-cgroups) (2006) ne sont plus tous jeunes ! Pour sa part, les LinuX Containers (aka LXC, 2008) sont les premiers à avoir combiné les cgroups et les namespaces en s'intégrant parfaitement au kernel (contrairement à [OpenVZ](https://fr.wikipedia.org/wiki/OpenVZ), 2005, et [VServer](https://fr.wikipedia.org/wiki/Linux-VServer), 2001).

## Premiers tâtonnements

### Installation et premier lancement

Très simple, via votre gestionnaire de paquet habituel, comme tout paquet :

```bash
sudo apt install docker.io
```

La commande shell éponyme `docker` permet alors d'accéder à un ensemble exhaustifs ; on peut par exemple lister les conteneurs démarrés :

```bash
docker ps
```

:::tip sudo or not sudo

Docker demande des privilèges qui nécessitent de basculer sur l’utilisateur ‘root’ ou faire systématiquement recours à la commande sudo. Pas très confortables dans un mode découverte aussi pour rendre votre expérience Docker plus conviviale, ajoutez votre utilisateur au groupe système docker via un `sudo useradd -g docker $USER`
:::

En l'état, si vous commencez de rien, rien ne s'affiche puisque rien n'est encore lancé !.

Des images prêtes à l'emploi sont récupérables en ligne de commande, via  `docker pull` qui exploite la plateforme [DockerHub](https://hub.docker.com/). Sur ce hub, on peut par exemple retrouver la plus petite distribution Linux, [Alpine](https://hub.docker.com/_/alpine), où est indiqué au passage comment la récupérer avec un `docker pull alpine`.

[!['docker pull'](./premiers-pas/dockerhub_alpine.png)](https://hub.docker.com/_/alpine 'DockerHub Alpine')

C'est parti !

```bash
docker pull alpine
```

> Using default tag: latest
latest: Pulling from library/alpine
213ec9aee27d: Pull complete 
Digest: sha256:bc41182d7ef5ffc53a40b044e725193bc10142a1243f395ee852a8d9730fc2ad
Status: Downloaded newer image for alpine:latest
docker.io/library/alpine:latest

Une fois l'image récupérée, elle apparait dans la liste des images disponibles 

```bash
docker images
```

>REPOSITORY                                                          TAG               IMAGE ID       CREATED        SIZE
alpine                                                              latest            9c6f07244728   2 months ago   5.54MB

Il est alors possible d'instancier cette image dans un conteneur et l'exécuter via un `docker run alpine`. Lancée ainsi, cette commande ne donne aucun résultat : l'image `alpine:latest` est instanciée dans un conteneur qui est exécuté et qui se termine dès lors que le service demandé est réalisé ; et vu que nous n'avons rien demandé, elle s'est donc arrétée. Pour constater cela, il faut lister TOUS les conteneurs y compris ceux arrétés en ajoutant l'option `-a` à `docker ps`

```bash
docker ps -a
```

> CONTAINER ID   IMAGE                                                    COMMAND                  CREATED         STATUS                     PORTS     NAMES
15261ef21398   alpine                                                   "/bin/sh"                4 seconds ago   Exited (0) 3 seconds ago             keen_herschel

Pour garder le conteneur opérationnel il faut le détacher et le rendre intéractif via `docker run -di alpine`. Cette commande vous retournera au passage l'identifiant du conteneur instancié.

```bash
docker ps -a
```

> CONTAINER ID   IMAGE                                                    COMMAND                  CREATED         STATUS                     PORTS     NAMES
81e36514ae0a   alpine                                                   "/bin/sh"                7 seconds ago   Up 6 seconds                         adoring_hopper
15261ef21398   alpine                                                   "/bin/sh"                6 minutes ago   Exited (0) 6 minutes ago             keen_herschel

On observe toujours notre  précédent conteneur (15261ef21398 'Exited (0) 6 minutes ago'), mais également celui qui vient d'être lancé (81e36514ae0a) et qui est donc bien toujours actif ('Up 6 seconds'), répondant **dans ce cas** au doux petit nom de `adoring_hopper`.

Pour s'y connecter, on va demander à Docker d'y exécuter un shell avec `docker exec -it adoring_hopper sh`. Dans l'exemple ci-dessous, vous pourrez constater qu'à l'invite shell la release de l'OS est bien alpine et que dans ce conteneur très léger (2,7 Mo) ne tourne que 3 pid (par exemple, pas de `systemd`)
 ![docker exec](./premiers-pas/dockerexec.png)

Ces deux trois commandes (pull, run et exec) peuvent résumée en une seule : `docker run -it alpine sh` ; si l'image alpine n'est pas sur l'hôte, Docker la télécharge puis instancie un conteneur en mode interactif et branche son tty sur le STDIN actuel (`-ti`) pour y exécuter la commande `sh`

### Premier microservice

Pour cette partie, nous allons instancier un conteneur contenant le serveur web NGINX. Si l'on se réfère aux versions disponibles (dénommés `tags`) sur [la page de NGINIX sur Dockerhub](https://hub.docker.com/_/nginx/tags) on peut trouver différentes version dont une plus légère que les autres basée sur Alpine Linux.
![DockerHub NGINX Alpine](./premiers-pas/dockerhub_nginx.png 'DockerHub NGINX, taggé Alpine')

Deux nouveautés sont introduites :
- nommage de conteneur : pour plus de simplicité appeler `web` à l'aide de l'option `--name`.  
- exposition de ports : un serveur web rend usuellement du sevice sur le port 80, il faut *exposer* (rediriger) le port associé du conteneur sur l'hôte via l'option `-p` ; sans cela, le port 80 ne sera pas en effet pas accessible en dehors du moteur interne de Docker. Dans l'exemple qui suit, je vais donc "brancher" le port 8080 de mon hôte sur le port 80 du conteneur afin de rendre celui-ci accessilbe.

```bash
docker run -tid -p 8080:80 --name web nginx:stable-alpine
```

> Unable to find image 'nginx:stable-alpine' locally
stable-alpine: Pulling from library/nginx
213ec9aee27d: Already exists 
864534705ce1: Pull complete 
fe2c9e7418f8: Pull complete 
f08ef11b2dfc: Pull complete 
36f0053ae033: Pull complete 
e47e25891bf2: Pull complete 
Digest: sha256:d24e098389beed466ea300d5473cdda939bf6e91a93873d0fd1dd18e138c2f13
Status: Downloaded newer image for nginx:stable-alpine
5d097034f1513be5c9bd6e85493f12b7192436293c37f9fb359c7cb624a0ef5b

Résultat : 

- ✅ `curl -sSfI http://127.0.0.1` retourne un `Failed to connect to 127.0.0.1 port 80` ; normal le port 80 du conteneur n'est pas exposé
- ✅ `curl -sSfI http://127.0.0.1:8080` retourne bien un `HTTP/1.1 200 OK`

Information confirmée :

- par `ss -4ltne|grep :80` : le host est en échoute sur le port 8080

> LISTEN 0      4096         0.0.0.0:8080       0.0.0.0:*    ino:63967 sk:100b cgroup:/system.slice/docker.service <->

- Si `docker inspect web` permet de lister toutes les propriétés du conteneur, on peut plus simplement utiliser `docker port web` qui retourne 

>80/tcp -> 0.0.0.0:8080  
80/tcp -> :::8080

Au passage, en parlant de `docker inspect`, il est aussi intéressant de récupérer l'adresse IP de notre conteneur via `docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' web`. Dans mon cas l'adresse du conteneur allouée dynamiquement par Docker est `172.17.0.2` ; ainsi, **sur mon hôte**, `curl -sSfI http://172.17.0.2` reverra donc aussi un `HTTP/1.1 200 OK`. Nous verrons ultérieurement comment Docker réalise l'adressage réseau local à l'hôte.