---
title: Le Dockerfile
tags:
 - docker
 - Dockerfile
 - multistage
---

<details><summary>&#128172; Synthèse rapide pour les gens &#127939;</summary>

- Dockerfile RTFM [ici](https://docs.docker.com/engine/reference/builder/) !
- Construire une image : `docker build -t <nomDeLimage:version> .`
- Consulter l'historique
  - de création d'une image : `docker history <image>:<version>`
  - d'un conteneur : `docker diff <conteneur>`
- Sauvegarder un conteneur modifié comme une nouvelle image `docker commit -m "commentaire" <conteneur> <image>:<version>`
- Publier une image sur dockerhub :
  - Versionner l'image `docker tag <image-source>:<version> <image-destination>:<tagname>`
  - Pousser l'image `docker push <new-repo>:<tagname>`

</details>

## Késako

Un fichier [Dockerfile](https://docs.docker.com/engine/reference/builder/) est façon de créer une image pour instancier un conteneur mieux que ce que nous avons dans [les premiers pas](./docker.md).

Il s'agit d'un bête fichier texte de configuration qui a pour objectif de créer une image au travers d'une séquence d'instructions parmi lesquelles on peut citer :

- [FROM](https://docs.docker.com/engine/reference/builder/#from) pour définir de quelle image on part
- [RUN](https://docs.docker.com/engine/reference/builder/#run) pour lancer des commandes (type gestion de paquets)
- [ENV](https://docs.docker.com/engine/reference/builder/#env) pour définir les variables d'environnement
- [EXPOSE](https://docs.docker.com/engine/reference/builder/#expose) pour exposer les ports du conteneur
- [VOLUME](https://docs.docker.com/engine/reference/builder/#volume) pour créer des espaces de données associés au conteneur, notamment utile pour des partages entre conteneurs
- [COPY](https://docs.docker.com/engine/reference/builder/#copy) pour copier des donner entre l'hôte Docker et le conteneur
- [ENTRYPOINT](https://docs.docker.com/engine/reference/builder/#entrypoint) pour définir le processus maitre du conteneur (celui qui "tourne" au démarrage du conteneur). Attention au passage à **ne pas utiliser** [CMD](https://docs.docker.com/engine/reference/builder/#cmd) à sa place, cette primitive ne devant venir que pour préciser le paramètre par défaut du processus maître .
- etc.

L'intérêt du Dockerfile est de pouvoir recréer la création d'image à tous moments car il agrége toutes les commands à réaliser pour construire un conteneur totalement opératinnel. Cela permet aussi de partager ce fichier avec d'autres personnes, et d'utiliser des scripts manipulant ces Dockerfile. En pratique, cela va aussi éviter de multiplier les arguments dans `docker run`, la création d'image étant toujours spécifique à un besoin (ex: différents environnements pour des développements).

Voici un premier exemple de contenu, aussi [disponible ici](https://gitlab.com/docker33560/pages/-/blob/main/pages/docker/fichierdockerfile/kesako/Dockerfile):

```dockerfile
FROM httpd:alpine
LABEL org.opencontainers.image.authors="stef"
RUN apk --no-cache add php8 php8-fpm \
    && rm -rf /var/cache/apk/* /tmp/*
```

Notre image partira de la dernière version d'Apache sous distribution Alpine Linux pour juste y installer PHP8, puis finalement supprimer les fichiers inutiles pour avoir un conteneur 'propre' lors de son premier lancement. Pour lancer sa construction, on utilise `docker build` avec la directive `-t` pour donner le nom `kesako-image` à l'image :

```bash
docker build -t kesako-image .
```

Ce qui a pour résultat :

>Sending build context to Docker daemon  543.2kB
Step 1/3 : FROM httpd:alpine
 ---> ddb24f700640
Step 2/3 : LABEL org.opencontainers.image.authors="stef"
 ---> Running in 1ea54e376016
Removing intermediate container 1ea54e376016
 ---> dcdb303b86f5
Step 3/3 : RUN apk --no-cache add php8 php8-fpm     && rm -rf /var/cache/apk/* /tmp/*
 ---> Running in 2db7e39e09d0
fetch https://dl-cdn.alpinelinux.org/alpine/v3.16/main/x86_64/APKINDEX.tar.gz
fetch https://dl-cdn.alpinelinux.org/alpine/v3.16/community/x86_64/APKINDEX.tar.gz
(1/8) Installing php8-common (8.0.25-r0)
(2/8) Installing argon2-libs (20190702-r1)
(3/8) Installing ncurses-terminfo-base (6.3_p20220521-r0)
(4/8) Installing ncurses-libs (6.3_p20220521-r0)
(5/8) Installing libedit (20210910.3.1-r0)
(6/8) Installing pcre2 (10.40-r0)
(7/8) Installing php8 (8.0.25-r0)
(8/8) Installing php8-fpm (8.0.25-r0)
Executing busybox-1.35.0-r17.trigger
OK: 66 MiB in 42 packages
Removing intermediate container 2db7e39e09d0
 ---> f37eb8bb6b83
Successfully built f37eb8bb6b83
Successfully tagged kesako-image:latest

`docker images kesako-image` nous confirmera l'existance d'une image à ce nom que nous lancerons (instanciation) dans un conteneur du nom de `kesako-conteneur` via un `docker run -tid --name kesako-conteneur kesako-image`. On confirmera que le conteneur dispose bien de PHP avec `docker exec -ti kesako-conteneur php -v` :

> PHP 8.0.25 (cli) (built: Oct 25 2022 23:07:23) ( NTS )
Copyright (c) The PHP Group
Zend Engine v4.0.25, Copyright (c) Zend Technologies

On pourra aussi consulter l'évolution de l'image par `docker history` :

```bash
docker history kesako-image
```

Va nous retourner l'ensemble des évolutions depuis le "scratch" ayant permis de construire l'image dont nous sommes partis ; nouns ne sommes ici à l'origine que des 3 premières lignes :

>IMAGE          CREATED              CREATED BY                                      SIZE      COMMENT
f37eb8bb6b83   About a minute ago   /bin/sh -c apk --no-cache add php8 php8-fpm …   18.6MB
dcdb303b86f5   About a minute ago   /bin/sh -c #(nop)  LABEL org.opencontainers.…   0B
ddb24f700640   3 weeks ago          /bin/sh -c #(nop)  CMD ["httpd-foreground"]     0B
<missing\>      3 weeks ago          /bin/sh -c #(nop)  EXPOSE 80                    0B
<missing\>      3 weeks ago          /bin/sh -c #(nop) COPY file:c432ff61c4993ecd…   138B
<missing\>      3 weeks ago          /bin/sh -c #(nop)  STOPSIGNAL SIGWINCH          0B
<missing\>      3 weeks ago          /bin/sh -c set -eux;   apk add --no-cache --…   12.7MB
<missing\>      3 weeks ago          /bin/sh -c #(nop)  ENV HTTPD_PATCHES=           0B
<missing\>      3 weeks ago          /bin/sh -c #(nop)  ENV HTTPD_SHA256=eb397fee…   0B
<missing\>      3 weeks ago          /bin/sh -c #(nop)  ENV HTTPD_VERSION=2.4.54     0B
<missing\>      3 weeks ago          /bin/sh -c set -eux;  apk add --no-cache   a…   36.7MB
<missing\>      3 weeks ago          /bin/sh -c #(nop) WORKDIR /usr/local/apache2    0B
<missing\>      3 weeks ago          /bin/sh -c mkdir -p "$HTTPD_PREFIX"  && chow…   0B
<missing\>      3 weeks ago          /bin/sh -c #(nop)  ENV PATH=/usr/local/apach…   0B
<missing\>      3 weeks ago          /bin/sh -c #(nop)  ENV HTTPD_PREFIX=/usr/loc…   0B
<missing\>      3 weeks ago          /bin/sh -c set -x  && adduser -u 82 -D -S -G…   4.68kB
<missing\>      2 months ago         /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B
<missing\>      2 months ago         /bin/sh -c #(nop) ADD file:2a949686d9886ac7c…   5.54MB

## On en tient plusieurs couches !

Une image Docker peut être vue comme un empilement de couches (aka "layers"). Dit autrement, une image est une collection ordonnée de changements sur un filesystem combiné à des paramètres d’exécution correspondant à l'exécution d'un service, chaque couche étant dépendante de toutes celles sur lesquelles elle s’appuie.

[![COW, Copy On Write](https://docs.docker.com/storage/storagedriver/images/container-layers.jpg 'Docker Copy On Write')](https://docs.docker.com/storage/storagedriver/#the-copy-on-write-cow-strategy)

Les couches sous Docker sont de deux types ; sinon la toute dernière qui est read-write, les layers précédents sont read-only. Chaque couche est elle-même une image qui apporte des compléments sur la couche précédente. Nous le verrons [plus tard](#optimisation-du-cache), ces couches peuvent être partagées entre images pour gagner du volume et du temps en terme de chargement, Docker vérifiant que les couches ne sont pas présentes localement avant de les télécharger (pas de duplication tant au niveau stockage qu'au niveau téléchargement).

```mermaid
graph TD
    A[Image 1] --> B(Couche 1)
    B --> C(Couche 2)
    C --> D(Couche 3)


    E[Image A] --> F(Couche A)
    F --> C(Couche 2)
    C --> G(Couche C)
```

Mais reprenons un [Dockerfile](https://gitlab.com/docker33560/pages/-/blob/main/pages/docker/fichierdockerfile/onentientplsrscouches/Dockerfile) similaire à celui de l'étape précédente où nous ajoutons PHP sur une image `httpd:alpine3.16` :

```dockerfile
FROM httpd:alpine3.16
LABEL org.opencontainers.image.authors="stef"
RUN apk update
RUN apk add php8
RUN apk add php8-fpm
RUN rm -rf /var/cache/apk/* /tmp/*
```

Une fois construit via `docker build -t couches:0.1 .`, cette image sera composée de 5 couches (`docker history couches:0.1`) :

>IMAGE          CREATED          CREATED BY                                      SIZE      COMMENT
71507e4bfdc0   15 minutes ago   /bin/sh -c rm -rf /var/cache/apk/* /tmp/*       0B
ba6dfc9fd959   15 minutes ago   /bin/sh -c apk add php8-fpm                     8.59MB
216e9d45871b   15 minutes ago   /bin/sh -c apk add php8                         10.1MB
f77974d85869   15 minutes ago   /bin/sh -c apk update                           2.47MB
3bfe2afb6701   24 minutes ago   /bin/sh -c #(nop)  LABEL org.opencontainers.…   0B
ddb24f700640   4 weeks ago      /bin/sh -c #(nop)  CMD ["httpd-foreground"]     0B 

Chaque ligne d'instruction est en effet à l'origine de la création d'une couche :

![Lien entre Dockerfile et couches](./fichierdockerfile/history.png 'Lien entre Dockerfile et couches')

Ce n'est clairement pas optimal et c'est la raison pour laquelle [dans le premier exemple](#késako) les différentes commandes étaient regroupées en une seule ligne : afin de n'avoir qu'une seule couche pour cet ensemble. Pourquoi ? les couches s'empilant, l'image prends de l'embonpoint avec des choses potentiellement devenues inutiles pour son fonctionnement final. Or dans un cluster, en particulier dans une architecture microservices, plusieurs conteneurs vont éxécuter la même image Docker pour tenir la charge (ex: un revere proxy).

D'un point de vue économique, utiliser de nombreuses images Docker requiert plus de place mémoire et il faudra aussi les stocker en vue de les rendre accessibles à l’ensemble des noeuds du cluster ; Réduire la taille des images permet donc de réduire la mémoire et le disque utilisée, deux ressources qui ne sont de surcroit pas gratuites. Sans compter quand on parle de [sobriété numérique](https://fr.wikipedia.org/wiki/Sobri%C3%A9t%C3%A9_num%C3%A9rique). 

Ces paramètres influent aussi directement sur les performances. Le temps de chargement des conteneurs pour leur exécution (une image de 500Mo prend plus de temps qu’une image de 5Mo).

A coté de cela, d'un point de vue plus SSI <a name="couchessi"></a>, il va de soit que la taille des images influe aussi sur la surface d'attaque (plus y'a de cochonneries, plus on s'expose) et que le nombre de couches peut nous exposer à d'éventuels oublis facheux : des fichiers sensibles laissés sur une couche antérieure à la couche finale n'est pas effacée, ils sont juste historisés !

Dans le conteneur `demo` initié de notre image `couches:0.1` (`docker run -tid --name demo couches:0.1`), que se passe-t-il alors lorsqu'on rajoute un fichier ?

```bash
docker exec demo touch ajout
docker diff demo
```

`docker diff` nous répondra alors que nous avons rajouté un fichier sur la dernière couche :

> C /usr
C /usr/local
C /usr/local/apache2
C /usr/local/apache2/logs
A /usr/local/apache2/logs/httpd.pid
**A /usr/local/apache2/ajout**

Note : chaque modification est historisée (`C` pour create, `D` pour drop, `A` pour append).

Sauvegardons mainenant cette très indispensable 😄 modification, comme si nous souhaitions nous servir ultérieurement de ce conteneur modifié comme image de base :

```bash
docker commit -m "Ajout du fichier ajout" demo couches:0.2
```

Une image en version 0.2 sera alors disponibles et `docker history couches:0.2` nous rappelera que la modification a été apportée sur une nouvelle couche :

>IMAGE          CREATED              CREATED BY                                 SIZE      COMMENT
*2bd61d14a4a6   About a minute ago   httpd-foreground                            2B        **Ajout du fichier ajout***
edaec180995c   6 minutes ago        /bin/sh -c rm -rf /var/cache/apk/* /tmp/*     0B
ca2b63ff47e7   6 minutes ago        /bin/sh -c apk add php8-fpm                 8.59MB
4f6be0b87248   21 minutes ago       /bin/sh -c apk add php8                     10.1MB
cd85be988c01   21 minutes ago       /bin/sh -c apk update                       2.47MB

## Optimisations

Maintenant que vous avez tout compris (ou presque 😏) sur les couches, nous allons pouvoir commencer à les analyser et à les optimiser.

### Réduction de taille

#### 📣 La base

Il est en synthèse primordial de respecter quelques consignes.

:::tip Construire une image correctement

1. Préférer une image minimaliste  
1. Eviter les surcouches  
1. Ne pas déplacer des fichiers  
1. Ne pas ajouter des données inutiles  
1. Faire le ménage  
:::

Utiliser une image optimale pour notre besoin, c'est prendre une image qui contient strictement ce qui est nécessaire, à défaut une image de base. Comme il n’est pas possible de supprimer un fichier qui a été ajouté dans un couche antérieure, le choix de l’image Docker de base est important et pour ma part je préfère manipuler des images utilisant **Alpine linux** (en moyenne, elles sont 30 fois plus petites) ; ceci étant dit, ce n'est pas toujours possible notamment pour l'IOT où certaines images ne sont pas dispo en amrv7 et nous verrons comment plus tard partir de rien (scratch).

Pour ce qui est d'éviter l'empilage de couches inutiles, partout ou cela est possible, il est aisé d'utiliser les **`&&`** (précédemment sur notre directive `RUN`).

Pour réaliser un déplacement, le fichier est d’abord copié à son nouvel emplacement puis supprimé de son ancien emplacement ; il est donc présent dans deux couches, et cela se multiplie si le fichier est ajouté dans une couche précédente ! Placer le fichier directement à la bonne place et utiliser des liens symboliques est donc préférable.

Concernant les donneés inutiles, pour illustration sous Debian, éviter les dépendance suggérées (`--no-install-recommends`) permet d'alléger les paquets pas forcément utiles d'être rappatriés. Il est également nécessaire de supprimer les fichiers qui ne sont plus utiles ; tout du moins les fichiers inutiles produits/introduits au niveau d’une couche doivent être supprimé dans cette même couche, faute de quoi ils seront juste historisés ; nous verrons plus tard pour cela l'usage du [multi stage](https://docs.docker.com/build/building/multi-stage/) qui permet de simplifier la lecture du Dockerfile, de combiner éventuellement plusieurs sources FROM, mais surtout d'en optimiser la taille puisque on pourra utiliser uniquement la partie utile d'un stage, et pas tout ce a permis de la composer (genre des modules npm, composer ou encore python 🤗).

Globalement vous l'aurez compris il faut être rigoureux et être conscient des modifications apportées par les commandes employées. Typique sous Debian, les fichiers produits par `apt-get update` se trouvent dans le dossier `/var/lib/apt/lists` qui n'a aucune utilité pour le service du conteneur. Le gestionnaire de paquet Alpine n'a d'ailleurs par défaut pas de cache configuré 😎. Minimalisme on vous dit ! De même, ne pas introduire de fichiers inutiles (comme des logs ou des historiques divers) peut être facilité à l'aide d'un fichier `.dockerignore`, similaire au `.gitignore` de GIT, qui exclue les fichiers non nécessaire pour l'image ; exemple de contenu :

>cefichierla.txt
logs/
.git
*.md
.cache

### Démonstration et optimisation du cache

Dans toutes ces démos, vous aurez remarqué que des téléchargements sont très souvent lancés. Cela ralenti la construction des images, rien n'étant mis en cache, et de façon à délivrer les contenus les plus légers possibles et le plus rapidement possible (si possible, via le partage des couches).

Repartons de [ce simple Dockerfile](https://gitlab.com/docker33560/pages/-/blob/4abab598d5cba00892aad4cd7aa469443b2bff0a/pages/fichierdockerfile/cache/Dockerfile) où nous installons vim et curl :

```dockerfile
FROM node:lts-alpine3.16
LABEL org.opencontainers.image.authors="stef"
RUN apk add vim
RUN apk add curl
```

La première fois que ce dockerfile est buildé, les composants sont récupérés ; la 2nde, pourvu qu'on fasse un temps soit peu au déroulé de la console, il est bien mentionné qu'ils sont récupérés du cache local :
> ---> Using cache

Si nous modifions le build pour construire l'image avec un `docker build --no-cache -t cache:0.2 .`, nous forçons le re-téléchargement. De façon plus atomique, il est possible de [rajouter](https://gitlab.com/docker33560/pages/-/blob/main/pages/docker/fichierdockerfile/cache/Dockerfile) uniquement un `--no-cache` sur l'`apk add` de vim, le cache n'est pas utilisé pour vim... mais plus non plus pour curl ! Cela illustre qu'il faut traiter les partie variables des images sur la fin du  pour maximiser l'usage du cache.

:::tip Ordonner son Dockerfile

Pour maximimiser vous pouvez les classer de la moins fréquemment modifiée (pour s'assurer que le cache de construction est réutilisable) à la plus fréquemment modifiée (celle qui cassera l'utilisation du cache pour les couches à venir).

:::

Ce cache est une bénédiction, mais il faut être attentif aux évènements dynamiques : le build "fige" l'image à un instant donné. Ceci est aussi valable pour les commandes de mise à jour des repositiories (`apk`, `apt`) lancés au début de script !

:::tip Expliciter le no-cache

Pour les directives qui interagissent avec des dépandances pouvant êtres mise à jour, il est faut expliciter un `no-cache` pour ne pas récupérer les dépendances datant de l'âge du build de l'image.

:::

#### Démonstration multi-stage

Partant de [ce Dockerfile](https://gitlab.com/docker33560/pages/-/blob/49563cd42ff7e4ccde96be861d14e866afa9c28e/pages/fichierdockerfile/multistage/Dockerfile) permettant de construite une image Docker de ce site Docusaurus :

```dockerfile
FROM node:lts

WORKDIR /app/website

COPY ./website /app/website
RUN yarn install

EXPOSE 3000 35729
ENTRYPOINT ["yarn", "serve"]
```

Résultat : 1.51Go !

L'image de base `node:lts` est basée Debian et pèse déjà [352Mb](https://hub.docker.com/layers/library/node/lts/images/sha256-16258bac1617ed4456e7f540c4468478fd7341ffeed478ffba1920013efc1348?context=explore), ça ne coute pas grand chose de passer sur une `node:lts-alpine3.16` ([49Mb](https://hub.docker.com/layers/library/node/lts-alpine3.16/images/sha256-ccd756ecfba8272d6638b0a39f2a2f175b4a3c03f2f045be1c5a9d29914fbc0e?context=explore)). Avec [ce simple changement](https://gitlab.com/docker33560/pages/-/blob/af1d3bdf87b1a23c282048cf11440f9e3b0a8a83/pages/fichierdockerfile/multistage/Dockerfile), notre image passe déjà à 780Mo.

Une fois en distribution Alpine, qui est déjà assez propre, on peut estimer que notre script d'installation ne produit pas beaucoup de déchets... appliquons quand même le multistages pour voir :

1. Nous partons de l'image LTS Alpine de Node, layer appelé "base"
2. De cette base, nous créons une couche intermédiaire de build dans laquelle on réalise les opérations permettant de compiler le site.
3. Nous repartons de l'image de base pour y copier le résultat du stage builder, et nous servons cela.

Le [Dockerfile](https://gitlab.com/docker33560/pages/-/blob/main/pages/docker/fichierdockerfile/multistage/Dockerfile) a alors cette tête :

```dockerfile
FROM node:lts-alpine3.16 as base

FROM base as builder 
WORKDIR /app/website

COPY ./website /app/website
RUN yarn install

FROM base 
COPY --from=builder /app/website /app/website
EXPOSE 3000 35729
WORKDIR /app/website
ENTRYPOINT ["yarn", "serve"]
````

Le résultat est sans appel : 358Mo ! encore 50% de gain !!

>REPOSITORY                             TAG             IMAGE ID       CREATED          SIZE
optim                                  0.3             d574f905ed61   38 seconds ago   **358MB**
optim                                  0.2             7346d6b07a5c   2 hours ago      **780MB**
optim                                  0.1             1783a1bf8428   2 hours ago      **1.51GB**

:::tip Appliquer le multistage
Les constructions en plusieurs étapes permettent de généralement réduire considérablement la taille de l'image finale, sans se débattre avec le nombre de couches et les fichiers intermédiaires.
:::

## Registre d'images

Une fois votre belle et petite image construite en local sur notre workstation préférée, il serait utile de la conserver dans un registre distant dans le cloud. DockerHub est le registre distant officiel, un catalogue qui nous a d'ailleurs permis l'hébergement et le "push/pull" des images dont nous sommes partis ; Ce registre n'est toutefois pas les seuls et d'autres proposent leurs service comme GitLab, Red Hat Quay, Amazon ECR, Google Container Registry & co.

### tar*TAG*ueule à la récré

Cette étape est anodine dans le contexte d'une image prise seule, mais pourra avoir des effets dramatiques lorsque les images seront déployées automatiquement par un ordonnanceur. L'ordonnanceur est "bête et méchant", il déploie les versions qu'on lui dit sans se soucier de leur contenu ! Il est donc important dès le départ de comprendre comment bien tagguer ses images.

Créons dans un premier temps l'[image](https://gitlab.com/docker33560/pages/-/blob/017bb733c7dec5b54754f94833b89c5d07a8c4f6/pages/fichierdockerfile/registre/Dockerfile) suivante par `docker build -t monalpine:1.0 .` :

```dockerfile
FROM alpine:3.16
LABEL org.opencontainers.image.authors="stef"
```

Si nous cherchons à l'exécuter `docker run monalpine` ne fonctionne pas car Docker se réfère par défaut à la version `latest` ; Nous allons donc tagger, autrement dit **versionner**, notre image par `docker tag monalpine:1.0 monalpine:latest` :

>REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
monalpine    1.0       5e7e598b0160   3 days ago      5.54MB
monalpine    latest    5e7e598b0160   3 days ago      5.54MB

L'image ID est la même car nous n'avons donc pas vraiment créé une image mais simplement ajouté un alias (une sorte de lien symbolique) comme en atteste un `docker inspect -f '{{.RepoTags}}' monalpine:1.0` : les deux tags cohabitent sur le même Image ID.

> [monalpine:1.0 monalpine:latest]

 Sans modifier le dockerfile, si nous rebuildons l'image sous un autre nom, par exemple `doublon:1.0`, nous ne recérons pas une image mais nous associerons un nouveau tag sur l'image `monalpine:1.0` ; la supprimer avec `docker rmi doublon:1.0` renverra d'ailleurs

> Untagged: doublon:1.0

L'image continue d'exister en `1.0` et en `latest`.

Observons maintenant les constructions suivantes :

- Modification du contenu de l'image  
Modifions légèrement notre [Dockerfile](https://gitlab.com/docker33560/pages/-/blob/e39f4b7b2400e7bfb0159b1a74abefba4d54f315/pages/fichierdockerfile/registre/Dockerfile) pour y ajouter git. L'image alors construite par `docker build -t monalpine:1.1` est différente de `monalpine:1.0` aussi cette image aura bien un ID différent.

> REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
monalpine    1.1       3121b0a4f81a   5 seconds ago   19.6MB
monalpine    1.0       5e7e598b0160   3 days ago      5.54MB
monalpine    latest    5e7e598b0160   3 days ago      5.54MB

Un `docker tag monalpine:1.1 monalpine:latest` aura pour effet de dépalcer le tag `latest` vers la version 1.1 comme l'indique l'inspection des images :

```bash
docker inspect --format='{{.Id}} > Tag : {{.RepoTags}}' monalpine:1.0
docker inspect --format='{{.Id}} > Tag : {{.RepoTags}}' monalpine:1.1
```

>sha256:5e7e598b01602eed0a8ce8e1540f336166cef1c1ede9281b8111331797c379e1 > Tag : [monalpine:1.0]
sha256:e72ce43b02c2a46f0962fbd5bbc4498f93d1f39c504b3179a94f81928436aa8c > Tag : [monalpine:1.1 monalpine:latest]

:::tip Ne jamais builder une image sur un tag `latest`.
Ce tag doit être réservé pour identifier la dernière version disponible.
:::

- Modification de la clause FROM  
Modifions de la même manière notre [Dockerfile](https://gitlab.com/docker33560/pages/-/blob/93b5b3c8e19e7650918fd21fd36e708569ab3ee5/pages/fichierdockerfile/registre/Dockerfile) pour y ajouter git, mais cette fois ci en prenant comme FROM `monalpine:1.0`. Le build de l'image `monalpine:2.0` fera apparaitre une nouvelle image dont l'ID est forcément différent

>REPOSITORY   TAG       IMAGE ID       CREATED              SIZE
monalpine    2.0       39f20abcf704   7 seconds ago        19.6MB
monalpine    1.1       3121b0a4f81a   About a minute ago   19.6MB
monalpine    latest    3121b0a4f81a   About a minute ago   19.6MB
monalpine    1.0       5e7e598b0160   3 days ago           5.54MB

De quoi sont composées les images 1.1 et 2.0 ? regardons cela avec `docker history` :

monalpine:1.1 :

> IMAGE          CREATED          CREATED BY                                      SIZE      COMMENT
3121b0a4f81a   12 minutes ago   /bin/sh -c apk add --no-cache git               14MB
**5e7e598b0160**   3 days ago       /bin/sh -c #(nop)  LABEL org.opencontainers.…   0B
*9c6f07244728*   2 months ago     /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B
<missing\>      2 months ago     /bin/sh -c #(nop) ADD file:2a949686d9886ac7c…   5.54MB

monalpine:2.0 :

> IMAGE          CREATED          CREATED BY                                      SIZE      COMMENT
3121b0a4f81a   40 minutes ago   /bin/sh -c apk add --no-cache git               14MB
**5e7e598b0160**   3 days ago       /bin/sh -c #(nop)  LABEL org.opencontainers.…   0B
*9c6f07244728*   2 months ago     /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B
<missing\>      2 months ago     /bin/sh -c #(nop) ADD file:2a949686d9886ac7c…   5.54MB

Vu que vous n'avez pas oublié qu'une layer est une image, il est aisé de retrouver à qui elle appartient avec `docker inspect --format='{{ .RepoTags }}` ;

- 9c6f07244728 : il s'agit de `alpine:3.16`
- 5e7e598b0160 : il s'agit de `monalpine:1.0`

Ces deux images sont donc strictement indiques malgré un Dockerfile différent mais ce n'est clairement pas limpide ! il eut été interessant pour le spéléologue que nous sommes devenu d'avoir ajouté des instruction `LABEL` afin d'être bien plus exicite. Et pour le mainteneur de cette image, il aurait été souhaitable qu'il respecte des conventions de taggages claires !

:::tip Respecter des conventions de [taggage](#taggage)

- `:1` : un tag stable pour une version majeure.  
  S'il y a des révisions mineures, 1 represente la plus récente version de la version 1.
- `:1.0` : un tag stable pour la version 1.0  
  Cela permet au développeur de bien correspondre à la version 1.0, et pas de pointer sans le savoir vers la verion 1.1
- `:latest` : mot clé réservé  
  Représente la dernière version stable de l'image, quelle que soit la version majeureis.
:::

![Convention de nommage](./fichierdockerfile/stabletaggingwithupdates.gif 'Conventions de nommage')

Il aurrait ainsi fallu nommer notre release de la façon suivante :

- `monalpine:1.1` : pour l'ajout de features (ici représenté par git)
- `monalpine:2.0` : à l'avènement de la prochaine version majeure de l'image de base

### Poussez madame

Maintenant que notre image est correctement nommée, nous allons pouvoir la versionner sur notre registre GitLab.

Pour cela, je préfère utiliser un token au lieu de répandre aux 4 vents mon login/mot de passe. J'imagine que vous aussi donc rendez-vous dans la section [Access Token](https://gitlab.com/-/profile/personal_access_tokens) de notre compte utilisateur que ça se passe (N'oubliez pas de copier cet ID une fois créé !) :

![Personnal Acces Token GitLab](./fichierdockerfile/PersonalAccessTokensGitLab.png 'Personnal Acces Token GitLab')

Ensuite, dans un projet GitLab, la section [Registre de conteneur](https://gitlab.com/docker33560/pages/container_registry) vous indiquera comment faire pour pousser vers vos conteneurs.

A l'invite de `docker login registry.gitlab.com`, saisissez votre username en guise de login et le token en guise de password.

```bash
docker tag monalpine:1.0 registry.gitlab.com/docker33560/pages:1.0
docker tag monalpine:1.0 registry.gitlab.com/docker33560/pages:latest
docker push registry.gitlab.com/docker33560/pages:1.0
docker push registry.gitlab.com/docker33560/pages:latest
```

![Registry avec la 1.0](./fichierdockerfile/gitlabregistry.png 'Registry avec la 1.0')

Nous pouvons constater que la `latest` pointe bien sur la `1.0`

```bash
docker tag monalpine:1.1 registry.gitlab.com/docker33560/pages:1.1
docker push registry.gitlab.com/docker33560/pages:1.1
```

Désormais vous pouvez voir [ici](https://gitlab.com/docker33560/pages/container_registry/3596727) que la `latest` pointe sur la `1.1`

A noter : Vous pouvez également échanger des images par import export !

- sauvegarde : `docker save -o /tmp/pages_1.1.tar registry.gitlab.com/docker33560/pages`  
La structure est la suivante : `tar -tvf pages_1.1.tar | python3 /usr/local/bin/treeify.pyd`[^1]

> .
├─ drwxr-xr-x 0
│  ├─ 0               0 2022-11-01 15:08 219101aef844b5427f194ae095ca5c84143e4ea6ea3365efa889792e20d68ec3
│  │  └─ 
│  ├─ 0               0 2022-11-04 17:51 3e571912155d9bac1a5285bf1c21105bea53585f77a159316eed491882710ab2
│  │  └─ 
│  └─ 0               0 2022-11-04 15:41 6f6fc2c464a99a2eb32de638e408681dc252d87efe0d4569969286346aded516
│    └─ 
├─ -rw-r--r-- 0
│  ├─ 0               3 2022-11-01 15:08 219101aef844b5427f194ae095ca5c84143e4ea6ea3365efa889792e20d68ec3
│  │  └─ VERSION
│  ├─ 0            1248 2022-11-01 15:08 219101aef844b5427f194ae095ca5c84143e4ea6ea3365efa889792e20d68ec3
│  │  └─ json
│  ├─ 0         5826560 2022-11-01 15:08 219101aef844b5427f194ae095ca5c84143e4ea6ea3365efa889792e20d68ec3
│  │  └─ layer.tar
│  ├─ 0            1840 2022-11-04 15:41 3121b0a4f81a5789d5351375e0586b6bcde1272b8dff5175939d05c5b1bde4a2.json
│  ├─ 0               3 2022-11-04 17:51 3e571912155d9bac1a5285bf1c21105bea53585f77a159316eed491882710ab2
│  │  └─ VERSION
│  ├─ 0             406 2022-11-04 17:51 3e571912155d9bac1a5285bf1c21105bea53585f77a159316eed491882710ab2
│  │  └─ json
│  ├─ 0            1715 2022-11-01 15:08 5e7e598b01602eed0a8ce8e1540f336166cef1c1ede9281b8111331797c379e1.json
│  ├─ 0               3 2022-11-04 15:41 6f6fc2c464a99a2eb32de638e408681dc252d87efe0d4569969286346aded516
│  │  └─ VERSION
│  ├─ 0            1282 2022-11-04 15:41 6f6fc2c464a99a2eb32de638e408681dc252d87efe0d4569969286346aded516
│  │  └─ json
│  ├─ 0        14429184 2022-11-04 15:41 6f6fc2c464a99a2eb32de638e408681dc252d87efe0d4569969286346aded516
│  │  └─ layer.tar
│  ├─ 0             665 1970-01-01 01:00 manifest.json
│  └─ 0             266 1970-01-01 01:00 repositories
└─ lrwxrwxrwx 0
  └─ 0               0 2022-11-04 17:51 3e571912155d9bac1a5285bf1c21105bea53585f77a159316eed491882710ab2
    └─ layer.tar -> ..
      └─ 219101aef844b5427f194ae095ca5c84143e4ea6ea3365efa889792e20d68ec3
        └─ layer.tar

- chargement : `docker load -i /tmp/pages_1.1.tar`

## Notes

[^1] : [treeify.pyd](https://bitbucket.org/Hakril/treeify/src) permet d'afficher une arborsence d'une archive