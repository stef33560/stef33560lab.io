<?php
$WORKER_DIR=getenv("WORKER_DIR");
$WORKER_TIMEOUT=getenv('WORKER_TIMEOUT');
$WORKER_COLOR=$argv[1];
$INDEXFILE=getenv("INDEXFILE");

$files = array_diff(scandir($WORKER_DIR), array('..', '.'));
$data=array();

// get data from workers
foreach ($files as &$file) {
    $fullpath=$WORKER_DIR.'/'.$file;
    if (file_exists($fullpath)) {
        $data = array_merge($data, array("$file"=>file_get_contents($fullpath)));
    }
}

// build HTML data
if (count($data)>0) {
    $workersResultsHTML = "";
    foreach ($data as $worker => $value) {
        $workersResultsHTML .=  "<div style='background: #$WORKER_COLOR; border-color: #".substr($worker, 0, 6).";'>$value</div>";
    }
    $html = '<div class="parent"> '.$workersResultsHTML.'</div> </div>';
echo $html;
    $content = file_get_contents($INDEXFILE);
    $newContent = substr_replace($content, $html, -17, 0);
    file_put_contents($INDEXFILE, $newContent);
}
?>