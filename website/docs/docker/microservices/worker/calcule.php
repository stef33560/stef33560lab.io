<?php
$INDEXFILE=getenv('INDEXFILE');
$WORKERDIR=getenv("WORKER_DIR");
$WORKER_NUM=gethostname();
$WORKER_TIMEOUT=getenv('WORKER_TIMEOUT');
$WORKER_COLOR=substr($WORKER_NUM, 0, 6);
#$WORKER_COLOR=bin2hex(openssl_random_pseudo_bytes(3));

$now=time();
$clock=0;

function dateDiff($date){
    $diff = abs($date - time());
    $retour = array();
    $tmp = $diff;
    $retour['s'] = $tmp % 60;
    $tmp = floor( ($tmp - $retour['second']) /60 );
    $retour['m'] = $tmp % 60;
    $tmp = floor( ($tmp - $retour["minute"])/60 );
    $retour['h'] = $tmp % 24;
    $tmp = floor( ($tmp - $retour['hour'])  /24 );
    $retour['d'] = $tmp;
    return $retour;
}

while (true) {
	sleep($WORKER_TIMEOUT);

	$content = ++$clock." bpm ($WORKER_NUM) | Uptime ".rtrim(http_build_query(dateDiff($now),'',', '),1);
	$file = "$WORKERDIR/$WORKER_NUM";

    file_put_contents($file, $content);
	
	shell_exec("php /affichage.php $WORKER_COLOR");
}
?>