---
title: Les volumes
tags:
 - docker
 - docker volume
---


<details><summary>&#128172; Synthèse rapide pour les gens &#127939;</summary>

- Gestion des données (volumes) :
  - partage avec le host : directive `-v <dossierDuHost>:<DossierDuConteneur` à insérer dans le `docker run`
  - partage par volume : `docker volume` avec `create <nomDuVolume>` pour le créer

</details>

## Problématique

Le conteneur est un élément éphémère, or pour fonctionner les applicatifs ont besoin de données. Il faut donc disposer d'un moyen pour "sauvegarder" les données. S'il peut être tentant pour le très néophyte d'utiliser le conteneur pour le transformer en image pour ensuite le relancer, ce qui est la pire des pratiques (et on peut se demander même comment en arriver à ce genre d'extrémintés !), nous allons voir comment manipule les volumes docker ... tout en voyant que leur usage peut également s'avérer hasardeux 😄

## Rappel : le filesystem est un layer !

Une image Docker se compose d'un ensemble de calques (layers) en lecture seule, seul le dernier étant rw. Lorsque vous lancez un conteneur à partir d'une image, Docker ajoute donc au sommet du tas de layers un nouveau layer en lecture-écriture ("[Union File System](https://fr.wikipedia.org/wiki/Union_File_System)"). Dès lors, sur ce layer :

* Lors d'une modification : Docker crée une copie depuis les couches en lecture seule vers le layer en lecture-écriture.
* Lors d'une création : Docker crée le fichier uniquement sur le layer en lecture-écriture, et ne touche pas au layer en lecture seule.
* Lors d'une suppression : Docker supprime le fichier uniquement sur le layer en lecture-écriture, et il le conserve sur les couches antérieures s'il y existe (ce qui peut causer des failles de sécu, j'en parle [ici](/docs/docker/fichierdockerfile#couchessi))

Lorsqu'un conteneur est supprimé, le layer en lecture-écriture est supprimé avec. Cela signifie que toutes les modifications apportées après le lancement du conteneur auront disparus avec.

### Introduction

Par défaut, les données du conteneur sont stockées... dans le conteneur 😏 Mais dans une certaine limite !

Allons voir cela en modifier par exemple la page d'accueil de NGINX située dans `/usr/share/nginx/html/index.html` d'un conteneur appelé web que nous aurons lancé via un `docker run --name web -d -p 8080:80 nginx:stable-alpine`. 

Deux méthodes pour cela :

- Après avoir ouvert un shell dans le conteneur via `docker exec -it web sh`, modifier le fichier ad hoc avec votre commande préférée.  
  N'oubliez pas que les conteneurs sont très light aussi il est possible que toutes les commandes ne soient pas disponibles (`apt-get` est ton ami)
- Modifier directement via par exemple un `docker exec web sed -i 's/\(nginx\)/\1@Docker/gi' /usr/share/nginx/html/index.html`

[![Homepage NGINX modifiée](./premiers-pas/nginxhome.png 'La home page est modifiée !')](http://127.0.0.1 'La home page est modifiée !')

Formidale. Maintenant laissons nous aller à deux scénarii :

- Le conteneur s'est arrété ; il faut le relancer.  
  Arrêtons pour cela manuellement le conteneur via un `docker stop web`, puis relançons le via `docker start web`.  
  ✅ Impeccable, la page <http://127.0.0.1:8080> est toujours là modifiée.  

- Le conteneur est perdu ; il faut le recréer.  
  Pour cela supprimons le avec `docker rm -f web` et relançons le `docker run -tid -p 8080:80 --name web nginx:stable-alpine`.  
  ❌ La page <http://127.0.0.1:8080> est revenue à sa version initiale.  

:::danger  Les conteneurs ne stockent pas la partie data de façon *persistante* 
Les données sont perdues avec le conteneur lorsqu'il disparait. 
:::

Pour palier à cela, il faut externaliser les data par le biais [des volumes](https://docs.docker.com/storage/volumes/ 'Volumes dockers') qui consistent, pour faire simple, à monter un dossier de l'ĥote dans le conteneur.

```bash
mkdir -p /srv/data/nginx/
docker run -tid -p 8080:80 -v /srv/data/nginx/:/usr/share/nginx/html/ --name web nginx:stable-alpine
```

A ce stade, `curl -sSfI http://127.0.0.1:8080` renvoie une erreur 403. Et pour cause ! le dossier `/usr/share/nginx/html/` est vide puisqu'il est mappé sur notre dossier `/srv/data/nginx/`. Ajoutons un fichier d'index

```bash
echo "<html><body>Stef was here</body></html>" | sudo tee /srv/data/nginx/index.html
```

[![Page d'accueil stocké sur le host](./volumes/nginxhome-share.png)](http://127.0.0.1:8080 "Page d'accueil stockée sur le host")

Le conteneur Docker ne doit être qu'un outil, un exécutant, qui manipule des données qui doivent être externalisées ; de cette façon, elles deviennent pérennent (notamment via des mécanismes de sauvegarde usuels, NFS, SAN, etc.) et les conteneurs sont cantonnés à leur rôle visant à fournir du (micro)service. Au delà de la persistance des données, l'intérêt de travailler avec des volume est multiple devient évident lorsqu'il s'agit de faire des backups, et essentiel pour la scalabilité horizontale (partage entre multiples conteneurs).

### Conteneur TBM 👼

Seules deux options s'offrent à nous pour gérer les données : une bonne et ... une moins bonne 😄. En vrac, tentez de deviner qui fait quoi 😉

- [ ] `./public:/usr/share/nginx/html`
- [ ] `/var/lib/postgresql/data`
- [ ] `/opt/data/html:/usr/share/nginx/html`
- [ ] `~/configs:/etc/configs`
- [ ] `postgresql-data:/var/lib/postgresql/data`

Alors ? facile ? y'a des choses qui pour vous ne peuvent pas marcher ? Alors détaillons.

Déjà la syntaxe Docker sur les volumes peut aider (comme d'hab ce qui est entre crochet, c'est optionnel):
> [SOURCE:]TARGET[:MODE]

* SOURCE : sur l'hôte, peut être un chemin relatif ou absolu, ou bien un volume nommé
* TARGET : dans le conteneeur, un chemin absolu
* MODE : une option permettant de monter le volume en read-only (`ro`) ou en read-write (`rw`).

Il n'y a donc que 3 variations qui peuvent être possibles.

#### SOURCE absent

C'est le cas de `/var/lib/postgresql/data`. Dans ce cas, Docker va créer un dossier anonyme et le monter comme un volume Docker et le monter sur le dossier indiqué dans le conteneur. Ce volume docker est stocké par défaut dans `/var/lib/docker/volumes/<uuid>/_data`, où `uuid` est un ID aléatoire assigné au volume en tant que nom.

#### SOURCE qui n'est pas un chemin
C'est le cas de `postgresql-data:/var/lib/postgresql/data`. Dans ce cas, Docker pense qu'on se réfère à un volume Docker déjà existant.

#### SOURCE qui est un chemin
C'est le cas de `/opt/data/html:/usr/share/nginx/html`. Absolute ou relatif, Docker va binder le dossier indiqué à l'endroit explicité en 2nde partie dans le contneeur. 

## Exercice rapido-pratico

J'avais dit en intro qu'il y avait du bon et du moins bon dans ces 2,5 😉 façons de partager la data au conteneur. 

![docker exec](./volumes/bienpasbien.png)

### Pas Biennnn

Je vais commencer par la moins bonne, [la dernière](#SOURCE-qui-est-un-chemin), qui s'appelle le Bind mount et qui permet de monter n'importe quel point du filesystem à l'intérieur du conteneur. Créons un dossier qui contient un fichier et mappons le sur un shell Alpine :
```
mkdir foo
echo "hello n00b" > foo/bar
```
Par la suite, lancer le conteneur à l'aide d'une des deux commandes :
- Soit avec `docker run -ti --rm --mount type=bind,source=$PWD/foo,target=/foo,readonly alpine sh`
- Soit avec `docker run -ti --rm --volume $PWD/foo:/foo:ro alpine sh`


De là, un `ls` nous montrera les dossiers usuels (`bin`, `dev`, etc...) mais aussi le dossier `foo` contenant mon fichier. Problèmes :
1. le conteneur dépendant d'un chemin sur le filesystem n'est plus portable sur un autre hôte
1. le conteneur a un accès direct au filesystem de l'hôte, ce qui est une brèche de sécurité.

Donc sauf besoin très spécifique (spoil, y'en a 3 : partage de config, de source code, d'arborescence immuable), cette méthode est quand même plutôt à proscrire ! Pour plus d'infos, [RTFM](https://docs.docker.com/storage/)

Et pour finir cette modalité de partage, deux petits conseils.

:::tip Gestion des logs

Monter des volumes dédiés pour les répertoires tels que /var/log/ dans le conteneur, pour que les journaux soient écrits sur le système de fichiers de l'hôte, peut sembler une bonne idée à la base puisqu'ils peuvent alors être extraits vers un serveur dédié à cet usage. En pratique, une meilleure   serait surement de configurer le bazard pour renvoyer l'ensemble des logs vers la sortie standard.

`ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log`
:::


:::danger attention aux volumes sensibles !!

Le montage de volumes sensibles (/etc/, /boot/, /proc/, ...) dans un conteneur fait que le processus exécuté dans ce conteneur peut éditer le contenu des fichiers comme l'utilisateur root de l'hôte ! 

De même, le montage de la socket Docker `/var/run/docker.sock` doit être fait avec précaution puisque il s'agit du moyen utilisé par le client Docker pour dialoguer avec le démon .... et donc instancier des conteneurs, modifier des images, etc. Un attaquant ayant accès à cette socket pourra donc ainsi s'échapper de son conteneur avec les droits associés à Docker (qui sont, par défaut, root).
:::

### Biennnn

Utilisons donc un vrai beau Volume Docker adressable avec l'argument `--volume` ou `--mount`. Cette fois on ne donne pas de chemin sur le filesystem car un volume est créé et stocké dans `/var/lib/docker/volumes/` ; il peut donc être réutilisé ! Petite démo : démarrons un conteneur lambda qui a un volume `/foo` puis démarrons en un second qui va créer à la volée notre volume `foo` :

```bash
docker run --name c1 -ti --volume /foo alpine sh
echo "j'écris sur mon volume foo depuis c1" > /foo/bar
```
Notez la syntaxe alternative possible à la place de `--volume /foo` : `--mount type=volume,src=foo,destination=/foo`

Pour le second conteneur :
```bash
docker run --name c2 -ti --volumes-from c1 alpine sh
```

Dans ce 2nd conteneur, `cat foo/bar` nous renverra bien la phrase écrite depuis le 1er conteneur.

Sur l'hôte, `docker volume ls` présentera bien ce montage, ce volume :

> DRIVER              VOLUME NAME
> local               ff4cbf54d88a71c3995421cf7e4f40a4c31fd9ccc2d4eb254a2cb3dff5476a58

On pourra vérifier avec `sudo ls /var/lib/docker/volumes/` que ce volume est présent sous forme d'un fichier :

> ff4cbf54d88a71c3995421cf7e4f40a4c31fd9ccc2d4eb254a2cb3dff5476a58
> metadata.db

L'inspection des conteneurs va permettre de confirmer via un `docker inspect -f '{{ .Mounts }}' c1`.
> [{volume ff4cbf54d88a71c3995421cf7e4f40a4c31fd9ccc2d4eb254a2cb3dff5476a58 /var/lib/docker/volumes/ff4cbf54d88a71c3995421cf7e4f40a4c31fd9ccc2d4eb254a2cb3dff5476a58/_data /foo local  true }]

Comme vous l'aurez compris, à l'avenir il faudra faire autrement pour que les volumes aient un nom compréhensible... Et éviter de devoir faire ce tour de passe-passe 💩

```bash
docker run --rm -it -v ff4cbf54d88a71c3995421cf7e4f40a4c31fd9ccc2d4eb254a2cb3dff5476a58:/from -v ZoliNomDeVolume:/to alpine ash -c "cd /from ; cp -av . /to"
docker volume rm ff4cbf54d88a71c3995421cf7e4f40a4c31fd9ccc2d4eb254a2cb3dff5476a58
docker volume ls
```

> DRIVER    VOLUME NAME
local     ZoliNomDeVolume

## Gestion des volumes

La première approche volontairement simpliste avait vocation à faire comprendre le caractère impérieux de l'externalisation des données. Les volumes Docker doivent donc être préférentiellement manipulés à l'aide de `docker volume` qui peut prendre 5 arguments. 

- `create` : `docker volume create monVolume` va nous permettre de créer un espace nommé `monVolume` contenant de la donnée à destination d'un conteneur. C'est quand même mieux qu'un `uid` imbitable, non ? 😄
- `ls` :  `docker volume ls` permet de confirmer la création de cet espace

>DRIVER    VOLUME NAME  
local     ff4cbf54d88a71c3995421cf7e4f40a4c31fd9ccc2d4eb254a2cb3dff5476a58  

- `inspect` : `docker volume inspect monVolume` permet de définir notamment où les données seront sur le host (`Mountpoint`)

>[  
    {  
        "CreatedAt": "2022-10-28T14:07:18+02:00",  
        "Driver": "local",  
        "Labels": {},  
        "Mountpoint": **"/var/lib/docker/volumes/84bd3aac927d92f08768321171098d26bcd06617de8286dd62419282f3662467/_data"**,  
        "Name": "84bd3aac927d92f08768321171098d26bcd06617de8286dd62419282f3662467",  
        "Options": {},  
        "Scope": "local"  
    }  
]

- `rm` : `docker volume rm monVolume` permet de supprimer un volume. Bien entendu, si ce volume est employé par un conteneur, Docker vous criera l'ID du conteneur associé qu'il faudra préalablement supprimer pour atteindre l'objectif demandé.

A noter, l'usage d'une dernière primitive : `prune`. Similaire à un `rm *` : il va supprimer (attention, sans trop de ménagements...) l'ensemble des volumes non rattachés à un conteneur. A manier avec précaution donc 😄 !


## Tempfs

Nous avons jusque là parlé de volume permettant de manipuler des données entre l'hôte et le conteneur au travers du filesystem. Il est parfois utile d'avoir un stockage "in memory", qui se met en place de la même manière qu'un bind mount (les données de l'image sont écrasées à partir du tmpfs)

```bash
docker run --mount type=tmpfs,destination=/foo -ti alpine sh
```

