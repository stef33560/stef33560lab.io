---
title: POC microservices
tags:
 - docker
 - microservices
---

## Principe

Le but de l'approche microservice est de segmenter un système monolythique (d'un bloc) en plusieurs morceaux plus petits. L'intérêt est de mieux maitriser tout l'environnement ; j'entends par là par exemple maitriser le cloisonnage réseau ou apporter une capacité de versionnage/mise à jour des différets services constitutifs (1 conteneur NGINX, 1 PHP, 1 mariadb, etc.) et donc de minimiser la maintenance.

Cela va également permettre de rendre la solution scalable de façon horizontale en multipliant les environnements disponibles en fonction de la demande.

[![Scalabilité horizontale](https://upload.wikimedia.org/wikipedia/commons/3/31/ScalabilityWithStaticStorage.png 'Scalabilité horizontale, Evanovertveldt, CC BY-SA 4.0')](<https://fr.wikipedia.org/wiki/Exigences_d%27architecture_technique> 'Exigences d'architecture technique')

Cette segmentation permet également d'augmenter la vitesse de déploiement puisque les services sont plus petits. La migration d'une archi traditionnelle (VMWare & co) vers une architecture orientée microservice n'est toutefois pas limpide, puisqu'elle devra revoir complètement l'architecture à l'image de la différence qu'on peut trouver entre une VM et un conteneur. Cette saine évolution permetttra à cout réduit de faire évoluer la disponibilité du service en vue de tendre vers une élimination de [SPOF](https://fr.wikipedia.org/wiki/Point_de_d%C3%A9faillance_unique) et se doter d'une capacité d'adaptation des ressources à la demande (création de conteneurs à la volée lorsque c'est nécessaire).

## Exemple de mise en place

Prenons un exemple minimaliste où on a un service qui affiche des valeurs sur une page web. Pour cela on déploie 3 conteneurs :

- 1 service d'affichage HTML (conteneur NGINX)
- 2 services de calcul (workers PHP)

Le [Dockerfile du service d'affichage](https://gitlab.com/docker33560/pages/-/blob/main/pages/docker/microservices/afficheur/Dockerfile) part de l'image officielle [NGINX Alpine](https://hub.docker.com/_/nginx/tags?page=1&name=alpine) pour installer NGINX et y copier [une conf par défaut](./microservices/afficheur/afficheur.tbz2) (hé oui, en Alpine, ça arrive tout nu !) dans un volume qui sera partagé avec d'autres conteneurs. Cette image étant dédiée au démon NGINX, on définie l'ENTRYPOINT sur ce service de façon à le démarrer automatiquement lors de l'instanciation de l'image :

 ```dockerfile
FROM alpine:3.16.2
RUN apk --no-cache add nginx
VOLUME /var/www/html/
VOLUME /srv/workers
COPY conf.d/default.conf /etc/nginx/http.d/
COPY html/* /var/www/html/
ENTRYPOINT ["nginx", "-g", "daemon off;"]
```

Le [Dockerfile](https://gitlab.com/docker33560/pages/-/blob/main/pages/docker/microservices/worker/Dockerfile) des services de calcul part de l'image officielle [PHP Alpine](https://hub.docker.com/_/php/tags?page=1&name=alpine) pour y copier deux scripts :

- [calcule.sh](./microservices/worker/calcule.php) : réalise le calcul en lui-même ; les deux workers diffèrent ici par une couleur de fond, et le cas échéant un paramètre timeout entre deux exécutions
- [affichage.php](./microservices/worker/affichage.php) : met à jour la page index.html (même script sur les 2 workers)
Le point d'entrée de ces worker reste le script calcule.php lancé à l'aide de PHP.

```dockerfile
FROM php:alpine3.15
COPY calcule.php /
COPY affichage.php /
ENTRYPOINT ["/usr/local/bin/php", "/calcule.php"]
```

L'arborescence est donc la suivante :

![Arborescence du POC](./microservices/microservice1-arbo.png 'Arborescence du POC')

On construira l'image du service d'affichage via un `docker build -t afficheur .`:

>Sending build context to Docker daemon  8.192kB
Step 1/7 : FROM alpine:3.16.2
latest: Pulling from library/alpine
213ec9aee27d: Already exists 
Digest: sha256:bc41182d7ef5ffc53a40b044e725193bc10142a1243f395ee852a8d9730fc2ad
Status: Downloaded newer image for alpine:3.16.2
 ---> 9c6f07244728
Step 2/7 : RUN apk --no-cache add nginx
 ---> Running in a2fc9ffb8309
fetch https://dl-cdn.alpinelinux.org/alpine/v3.16/main/x86_64/APKINDEX.tar.gz
fetch https://dl-cdn.alpinelinux.org/alpine/v3.16/community/x86_64/APKINDEX.tar.gz
(1/2) Installing pcre (8.45-r2)
(2/2) Installing nginx (1.22.1-r0)
Executing nginx-1.22.1-r0.pre-install
Executing nginx-1.22.1-r0.post-install
Executing busybox-1.35.0-r17.trigger
OK: 7 MiB in 16 packages
Removing intermediate container a2fc9ffb8309
 ---> 577384f0439c
Step 3/7 : VOLUME /var/www/html/
 ---> Running in e09070562fb1
Removing intermediate container e09070562fb1
 ---> 26ceb1e9aac3
Step 4/7 : VOLUME /srv/workers
 ---> Running in 25aec7adf98d
Removing intermediate container 25aec7adf98d
 ---> f384b3df4111
Step 5/7 : COPY conf.d/default.conf /etc/nginx/http.d/
 ---> c1d093966312
Step 6/7 : COPY html/* /var/www/html/
 ---> d2a907631e64
Step 7/7 : ENTRYPOINT ["nginx", "-g", "daemon off;"]
 ---> Running in 8b626eb9c4c3
Removing intermediate container 8b626eb9c4c3
 ---> 316c31db0ffb
Successfully built 316c31db0ffb
Successfully tagged afficheur:latest

Celle de l'image des workers se de même `docker build -t worker .` depuis le dossier `worker`.
>Sending build context to Docker daemon   5.12kB
Step 1/6 : FROM php:alpine3.15
 ---> 25d4a8e997e0
Step 2/6 : ENV INDEXFILE='/var/www/html/index.html'
 ---> Running in c225ad5f6224
Removing intermediate container c225ad5f6224
 ---> 780c04f30d04
Step 3/6 : ENV WORKER_DIR='/srv/workers'
 ---> Running in 66b228cbce37
Removing intermediate container 66b228cbce37
 ---> 3a8990e7f506
Step 4/6 : ENV WORKER_TIMEOUT='5'
 ---> Running in a8429d02ed2c
Removing intermediate container a8429d02ed2c
 ---> 5e4364ba0f44
Step 5/6 : COPY ["calcule.php", "affichage.php", "/"]"
 ---> c5d7053f83c0
Step 6/6 : ENTRYPOINT ["/usr/local/bin/php", "/calcule.php"]
 ---> Running in 498e29ce497d
Removing intermediate container 498e29ce497d
 ---> 305a9914de1b
Successfully built 305a9914de1b
Successfully tagged worker:latest

Une fois cela fait, on peut lancer les microservices et c'est à ce moment là qu'on lie les services du workers au volume du service afficheur via `--volumes-from` : 

```bash
docker run -tid --name site afficheur
docker run --rm -tid --volumes-from site --env WORKER_TIMEOUT='5' --hostname 00ff00_w1 --name w1 worker
docker run --rm -tid --volumes-from site --env WORKER_TIMEOUT='1' --hostname 0000ff_w2 --name w2 worker
docker ps
```

Dans ce cas d'école, j'ai positionné les paramètres Docker suivants :

- `--rm` pour supprimer les conteneurs lors de leur arrêt ;  
❗️ attention, arrêt du conteneur ne veut pas dire suppression des volumes qui persisteront sur l'hôte (`docker volume rm` voir `prune` si vous êtes sûr de vous) ! 
- `--volumes-from` pour [partager](https://docs.docker.com/storage/volumes/#back-up-restore-or-migrate-data-volumes) les deux volumes définis dans le Dockerfile du conteneur site (`/var/www/html/` et `/srv/workers`)

Et notamment pour les workers, deux variables dans la ligne de commande des workers :

- `WORKER_TIMEOUT` : permet de définir en secondes la fréquence de déclenchement du worker
- `hostname` : les 6 premiers caractères serviront de couleur HTML pour les représenter sur la page web 😎
  - w1 sera vert
  - w2 sera bleu

>CONTAINER ID   IMAGE                                COMMAND                  CREATED        STATUS                  PORTS                                       NAMES
8f38f31f048f   worker                               "/usr/local/bin/php …"   Less than a second ago   Up Less than a second                                               w2
549b7d22007f   worker                               "/usr/local/bin/php …"   Less than a second ago   Up Less than a second                                               w1
6e9deff826d5   afficheur                            "nginx -g 'daemon of…"   1 second ago             Up Less than a second

J'en profite pour glisser un petit paramètre sympathique de filtre à `docker inspect` pour récupérer l'adresse IP du conteneur :

```bash
chromium "http://$(docker inspect -f '{{.NetworkSettings.IPAddress}}' site)" &
```

![Résultat avec 2 microservices](./microservices/microservice1.png '2 microservices')

Chaque exécution de worker fait l'objet d'un nouveau résultat encadré. Dans chaque encadré sont présentes autant de boites qu'il y a de workers en fonction. Vous comprenez sans doute mieux l'intérêt d'avoir utilisé des couleurs et des noms de hostnames bien connus 😄

- la couleur de fond de la boite représente le worker qui a rajouté les données dans la page présentée
- cette couleur se retrouve dans l'encadrement de chaque boite pour rappeler quel worker a fait le calcul

Dans ce cas: 

- 1ere ligne : w2 s'est executé, seul w2 a calculé un résultat
- 4e ligne : w1 s'est exécuté, il présente deux résultats
  - 1er encadré (border bleue) : c'est le résultat calculé par w2
  - 2e encadré (border vert) : c'est le résultat calculé par w1

On peut constater retrouver les temps d'excution des workers via les bpm :

- Toutes les 5 secondes pour w1
- Toutes les secondes pour w2  

Si je rajoute un worker via `docker run --rm -tid --volumes-from site --env WORKER_TIMEOUT='10' --name w3 worker`, on voit son apparition en couleur violette prise selon le hostname défini cette fois ci automatiquement par Docker (*31085f*4b7749)

![Résultat avec 3 microservices](./microservices/microservice1-3w.png '3 microservices')

je peux bien entendu en rajouter autant que je veux !

![Résultat avec 7 microservices](./microservices/microservice1-7w.png '7 microservices !')

En terme de sécurité, les services workers sont cloisonnés et inacessibles en dehors du moteur, les conteneurs dialoguent à l'aide de `--volumes-from`. Seul le conteneur site est accessible sur le port 80, en atteste le résultat de la commande `sudo nmap -sS 172.17.0.0/28` :

>Starting Nmap 7.80 ( https://nmap.org ) at 2022-10-30 19:43 CET
Nmap scan report for 172.17.0.3
Host is up (0.0000050s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
MAC Address: 02:42:AC:11:00:03 (Unknown)  
Nmap scan report for 172.17.0.4
Host is up (0.0000050s latency).
All 1000 scanned ports on 172.17.0.4 are closed
MAC Address: 02:42:AC:11:00:04 (Unknown)  
Nmap scan report for 172.17.0.5
Host is up (0.0000050s latency).
All 1000 scanned ports on 172.17.0.5 are closed
MAC Address: 02:42:AC:11:00:05 (Unknown)  
Nmap scan report for 172.17.0.6
Host is up (0.0000050s latency).
All 1000 scanned ports on 172.17.0.6 are closed
MAC Address: 02:42:AC:11:00:06 (Unknown)  
Nmap scan report for 172.17.0.7
Host is up (0.0000050s latency).
All 1000 scanned ports on 172.17.0.7 are closed
MAC Address: 02:42:AC:11:00:07 (Unknown)  
Nmap scan report for 172.17.0.8
Host is up (0.0000050s latency).
All 1000 scanned ports on 172.17.0.8 are closed
MAC Address: 02:42:AC:11:00:08 (Unknown)  
Nmap scan report for 172.17.0.9
Host is up (0.0000050s latency).
All 1000 scanned ports on 172.17.0.9 are closed
MAC Address: 02:42:AC:11:00:09 (Unknown)  
Nmap scan report for 172.17.0.10
Host is up (0.0000050s latency).
All 1000 scanned ports on 172.17.0.10 are closed
MAC Address: 02:42:AC:11:00:0A (Unknown)  
Nmap done: 14 IP addresses (8 hosts up) scanned in 1.87 seconds
