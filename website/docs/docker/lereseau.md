---
title: Le réseau
tags:
 - docker
 - network
 - réseau
---

<details><summary>&#128172; Synthèse rapide pour les gens &#127939;</summary>

- Lister les réseaux existants : `docker network ls`
- Regarder la configuration IP d'un réseau `docker network inspect -f '{{.IPAM.Config}}' <reseau>`
- Créer un réseau : `docker network create -d <type> --subnet <IP>/<masque> <nom>`
  Drivers disponibles
  - `bridge` pour nater la passerelle Docker de l'hôte
  - `host` pour ne voir que l'ôte
  - `none` pour l'isoler de tout
  - `macvlan` ou `ipvlan` pour exposer le conteneur "devant" la couche docker
- Gérer le raccordement d'un conteneur
  - Attacher : `docker network connect <reseau> <conteneur>`
  - Détacher : `docker network disconnect <reseau> <conteneur>`

</details>

## Principe de base

Il existe 3 réseaux de base gérés par le moteur docker que l'on peut visualiser par un `docker network ls` :
>NETWORK ID     NAME      DRIVER    SCOPE
014b19c9123c   bridge    bridge    local
3bd89c323102   host      host      local
698d7b85f0c5   none      null      local

- `none` : le conteneur est dans cas isolé de tout le reste (aucune communication) ; c'est particulièrement pratique pour intégrer des composant devant être airgapped
- `host` : le conteneur est isolé de tout sauf de l'hôte ; il ne pourra pas communiquer directemnt avec les autres conteneurs.
- `bridge` : c'est le réseau principal, appelé docker0, dont la valeur par défaut est hardcodé dans Docker
  A noter qu'il existe des "super-bridge", `overlay` permettant aux conteneurs de communiquer au sein d'un cluster (donc depuis des hôtes distincts)
- `net` : permet de spécifier explicitement vers quel autre conteneur la communication peut être réalisée `--net container:<autreConteneur>`
- `IPvlan` et `Macvlan` : permet de positionner le conteneur sur le réseau de l'hôte, au même niveau que les autres machines et pas sur le sous réseau des Docker (l’interface hôte porte plusieurs adresses).

Par défaut, tous les conteneurs sont créés dans le bridge par défaut avec une IP qui variera d'un lancement à l'autre dans le périmètre du masque, consultable via un `docker network inspect -f '{{.IPAM.Config}}' bridge`. Point d'attention : les conteneurs devront donc communiquer par leur nom vu que leur IP peut changer à chaquer lancement.

## Exercices pratiques

### Réseau none

Où comment se retrouver dans un conteneur sans racines ([none](https://docs.docker.com/engine/reference/run/#network-none)) !

['Rey orpheline'](./networking/none.png 'déconnecté')

```bash
docker run -tid --name c0 --network none alpine:3.16.2
docker exec c0 ip a
```

Seule l'interface de loopback existe, pas de réseau branché.
> 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever

### Réseau type bridge

Chaque réseau créé a donc avoir sa propre plage d'adresse IP et y conteneir autant de conteneurs que son masque le permet ; exemple avec ce réseau `mysubnet`

![Réseau bridge, la famille](./networking/bridge.png 'Réseau bridge, la famille')

Créons un réseau mysubnet pouvant contenir 6 conteneurs :

```bash
docker network create -d bridge --subnet 172.171.170.0/29 mysubnet
```

Pour lancer un conteneur dans ce réseau, rien de plus simple :

```bash
docker run -tid --name c1 --network bridge alpine:3.16.2 
```

Créons maintenant 2 autres conteneurs c2 et c3

```mermaid
erDiagram
          bridge ||--|{ c1 : contient
          mysubnet ||--|{ c2 : contient
          mysubnet ||--|{ c3 : contient            
```

```bash
docker run -tid --name c2 --network mysubnet alpine:3.16.2 
docker run -tid --name c3 --network mysubnet alpine:3.16.2 
```

Chaque conteneur prends alors une IP des subnets auquels il est raccorché. Ici `c2` n'appartient qu'à un subnet :

```bash
docker exec c2 ip a
```

Son réseau est :
> 134: eth0@if135: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP
    link/ether 02:42:ac\:ab:aa:02 brd ff:ff:ff:ff:ff:ff
    inet 172.171.170.2/29 brd 172.171.170.7 scope global eth0
       valid_lft forever preferred_lft forever

Les réseaux étant isolés les uns des autres, les conteneurs d'un réseau ne pourront pas communiquer avec les conteneurs des autres réseaux. Illustration :

```bash
docker exec c0 ping -qc1 172.171.170.2 
docker exec c0 ping -qc1 8.8.8.8
```

>--- 172.171.170.2 ping statistics ---
1 packets transmitted, 0 packets received, 100% packet loss
PING 8.8.8.8 (8.8.8.8): 56 data bytes
64 bytes from 8.8.8.8: seq=0 ttl=114 time=20.677 ms  
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 20.677/20.677/20.677 ms

`c1` étant dans un subnet isolé de `c2`, il ne peut communiquer avec lui ; pour autant, en passant par la gateway du host, il peut communiquer avec Internet.

Il en va de même avec la résolution de noms :

```bash
docker exec c1 ping -qc1 c1 
docker exec c3 ping -qc1 c1 
```

là où `c1` réponds qu'il ne voit pas de quoi on parle....

> ping: bad address 'c2'

...`c3` est bien au courant de qui est `c2` :

> 1 packets transmitted, 1 packets received, 0% packet loss

### Réseau host

Dans ce cas, le conteneur ne voit que l'hôte ([host](https://docs.docker.com/engine/reference/run/#network-host)).

!['Je suis ton père'](./networking/host.png 'Réseau host, je suis ton père')

```bash
docker run -tid --name c4 --network host alpine:3.16.2 
```

`c4` ne verra donc que son hôte :

```bash
docker exec c4 ping -qc1 c0
docker exec c4 ping -qc1 c1
docker exec c4 ping -qc1 172.17.0.1
```

>ping: bad address 'c0'  
ping: bad address 'c1'  
PING 172.17.0.1 (172.17.0.1): 56 data bytes
--- 172.17.0.1 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss

### IPvlan et Macvlan

Il est déjà utile de comprendre la subtilité entre ces deux types :

- Macvlan permet de configurer des sous-interfaces ("slave devices") de l'interface physique de l'hôte en partageant son adresse IP/MAC. Les conteneurs son groupés par sous-interface, connectés directement au réseau physique avec leur propre adresse MAC et une adresse IP distincte de l'hôte. La limite du nombre de conteneur groupé est fixé par la capacité de l'interface réseau de l'hôte.
- IPvlan permet églaement à chaque conteneur de cohabiter au sein d'un même subnet, mais l'adresse MAC est identique à l'hôte.

![IPvlan et MACvlan](./networking/IPvlan-vs-macvlan.png 'IPvlan et MACvlan')

Pour la mise en place, première chose, récupérer les informations sur la route principale employée par l'hôte :

```bash
ip -o a | grep  $(route | grep '^default' | grep -o '[^ ]*$')
```

>2: enp0s31f6    inet 192.168.1.10/24 brd 192.168.1.255 scope global dynamic noprefixroute enp0s31f6\       valid_lft 51418sec preferred_lft 51418sec
2: enp0s31f6    inet6 2001:861:3100:b910:6519:864e:4e28:6a5e/128 scope global dynamic noprefixroute \       valid_lft 3445sec preferred_lft 3445sec
2: enp0s31f6    inet6 2001:861:3100:b910:32de:f00f:e8c2:7143/64 scope global temporary dynamic \       valid_lft 7191sec preferred_lft 5391sec
2: enp0s31f6    inet6 2001:861:3100:b910:b19a:a80b:9866:ba89/64 scope global dynamic mngtmpaddr noprefixroute \       valid_lft 7191sec preferred_lft 5391sec
2: enp0s31f6    inet6 fe80::d5eb:10b9:244e:503f/64 scope link noprefixroute \       valid_lft forever preferred_lft forever

Je vais donc créer deux réseau macvlan :

```bash
docker network create --driver macvlan --subnet 192.168.10.0/24 --gateway=192.168.10.254 -o parent=enp0s31f6 macvlan0
docker network create --driver macvlan --subnet 192.168.11.0/24 --gateway=192.168.11.254 macvlan0-noparent
```

Je lance un conteneur connecté sur le premier réseau

```bash
docker run -tid --name c8 --network macvlan0 alpine:3.16.2
docker exec c8 ip a
```

> 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
187: eth2@if2: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP
    link/ether 02:42:c0:a8:0a:01 brd ff:ff:ff:ff:ff:ff
    inet 192.168.10.1/24 brd 192.168.10.255 scope global eth2
       valid_lft forever preferred_lft forever

```bash
docker exec c8 ip route
```

>default via 192.168.10.254 dev eth2
192.168.10.0/24 dev eth2 scope link  src 192.168.10.1

Puis je reconnecte la VM à l'autre réseau

```bash
docker network disconnect macvlan0 c8 && docker network connect macvlan0-noparent c8
```

//TODO comprendre pourquoi les conteneurs ne sont pas accessibles
//docker exec c8  nc -lp 80 -e echo -e "HTTP/1.0 200 OK\nContent-Length: 3\n\nHi\n"

### Réseau direct

il est possible de lier fortement deux conteneurs appartenant à un même subnet :

```bash
docker run -tid --name c5 --link c1 alpine:3.16.2
```

La particularité dans ce cas est que le conteneur ainsi créé voit le conteneur lié hard-codé dans son fichiers `/etc/hosts` :

```bash
docker exec -ti c5 cat /etc/hosts
```

> 127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
**172.17.0.2      c1 d7b5149659ca**
172.17.0.3      256634b3210e


### Ajout manuels externes

Il est possible de préciser des paramètres complémentaires pour rajouter des host externes (`--add-host <nom>:<ip>`) ou pour forcer le serveur DNS à employer (`--dns`)

```bash
docker run -tid --name c6 --add-host ZeHotHost:192.168.1.201 --dns 8.8.8.8 alpine
docker exec -ti c6 cat /etc/hosts 
```

> docker exec -ti c6 cat /etc/resolv.conf /etc/hosts 
search home
**nameserver 8.8.8.8**   
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
**192.168.0.201   ZeHotHost**
172.17.0.4      278030a82248

Le DNS employé par l'hôte n'est donc ainsi pas hérité, et notre conteneur sait résoudre un nom d'hôte qui n'appartient pas à son subnet.