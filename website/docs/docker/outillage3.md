---
title: Outillages SBOM
tags:
 - docker
 - trivy
 - grype
 - syft
 - sbom
---
## Et BOuM, le SBOM

### Whatabout le sbom ?

L'intérêt de disposer d'un Software Bill of Material, une liste des composants constitutifs, a fait plus particulièrement sens avec la vulnérabilité 0day [Log4Shell](https://fr.wikipedia.org/wiki/Log4Shell) qui a mis en lumière une faille massive de la chaîne d'approvisionnement logicielle dans des milliers d'applications logicielles ; la question s'est alors posée, mais où est ce fichu composant JDNI dans mon infra ?

![Log4Shell](./outillage/log4shell.jpeg 'log4shell')

Si seuls les développeurs pythons se sentaient globalement à l'abri, log4 est un composant extrêmement répandu dans les logiciels et cette faille a créé un risque très important. Typiquement, pour en revenir à Log4Shell, ça a été un peu Dallas dans les semaines qui ont suivi la première révélation.

![Log4Shell Timeline](./outillage/log4shell_story.jpg 'Log4Shell Timeline')

Par extension, qu'on développe des logiciels pour une utilisation de besoins internes, commerciale ou que l'on déploie des logiciels tiers au sein de l'entreprise, le risque lié aux composants doit être géré : s'il n’est pas maitrisé, l'entreprise ira un jour ou l'autre au-devant de graves problèmes pouvant altérer "simplement" son image de marque et sa réputation, jusqu'à causer [des dégats financiers colossaux](https://www.datasecuritybreach.fr/equifax-le-pirate-a-plus-de-14-milliard-de-perte/) et/ou faire encourir [des sanctions réglementaires](https://www.zdnet.fr/actualites/reconnaissance-faciale-la-cnil-reclame-20-millions-d-euros-a-clearview-ai-39948788.htm). C'est sans doute pour cela que l'administration Biden a [obligé](https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/) toutes les entreprises à connaitre leur SBOM suite aux multiples cyberattaques autour de [SolarWinds](https://fr.wikipedia.org/wiki/Cyberattaque_de_2020_contre_les_%C3%89tats-Unis) et [Colonial pipeline](https://fr.wikipedia.org/wiki/Cyberattaque_de_Colonial_Pipeline). C'est d'ailleurs de cette partie du globe que vient [le référentiel](https://www.ntia.gov/SBOM), mentionnant les noms des composants, leurs versions, leurs origines et la ou les licences associées.

Faire confiance est nécessaire, mais comme dirait l'autre ça n'exclue donc pas le contrôle ; par pour rien que les anglais traduisent ça par "Trust is good, control is better"... il est primordial (vital ?) de vérifier les logiciels durant son cycle de vie, afin de s'assurer de la liste des ingrédients qui le compose (SBOM), permettant ainsi d’améliorer la sécurité des logiciels à toutes étapes de la chaine développement du logiciel, une tendance confirmée sur 2023 par le dernier rapport [Linux Foundation](https://www.linuxfoundation.org/research/the-state-of-software-bill-of-materials-sbom-and-cybersecurity-readiness), tendance sans doute infléchie grâce aux efforts qu'elle-même réalise autour de [SPDX](https://fr.wikipedia.org/wiki/SPDX).

### Action ...

Il est possible de génerer cette Software Bill of Material avec trivy simplement en redéfinissant le format de sortie :

```bash
trivy image --list-all-pkgs --ignore-unfixed --format spdx-json aquasec/trivy -o trivy_sbom.trivy.json
```

Nous allons laisser de côté Trivy, ainsi que Grype qui ne sait pas non plus faire cela, pour introduire l'appel au cousin de ce dernier : Syft.
![Syft](<https://user-images.githubusercontent.com/5199289/136844524-1527b09f-c5cb-4aa9-be54-5aa92a6086c1.png> 'Syft')
![](https://static.scarf.sh/a.png?x-pxid=0e766244-d3e9-4d22-86ae-13be0bddc9d2)

Syft s'installe [de la même manière que Grype](https://github.com/anchore/syft#installation) et s'exécute aussi tant sur des dossiers (`syft dir:./`) que sur des images (et le cas échéant sur toutes ses couches avec `--scope all-layers`):

```bash
syft -o spdx-json aquasec/trivy:latest --file /tmp/trivy_sbom.syft.json
```

:::tip Format de sortie de Syft
Il peut être modifiée à l'instar de son couson à l'aide de l'option `-o` pour présenter le rapport [sous différentes formes](https://github.com/anchore/syft#output-formats).

Par exemple `-o table --file /tmp/syft_table.txt` pour générer une vue [plus humainement compréhensible](./outillage/syft_table.txt).
:::

Là où nous allons aller plus loin avec Syft, c'est que nous allons signer notre SBOM. Ainsi signée, cette attestation, ce manifeste pour les Javaistes, pourra être ajouté à une image Docker.

Pour cela, il faudra d'abord utiliser la clé privée d'une signature générée par [COSIGN](https://github.com/sigstore/cosign) (GPG pas encore supporté...).
![Cosign](<https://raw.githubusercontent.com/sigstore/community/main/artwork/cosign/horizontal/color/sigstore_cosign-horizontal-color.svg> 'Cosign')

Prenons l'exemple de l'image docker de ce site, présente sur mon repo privé, pour laquelle je vais générer une attestation et l'y attacher :

```bash
docker login docker.io
syft attest --key ~/.ssh/cosign.key -o spdx-json stef33560/pages:latest --file stef33560_pages_latest.json
cosign attach attestation --attestation stef33560_pages_latest.json stef33560/pages:latest 
```

Cette dernière opération rajoute un tag à mon image :
!['Layer attestation Syft'](./outillage/syft_layer.png 'Layer rajouté par Syft')

### ... réaction !

Jusqu'à présent, nous avions analysé l'image pour déterminer les failles qu'elles contenaient. Là où ça devient fun, c'est qu'il est possible de lister les CVE avec juste la SBOM 🤯. Attacher la SBOM signée à l'image Docker prends alors à tout son sens puisque chacun étant libre de récupérer ce manifeste, cette attestation, et de s'assurer de sa viabilité à l'aide de la clé publique, on peut alors en sortir les CVE sans devoir analyser l'image :

<a name="decodage"></a>

```bash
cosign verify-attestation --key /tmp/cosign.pub --type spdxjson stef33560/pages:latest --output-file /tmp/got_attestation.json
```

:::tip décodage attestation
Cette attestation est encodée base64 et signée, aussi il ne sera pas aisé pour nous de le lire 🙂 Une fois décodé, `jq` nous permettra d'extraire ce qui nous interesse : `cat /tmp/got_attestation.json| jq '.payload | @base64d | fromjson | .predicate' > /tmp/attestation.jq.son`
:::

Il nous est confirmé que la signature est OK :
>Verification for stef33560/pages:latest --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key

A titre de comparaison de résultat, nous allons continuer de réaliser l'exercice sur l'image `aquasec/trivy`. Dans la mesure où l'image ne contient pas d'attestation, nous allons nous autoriser d'en créer une (formatée JSON [ici](./outillage/attestation.jq.json)) pour ensuite demander à Grype de nous sortir les CVE.

```bash
syft attest --key ~/.ssh/cosign.key -o spdx-json aquasec/trivy:latest --file /tmp/attestation.json
grype att:/tmp/attestation.son --key ~/.ssh/cosign.pub`
```

Le résultat est le suivant :
>✔ Vulnerability DB        [no update available]
 ✔ Attestation verified
 ✔ Scanned image           [5 vulnerabilities]  
[0000]  WARN some package(s) are missing CPEs. This may result in missing vulnerabilities. You may autogenerate these using: --add-cpes-if-none
NAME                        INSTALLED   FIXED-IN  TYPE       VULNERABILITY   SEVERITY
busybox                     1.35.0-r17            apk        CVE-2022-28391  High
google.golang.org/protobuf  v1.28.1               go-module  CVE-2015-5237   High
google.golang.org/protobuf  v1.28.1               go-module  CVE-2021-22570  Medium
zlib                        1.2.12-r3             apk        CVE-2022-37434  Critical  
zlib                        1.2.12-r3             apk        CVE-2018-25032  High

:::tip sbom et attestations
Il est également possible d'obtenir le même résultat à partir du seul SBOM [décodé](#decodage) par `grype sbom:/tmp/attestation.jq.json`
:::

Corollaire assez intéressant à ce résultat : Grype remonte 5 CVE là où Trivy en remontait 4 et ... ce ne sont pas les mêmes 🍿 (CVE-2020-8911, CVE-2020-8912, GHSA-7f33-f4f5-xwgw et GHSA-f5pg-7wfw-84q9).

Ce constat est parfait pour ma conclusion.

## OK, et sinon à quoi ça sert ?

C'est bien beau d'avoir un SBOM et une automatisation du check SSI des assets, mais ça n'est pas tellement différent d'une cinquantaine de classeur d'une norme ISO quelconque enfermée dans une armoire : ça n'est utile que si l'entreprise s'en sert ! Il faut avoir une vraie équipe de DevSec qui les traite derrière, de surcroit car les outils donnent des résultats qui doivent être critiqués et pas simplement pris comme parole d'évangile. Cette équipe adossée aux [SRE](https://fr.wikipedia.org/wiki/Site_Reliability_Engineering) doit investguer en amont et en aval de la livraison en production, en menant des actions correctives comme de la "simple" mise à jour des composants. Mais il existe des cas particuliers qu'on peut ainsi regrouper :

- vulnérabilité déjà fixée par un patch (faux positif)  
La migration vers une nouvelle version d'un asset est parfois trop coûteuse, aussi un patch de la librairie référencée sur le numéro de version porte à confusion (exemple : `lib-1.3-1-r1`). Dans ce cas, il faut ajouter whilelister la vulnérabilité (type fichier `.trivyignore`).
- vulnérabilité sur des composants non utilisés
Un conteneur étant construit à partir d’autres conteneur, l'héritage des composants vulnérables est réelle mais ils ne sont pas forcément utilisés dans le cadre de notre image. Ignorer la  n'est pas une bonne option s'il advenait à être utilisé plus tard! Comme je l'ai précisé par ailleurs, il est préférable d’optimiser l’image en supprimant les composants inutiles.
- vulnérabilité présente mais non exploitable directement dans le contexte
Avant d’ignorer une telle vulnérabilité, il faut garder à l’esprit qu’un attaquant exploite différentes failles pour atteindre son objectif. L’attaquant sera donc amené à modifier le contexte de l’application avant d’exploiter la vulnérabilité aussi il est recommandé de mettre à jour le composant afin de ne plus être exposé à cette vulnérabilité
- vulnérabilité non fixée ([0-day](https://fr.wikipedia.org/wiki/Vuln%C3%A9rabilit%C3%A9_zero-day)).
Lorsque le correctif n'est pas encore disponible, certao,s l'ajoutent forairement en whiteliste (fichier .trivyignore) en attendant le correctif. Cependant mon avis (qui vaut ce qu'il vaut) est de le conserver dans le rapport pour ne pas l'oublier.

En synthèse, voici 4 options sont possibles pour traiter une vulnérabilité sur un composant vulnérable:

- Si on doit conserver, le mettre à jour
- Si on ne l'utilise pas, le supprimer
- S'il est patché (faux positif), le whiltelister pour l'ignorer
- Si aucune correction, selon la politique de l'entreprise, soit versionner son usage soit le whitelister