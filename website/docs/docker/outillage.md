---
title: Outillages Analyse 
tags:
 - docker
 - Dockerfile
 - trivy
 - grype
 - cve
---

On aura beau être attentif, un peu d'aide automatique ne fait jamais de mal. Voici quelques outils qui sont ou seront indispensables dans votre quotidien !

## Un Dockerfile propre (Linter)

En phase de dev, [Hasker Dockerfile Linter](https://github.com/hadolint/hadolint) est l'outil idéal pour cela. Il peut s'exécuter tant en binaire qu'en docker et peut être utilisé directement dans VSCodium. 

![Hasker Dockerfile Linter](./outillage/hasker_linter.png 'hasker_linter.png')

Pour l'installer, rien de plus simple :

1. Tirer l'image `docker pull hadolint/hadolint:latest-alpine`
1. Créer le fichier wrapper dans `/usr/local/bin/hadolint` tel que [la doc](https://github.com/hadolint/hadolint#install 'documentation hadolint') le prévoit
   A cette étape, vous devriez déjà dans un shell pouvoir appeler ce wrapper sur un fichier Docker pour vous sortir quelquechose.
1. Installer le plugin [exiasr.hadolint](https://marketplace.visualstudio.com/items?itemName=exiasr.hadolint)
   Au lancement, le plugin vous demande où est le launcher, il n'y a qu'à préciser le wrapper créé juste avant.

et tadaaa !

![Hadolint en action](./outillage/hadolint.png 'Hadolint en action')

Nous verrons plus tard qu'il est aussi possible d'intégrer ce genre de modifications dans une CI.

## Une image bien constituée (Dive)

Même en essayant de respecter les bonnes pratiques [dont j'ai parlé](./fichierdockerfile.md#points-clé), il n'est pas acquis ce marche à tous les coups. Analyser les couches manuellement s'avère aussi fastidieux : décompresser (untar) une archive permet de récupérer autant de dossier que de layers, rendant leur parcours et comparaisons loin d'être aisé. C'est là qu'intervient [Dive](https://github.com/wagoodman/dive) simplifier ce travail : cet outil open source permet de naviguer entre les couches d’une image Docker, de mettre en évidence immédiatement les changements entre les couches (ajouts, modification ou suppression), et même d’estimer l’efficacité d’une image et d’une intégration à une pipeline de CI. Pensez donc au passage à [remercier l'auteur](https://www.paypal.com/paypalme/wagoodman 'Thanks William ! 💕') !

Plusieurs méthodes d’utilisation sont disponibles, par exemple, en tant que binaire ou en tant qu’image Docker (qui a dit la poule ou l'oeuf ? 😄) ; utilisons la version Docker pour analyser notre précédente image tmp:0.2

```bash
docker pull wagoodman/dive
docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest tmp:0.2
```

Dans la mesure où cette image s'exécute dans le conteneur, elle n'est pas capable d'accéder autres images. Pour cela il faut partage l'API du host, au socket

:::danger partage du Socket
Donner accès à un conteneur revient à lui donner un accès `root` à l'hôte (Docker in Docker aka DinD), voir le cas échéant à escalader cet accès `root` au cluster !
:::

![Ecran accueil Dive](./outillage/dive.png 'Ecran d accueil Dive')

L'encadré 'layers' permet de naviguer simplement d'une couche à l'autre et de déterminer les actions réalisées et l'impact sur le filesystem dans la fenêtre 'current layer contents' à l'aide d'un filtre ou, de base, d'une coloration syntaxique du plus bel effet :

- vert : nouveaux fichiers,
- jaune : fichiers édités
- rouge : fichiers supprimés

A ne pas oublier, l'encart à bas à gauche donne l'"efficacité" d'un layer, traduisant la perte d'espace engendrée.