---
title: Outillages CVE
tags:
 - docker
 - trivy
 - grype
 - syft
 - sbom
---
## Détection des failles de sécurité (CVE)

### Pourquoi

Avec Dive, on a vu qu'il n'est pas toujours aisé de suivre manuellement tous les éléments consitutifs d'une image. Alors autant dire qu’il est impossible de suivre les vulnérabilités associées ! MAIS HEUREUSEMENT, des outils de détection de vulnérabilités existent. A l'aide de ces outils, nous allons pouvoir classifier les vulnérabilités qui sont de différents types :

- "Vraies" vulnérabilités  
  Ce qu'elles soient contenues dans des paquets tant d'OS, de [langages](https://aquasecurity.github.io/trivy/v0.24.4/vulnerability/detection/language/), des Reposotories GIT  à des clés SSH faibles
  Ses sources proviennent de [différentes sources de CVE](https://aquasecurity.github.io/trivy/v0.34/docs/vulnerability/detection/data-source/) 
  ❗️Attention, les outils ne supportent pas les builds propriétaires...
- Mauvaises configurations  
  Et ce qu'elles soient dans :
  - des fichier plats malformés (YAML, JSON, XML & co), ceci pouvant par exemple introduire [des XSS](https://fr.wikipedia.org/wiki/Cross-site_scripting)) 
  - des configurations d'[IaC](https://fr.wikipedia.org/wiki/Infrastructure_as_code) (docker OK, cluster K8s encore en bêta). Pour le récupérer, il existe [différentes méthodes](https://github.com/aquasecurity/trivy#get-trivy)
- Mots de passe exposés en clair  
  On parle ici de token GitLab, Slack, AWS, [etc](https://github.com/aquasecurity/trivy/blob/main/pkg/fanal/secret/builtin-rules.go)

### Comment ça marche

Afin de comprendre le fonctionnement de ce genre de produit, voici globalement les étapes imaginées par le créateur de Trivy, Teppei Fukuda ([knqyf263](https://github.com/knqyf263)) avant même qu'Aquasec acquière la solution :

```mermaid
graph TD
    A[Lancement] -->|vuln-list-update| B(Collecte des données)
    B --> |Liste de CVE| C[Liste JSON]
    C --> |vuln-list| E{Base inexistante}
    E --> |Non| F[Création de la base]
    F --> |Mise en cache du repository vuln-list| G[Cache]
    G -->|Import NoSQL| H[Base NoSQL 'Bolt']
    E --> |Oui| I[Mise à jour de la base]
    I --> H
```

[vuln-list-update](https://github.com/aquasecurity/vuln-list-update) met à jour quotidiennement [vuln-list](https://github.com/aquasecurity/vuln-list) via une CI Travis, ce dernier dépot présentant les sources de mise à jour.

## Les outils

Il existe plusieurs outils traitant ces aspects, comme [Anchore](https://github.com/anchore/harbor-scanner-adapter) ou [Clair](https://github.com/quay/clair), voir [dagda](https://github.com/eliasgranderubio/dagda) mais nous allons nous intéresser ici à Trivy et Grype.

### Trivy

![Aquasec\/Trivy](https://github.com/aquasecurity/trivy/raw/main/docs/imgs/logo.png 'Aquasec/Trivy')  
![](https://static.scarf.sh/a.png?x-pxid=2bf4acd8-6d89-4863-ae96-de5af6a6e81e)

Son installation de base est assez triviale :

```bash
curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.33.0
```

Tant et si bien qu'on peut déjà envisager de l'utiliser un peu partout comme directement [dans une étape de CI](<https://aquasecurity.github.io/trivy/v0.34/tutorials/integrations/gitlab-ci/> 'Exemple d\'intégration à une GitLab CI'). Même si cet exemple n'est pas de mon point de vue la meilleure méthode, tout bon "DevSecOps" vous dira que quoi qu'il arrive, la sécu, ça s'automatise 😄

![DevSecOps](./outillage/devsecops.jpeg 'It all started with Ops... and now  ¯\_(ツ)_/¯')

 Cet outil est aussi aisément [dockerisable](https://gitlab.com/docker33560/trivy/-/blob/main/Dockerfile), commande que je résumerais d'ailleurs par un simple `trivy` pour plus de confort de lecture dans ce qui suivra :

```bash
docker run --rm \
  --network="host" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /opt/trivy:/home/trivy/.cache \
  -v /root/.docker/config.json:/root/.docker/config.json \ 
  stef.docker.scarf.sh/docker33560/trivy
```

:::tip Explication des options Docker

1. le partage du socket : indispensable pour scanner des images locales (et uniquement elles). Attention, trivy passe donc root...
1. le partage du dossier : nécessaire pour éviter le download de la base de donnée à chaque analyse.  
1. le partage du fichier de configuration docker : nécessaire pour scanner les images sur des registres nécessitant authentification
  Pensez à préalablement vous logguer via un `docker login`
:::

:::tip Isolation du réseau
Dans le cas d'une install [airgapped](https://aquasecurity.github.io/trivy/v0.34/docs/advanced/air-gap/), il faut rappatrier [la base](https://github.com/aquasecurity/trivy-db/releases/latest) et lancer trivy avec `--skip-update` pour l'empêcher de chercher à la mettre à jour. Voir [cette section](#trivy-airgapped)
:::

Trivy permet de scanner :

- une image docker (locale ou distante), en l'occurence ici l'image docker de Trivy : `trivy image aquasec/trivy:latest`  
  ![Résultat d\'analyse de Trivy](./outillage/trivy_on_trivy.png 'Résultat d'analyse de Trivy')
- un filesystem, en l'occurence pour l'illustration le dossier où j'ai cloné [du repository trivy](https://github.com/aquasecurity/trivy) : `trivy -v $PWD:/scanme fs /scanme`  
  ![Résultat d\'analyse de Trivy sur... son repo cloné en local](./outillage/trivy_on_trivy_fsgitlab.png 'Résultat d'analyse de Trivy sur... son repo cloné en local')
  :::tip Scan de repo
  Ce scan peut aussi être réalisé directement via l'option `trivy repo "https://github.com/aquasecurity/trivy"` (trivy s'occupe du clone !)
  :::
- un Dockerfile, en se positionnant dans le dossier où est présent le Dockerfile : `trivy -v $PWD:/scanme aquasec/trivy config /scanme`  
  ![Résultat d\'analyse de Trivy sur un Dockerfile](./outillage/trivy_on_dockerfile.png 'Résultat d'analyse de Trivy sur un Dockerfile')

:::tip des options à retenir

- dumper l'ensemble des paquets employés  : `--list-all-pkgs --ignore-unfixed`
- restreintre la quantité d'alertes : `--severity HIGH,CRITICAL`
- rediriger la sortie vers un fichier : `--output`
:::

En dehors de ce mode standalone, il est très intéressant de noter qu'il est possible d'utiliser Trivy en client/serveur :
[![Trivy Architecture client serveur](<https://aquasecurity.github.io/trivy/v0.22.0/imgs/client-server.png> 'Trivy Architecture client serveur')](https://aquasecurity.github.io/trivy/v0.22.0/advanced/modes/client-server/)

Ce [Dockerfile](https://gitlab.com/docker33560/pages/-/blob/main/pages/docker/outillage/trivy-server/Dockerfile) permettra de le manipuler sous cette forme plus aisément, et avoir un process qui ne tourne pas en `root` :

```dockerfile
FROM alpine:3.16.2
ARG TRIVY_VERSION="0.34.0"
RUN apk --no-cache add ca-certificates git curl && \
    curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v$TRIVY_VERSION
EXPOSE ${PORT}
RUN addgroup -S trivy && adduser -S trivy -G trivy
USER trivy
ENTRYPOINT ["trivy", "server", "--listen", "0.0.0.0:4954"]
```

```bash
docker network create -d bridge trivy
docker run --rm  \
  --name trivy-server  \
  --network="trivy"  \
  -v /opt/cache:/home/trivy/.cache  \
  trivy-server --skip-update
```

Le serveur est lancé
>2022-10-31T14:43:52.523Z	INFO	Need to update DB
2022-10-31T14:43:52.523Z	INFO	DB Repository: ghcr.io/aquasecurity/trivy-db
2022-10-31T14:43:52.523Z	INFO	Downloading DB...
2022-10-31T14:43:55.689Z	INFO	Listening 0.0.0.0:4954...

C'est parti pour la première analyse en utilisant le serveur, une fois n'est pas coutume sur l'image `nginx:alpine` :

```bash
docker run --rm  \
  --network="trivy"  \
  -v /root/.docker/config.json:/root/.docker/config.json \
  -v /var/run/docker.sock:/var/run/docker.sock \
  stef.docker.scarf.sh/docker33560/trivy  \
  --server  \
  "http://trivy-server:4954"  \
  image nginx:alpine
```
![Trivy sur nginx:alpine](./outillage/trivy-server_on_nginx-alpine.png 'Trivy sur nginx:alpine')

Pour l'exercice, interrogeons maintenant notre serveur avec l'image `afficheur:latest` construite [lors du POC microservices](./microservices.md#) ([Dockerfile du service d'affichage](https://gitlab.com/docker33560/pages/-/blob/main/pages/docker/microservices/afficheur/Dockerfile)) :

```bash
docker run --rm \
  --network="trivy" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  stef.docker.scarf.sh/docker33560/trivy \
  --server "http://trivy-server:4954" \
  image afficheur:latest
```

![Trivy sur image locale](./outillage/trivy-server_on_localimage.png 'Trivy sur image locale')

Vous constatez ici les écarts qu'il peut y avoir entre les images custom et celles du Dockerhub : notre image `afficheur:latest` a été construite sur la base de `alpine:3.16.2`, nous n'y avons ajouté que nginx 1.22.1 et aucune faille liée à NGINX ne resort : la surface d'attaque de l'image `nginx:alpine` est donc plus importante car elle embarque `curl`.

### Trivy airgapped

Récupérer la base :

```bash
docker run --rm \
  --network="host" \
  -v /opt/cache:/home/trivy/.cache \
  stef.docker.scarf.sh/docker33560/trivy image --download-db-only
```

Puis lancer l'analyse :

```bash
docker run --rm \
  --network="none" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /opt/cache:/home/trivy/.cache \
  stef.docker.scarf.sh/docker33560/trivy --skip-update  image local:image
```

### Grype

[Grype](https://github.com/anchore/grype) est une solution très proche de Trivy aussi je ne m'étendrais pas trop sur ce sujet.
![Grype](https://user-images.githubusercontent.com/5199289/136855393-d0a9eef9-ccf1-4e2b-9d7c-7aad16a567e5.png 'Grype')
![](https://static.scarf.sh/a.png?x-pxid=b24c1ec2-75f5-4add-80f2-eb9dc82cd29c)

[Certaines différences](https://gitlab.com/gitlab-org/gitlab/-/issues/327174) feront que les résultats ne seront pas tout à fait les mêmes, on touche d'ailleurs là un défaut de ce genre de solutions...

Pour installer ce produit qui prends en charge [pas mal de monde](https://github.com/anchore/grype#features), rien de plus simple :

```bash
curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sudo sh -s -- -b /usr/local/bin
```

Je lui préfèrerai son exécution dans un conteneur tout bête [Dockerfile](https://gitlab.com/docker33560/grype/-/blob/main/Dockerfile) qui, à l'instar de trivy, s'exécutera sur un dossier (`grype dir:<theFolder>`) ou sur sur une image :

```bash
docker run --rm \
  --network="host" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v ~/.cache:/home/grype/.cache \
  stef.docker.scarf.sh/docker33560/grype:latest aquasec/trivy:latest
```

![Grype on Trivy](./outillage/grype_on_trivy.png 'Grype on Trivy')

Vous l'avez vu ? il n'y a que 2 CVE... et pas les mêmes que Trivy 😰 ?  Hé oui, deux marteaux mais pas les mêmes clous...

Suite [au prochain épisode](./outillage3.md) pour exploiter ces CVE à l'échelle !

## Intégration dans la CI

Chez GitLab, on fait les choses bien. Surtout quand on sent un bon filon 😄 La preuve en est pour intéger le scan des conteneurs, il suffit d'[un include](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml) pour rajouter un job sur un stage `test` qui va lancer `Trivy` :

```yaml
include:
  - template: Security/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CS_IMAGE: "${CI_IMAGE_NAME}:${CI_IMAGE_VERSION}"
    CS_REGISTRY_USER: $CI_REGISTRY_USER
    CS_REGISTRY_PASSWORD: "$CI_REGISTRY_PASSWORD"
```

`CS_IMAGE` va préciser l'image (FQDN, par défaut `registry.gitlab.com`) et `CS_REGISTRY_USER` et `CS_REGISTRY_PASSWORD` sont utiles dans le cas où la registry n'est pas celle de GitLab.

:::tip Préférer Grype
Pour cela, il suffit de rajouter la variable `CS_ANALYZER_IMAGE: "registry.gitlab.com/security-products/container-scanning/grype:5"`.
Notez que cette variable est utilisable avec une registry privée pour désigner l'image de trivy ou grype [que vous y aurez intégrée](https://gitlab.com/security-products/container-scanning/container_registry/).
:::

:::tip Remonter les CVE des languages
Par défaut, le scan ne remonte que les vulnérabilités sur l'(OS), et donc ses paquest (yum, apt, apk, tdnf). Si on veut aussi remonter les CVE sur les langages (ce qui est par défaut désactivé), il faut il faut préciser `CS_DISABLE_LANGUAGE_VULNERABILITY_SCAN: "false"`:
:::

Il existe nombre d'options comme par exemple [ADDITIONAL_CA_CERT_BUNDLE](https://docs.gitlab.com/ee/user/application_security/container_scanning/#using-a-custom-ssl-ca-certificate-authority) que vous utiliserez probablement si vous avez votre propre PKI pour les certicats.

Globalement ces actions, ainsi que d'autres, s'activent dans le menu [Sécurity & Compliance > Configure](https://gitlab.com/docker33560/sandbox/-/security/configuration) ; En cliquant sur les boutons, GitLab va vous proposer des Merge Request pour rajouter d'autres includes ; [beaucoup](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates/Security) d'options sont possibles, mais peu sont inscrites d'office sans l'[abonnement Ultimate](https://about.gitlab.com/pricing/feature-comparison/) à 100USD/mois/utilisateur. Une marche très haute à monter, mais excessivement pointue avec beaucoup d'aides comme [la proposition de résolution sur détection](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#resolve-a-vulnerability), ou toute la gestion de [la mise à jour des vulnérabilités](https://docs.gitlab.com/ee/user/application_security/index.html#vulnerability-scanner-maintenance), permettant de pousser votre côté Sec à son paroxysme... 