---
title: Scaled Agile Framework
tags:
 - safe
 - agile
 - lean
---
## SAFe, qu'est-ce que c'est ?

Ce n'est ni plus ni moins qu'une accumulation ordonnée de pratiques, de connaissances, de conseils, visant à structurer l'organisation d'une entreprise autour de la valeur des produits logiciels qui sont délivrés pour maximiser son business. Sinon avoir eu l'intelligence de rallier différents mondes dans un cadre méthodologique cohérent et structuré, il n'y a pas de réelle révolution dans SAFe : pas de nouvelles méthodes sorties d'on ne sait où, mais simplement un recul sur des pratiques existantes, une vision d'une organisation structurée sous une forme d'agilité lui permettant de concevoir de meilleurs systèmes et logiciels qui répondent mieux aux besoins changeants des clients. 

SAFe a quatre valeurs fondamentales qui sont :
- Alignement : toutes les équipes poursuivent le même objectif, généralement même les grands objectifs de l'entreprise à partir desquels tout se décline.
- Qualité intégrée : chaque livrable, chaque incrément de solution, est issu d'un processus qui embarque des normes de qualité tout au long de son cycle (et pas juste à la fin).
- Transparence : elle va avec la confiance, un truc pas si éloigné de la si fameuse bienveillance
- Collaboration : travailler dans le même sens, s'y investir tous ensemble, permet de créer la valeur attendue sans se disperser

## Avant SAFe, l'agilité

### "Pourquoi ça s'appelle la Terre ?"

Tout va plus vite, nous sommes dans une société de consommation dans tous les secteurs. Dans l'IT, seules les salles de marché était jusqu'à peu dans le consommable, le jetable, l'instantané mais depuis une vingtaine d'années l'idée de disposer plus vite d'un produit logiciel partiellement opérationnel a émergé. Et avec elle l'idée de l'adaptivité au changement en cours de route s'est imposée comme une évidence, tout en restant une nouvelle culture avec de nouveaux usages qu'il faut dompter et pour laquelle l'organisation traditionnelle doit évoluer pour adopter le mindset. Car l’agilité n’est pas tant une question de framework, de méthode, mais une véritable culture et philosophie :

- RESPECT parce qu’il ne peut y avoir de confiance sans le respect mutuel. Aucune raison de ne pas respecter l'autre, quelque soit son rôle dans l'organisation.
- HONNÊTETE parce que nous n’avons dans l'équipe rien à nous cacher, c'est la base de la confiance pour une communication efficace.
- CURIOSITE et HUMILITE, parce qu’on est toujours le padawan d'un autre, pour devenir chacun à notre tour le maitre jedi d'un nouveau padawan
- PARTAGE et EMPATHIE parce que l’écoute de l’autre est la base du travail en équipe.
- PRAGMATISME parce que faut pas déconner, tout ne marche pas comme dans les livres (foutus humains que nous sommes !)

Différentes méthodes agiles ont existé, et existent encore, RAD, XP, ASD & co... mais Scrum s'est imposé comme la méthode agile la plus répandue. Car c'est au milieu des années 90, Scrum est né des [nouvelles règles de développement d'un nouveau produit](https://www.slideshare.net/faimetti/the-newnewproductdevelopmentgame-french), un article issu de deux personnes reconnues dans ce domaine puis est né le Manifeste en 2001, pour n'être en fait publié qu'en 2010. 

Ce [manifeste Agile](https://agilemanifesto.org/iso/fr/manifesto.html), qui bien qu'il commence à dater (2001) n'ai pas été le premier à en parler (cf les travaux de Roger N. Nagel sur l'organisation dans la fin des années 70), ceci se traduit par un truc proche du Lean :
- aux individus et leurs interactions plutôt qu'aux processus et aux outils.
- à un logiciel fonctionnel plutôt qu’à une documentation exhaustive.
- à la collaboration avec les clients plutôt qu'à la négociation contractuelle.
- à l’adaptation au changement plutôt qu'à l'exécution d’un plan.

Globalement, les traits d'une approche agile peuvent être résumés ainsi :
- Contexte d’incertitude et de complexité ; certains diront "on sait en gros ce qu'on veut", d'autres "on sait où on veut aller, mais pas comment", mais en réalité l'agile n'est queune méthode contre la complexité
- Satisfaction du client par la création de valeur ; la valeur ; la valeur (je l'écris encore, la répétition ancre la notion... et ça va se répéter)
- Une organisation, des rôles et responsabilités bien définis ; l'Agile c'est pas le bordel couvré, tout au contraire !
- Collaboration transverse et pluridisciplinaire au-delà des organigrammes ; une équipe produit et pas projet.
- Confiance dans les collaborateurs à qui les objectifs ont été donnés ; ce qu'on appelle le management "bienveillant" ? en tout cas, tout est donné pour ne pas être toxique
- Flux de travail tiré et faibles tailles de lot ; plus grande valeur en premier le plus rapidement possible.
- Culture de la résolution de problème, du Feedback, de l’apprentissage collectif et de l’amélioration continue (qu'aurions nous fait si Deming n'était pas né ?).

### SCRUment ça marche ?

Scrum est une méthode Agile qui permet de produire la plus grande valeur métier dans la durée la plus courte. Avec cette méthode, on obtient un logiciel fonctionnel qui s'enrichie itérativement toutes les 3 à 4 semaines, à chaque sprint.

Cet enrichissement est priorisé part le métier, et l’équipe s’organise elle-même pour déterminer la meilleure façon de produire ce qui est le plus prioritaires de façon à ce qu'à chaque fin de sprint, tout le monde peut voir fonctionner le produit et décider soit de le livrer dans l’état, soit de continuer à l’améliorer pendant un sprint supplémentaire.

[![Cycle de vie SCRUM](safe/scrum.png 'Cycle de vie SCRUM')](https://www.tuleap.org/fr/agile/comprendre-methode-agile-scrum-10-minutes)

L'équipe est structurée autour :
- de rôles :
  - le product owner, il est soit le client, soit une personne représentant le client.  
  il définit les fonctionnalités du produit et ses priorités, choisit le contenu de la release, et accepte et rejette les résultats
  - le scrum master, qui représente le management de projet chargé de vérifier que les valeurs et les pratiques de Scrum sont respectées par l’équipe tout en facilitant la coopération 
  - l'équipe, composée de 3 à 8 personnes, pluridisciplinaire (architecte, concepteur analyste, développeur, testeur, ...), à à plein temps sur le projet, et auto-organiseé
- de cérémoniaux :
  - Sprint Planning : définit le but/périmètre du print et identifie/estime/attribue les tâches sur lesquelles l'équipe s'engage
  - Le Daily Scrum Meeting : de courte mélées quotidiennes (qu'ai fait hier, que fais-je aujourd'hui, qu'est-ce qui me gène ?)
  - Revue de Sprint (Rétrospective) : Essentiellement pour préparer la démo
- de livrables (artefacts) :
  - Product Backlog : le tout premier livrable constitué ; c'est la liste au père noël, les besoins du produit (fonctionnalités, fonctions, exigences, améliorations, corrections, etc.) représentées sous forme de User Story et définis à l'aide de critères d'acceptation
  - Sprint Backlog : un lot de user stories, une prévision de ce que l’équipe de développement a estimé pouvoir livrer en fin de Sprint à la suite du Sprint Planning.
  - Incrément : le logiciel en lui-même et tout ce qui gravite autour (y compris la documentation)

[![Processus SCRUM](safe/scrum-lifecycle.png 'Processus SCRUM')](https://www.jolipixel.fr/les-methodes-agiles-en-gestion-de-projet-web/)

## Et puis un consultant est passé par là

![](devops/wip.jpg)

## Et tout le monde a adhéré !