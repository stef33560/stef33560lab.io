---
title: Intégration continue
tags:
 - gitlab CI
 - docusaurus
---

## Build par défaut

Afin de builder automatiquement les contenus statiques, la [CI par défaut](https://gitlab.com/pages/docusaurus/-/blob/master/.gitlab-ci.yml) prévue dans le template Gitlab est largement suffisante.

Rappel de la structure de docusaurus :

![docusaurus tree](./docusaurus-ci/docusaurus-ci-tree.png)

La CI par défaut reprends [les étapes classique](https://docusaurus.io/fr/docs/deployment) du déploiement de Docusaurus.
- On part d'une image Node 
- stage test : si le commit n'est pas sur la branche master : 
  - `yarn install` pour rappatrier les composants
  - `yarn build` pour compiler 
- stage deploy : si le commit est sur la branche master, les étapes du stage test sont complétées par un déplacement du build vers le dossier `public` qui sera exposé 

## Améliorations

Quelques améliorations sympathiques peuvent être apportées.

### Cache

Inutile de retélécharger ce qu'on a déjà récupéré... `yarn install` récupère les composants NPM dans `website/node_modules` ; Mettre en cache ces quelques éléments en les spécfiant au début de '/.gitlab-ci.yml' permet de gagner quelques secondes au build.

```yml title='/.gitlab-ci.yml'
# allow caching for faster deployment
cache:
  paths:
    - website/node_modules/
```

### Builder direct dans `public/`

  Builder dans un dossier puis déplacer son contenu fait perdre du temps.
  Pour éviter cela, rajouter dans `package.json` une target `build:gitlab`
  
  ```json title='./package.json'
  "build:gitlab": "docusaurus build --out-dir ../public",
  ```

  Comme ceci :
  ![target build:gitlab dans package.json](./docusaurus-ci/docusaurus-ci-buildgitlab.png)

  Modifier alors la CI pour remplacer le script `yarn build` et `mv build/ public/` par simplement `yarn build:gitlab` comme ceci :
  ![target build:gitlab dans .gitlab-ci.yml](./docusaurus-ci/docusaurus-ci-buildgitlab2.png)

### Preview lors d'une merge request

  Inutile de builder systématiquement lors de l'ajout de code dans un feature-branch ; par contre dès lors qu'une Merge Request est initiée, il peut être sympa de voir le résultat avant de déployer en prod. 
  Pour cela, on rajouter un stage preview et utiliser les [environnements](https://docs.gitlab.com/ee/ci/environments/) Gitlab.

- job pages:preview:
  - condition de déclenchement : sur merge request
  - actions réalisées :
    - build des composants
    - changement dynamique de la `baseUrl` de `docusaurus.config.js`
      Ceci est utile afin de ne pas afficher de warning une fois déployé dans l'environnement GitLab intermédiaire
    - Création du build dans le dossier `public`
    - Création de l'environnement sous le nom `preview` suffixé par le nom de la feature-branch.
      Au passage, on spécifie à la volée l'URL d'accès de cet environnement
- job pages:preview:stop:
  - condition de déclenchement: merge request acceptées
  - action réalisée : dépublication de l'environnement

Voici les jobs à rajouter :

```yml title='/.gitlab-ci.yml'
stages:
  - preview
  - deploy

pages:preview:
  stage: preview
  script:
    - cd website
    - yarn install
    - sed -i "s+baseUrl.*$+baseUrl:'/-/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/public/',+" docusaurus.config.js
    - yarn build:gitlab
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_MERGE_REQUEST_IID
  environment:
    name: preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    url: https://${CI_PROJECT_NAMESPACE}.gitlab.io/-/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/public/index.html
    on_stop: pages:preview:stop

pages:preview:stop:
  stage: preview
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  allow_failure: true
  environment:
    name: preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    action: stop
  script:
    - echo "👋 stop preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}"
```
