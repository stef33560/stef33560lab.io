---
title: Gitlab Pages
tags: 
 - gitlab pages
 - docusaurus
---

## Gitlab Pages

### Cékoidonc

[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/index.html) permet de publier des sites de contenus statiques (pas d'interprétation server-side) directement depuis un repo GitLab. Pour publier ces contenus, on peut bien sur publier directement des les contenus HTML/CSS/JS mais aussi utiliser un générateur de site statique comme Gatsby, Jekyll, Hugo, Middleman, Harp, Hexo, or Brunch. 

Le build des contenus statiques est assuré par Gitlab CI, d'ailleurs des templates sont disponibles par défaut [pour la plupart des générateurs](https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/lib/gitlab/ci/templates/Pages).

### Solution technique

Dans mon cas, j'ai [retenu](https://madoke.org/blog/docusaurus-gatsby-hugo/) [Docusaurus](https://gitlab.com/pages/docusaurus) parceque je souhaitais utiliser le [MarkDown](https://docusaurus.io/fr/docs/markdown-features) pour ma doc, et concentrer mes efforts sur la doc et pas sur dans du dev de ce qui doit aller autour pour la publier. Un truc de faignants quoi :)

En outre, je voulais aussi pouvoir disposer d'un outil qui gère le [versionning](https://docusaurus.io/fr/docs/versioning) et qui permette (quand même) de pouvoir faire un peu de dev dessus sans partir quelquechose comme des composant Drupal !

### Quickstart

Une fois la [version de base](/docs/gitlab-pages/getting-started) mise en place, il suffit de pousser le dossier dans un projet portant votre nom d'utilisateur suffixé par `.gitlab.io` pour voir le contenu directement accessible sur https://username.gitlab.io; exemple de projet GitLab : `stef33560.gitlab.io`

```bash
git push --set-upstream git@gitlab.com:stef33560/stef33560.gitlab.io.git main
```

Une fois le projet poussé, il suffit de rajouter un simple [`.gitlab-ci.yml`](https://gitlab.com/stef33560/stef33560.gitlab.io/-/blob/master/.gitlab-ci.yml) pour déclencher le pipeline de construction du contenu statique :

```yml title="/.gitlab-ci.yml"
image: node:latest

stages:
  - test
  - deploy

test:
  stage: test
  script:
  - cd website
  - yarn install
  - yarn build
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

pages:
  stage: deploy
  script:
  - cd website
  - yarn install
  - yarn build
  - mv ./build ../public
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

### Customisation

On trouve pas mal de trucs à customiser déjà en 1er niveau dans `/docusaurus.config`.
Outre le blahblah aussi inutile qu'indisepensable ([`locales`](https://docusaurus.io/fr/docs/i18n/tutorial), `title`, `tagline`, `menu`, `copyright` du footer), on va pouvoir changer un premier niveau style avec [des couleurs custom](https://docusaurus.io/docs/styling-layout#styling-your-site-with-infima) dont les limites seront aisément repoussables à l'aide de modules [CSS](https://docusaurus.io/docs/styling-layout) ou de Sass/SCSS en déployant [le module qui va bien](https://docusaurus.io/docs/styling-layout#sassscss), voir de composants React avec [Swizzle](https://docusaurus.io/docs/swizzling). 

### Mise à jour

Rien de plus [simple](https://docusaurus.io/fr/docs/installation#updating-your-docusaurus-version) : il suffit de modifier le `package.json` pour référencer la version souhaitée :

```json title="/website/package.json"
{
  "dependencies": {
    "@docusaurus/core": "2.1.0",
    "@docusaurus/preset-classic": "2.1.0",
    // ...
  }
}
```
