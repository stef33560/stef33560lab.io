---
title: Ajout d'un moteur de recherche
tags:
 - docusaurus
 - moteur de recherche
---

La publication de modops ou de notes risque d'être rapidement le bordel. Aussi l'ajout d'un moteur de recherche semble totalement indispensable.

## Choix
[La doc](https://docusaurus.io/fr/docs/search) propose différentes solutions mais celle officielle ne me plait pas : dépendre d'un autre service qui à tout moment peut devenir payant a eu le don par le passé de m'agacer aussi je préfèrerai utiliser une solution 'standalone'. Là encore [plusieurs solutions](https://docusaurus.io/community/resources#search) sont dispo mais certaines sont plus agées que d'autres... et se basent sur des dépendances dépréciées ; j'ai donc opté pour https://github.com/easyops-cn/docusaurus-search-local, un module certes standalone mais surtout a priori bien maintenu ([v0.33.4](https://github.com/easyops-cn/docusaurus-search-local/releases/tag/v0.33.4) dispo il y a 6 jours au jour d'aujourd'hui ce 24/10/2022).

# Install
Rien de plus simple, rapide et efficace comme je les aime :

Ajouter le composant :
```bash 
npm install --save @easyops-cn/docusaurus-search-local
npm install
```

Puis ajouter une section "themes" dans docusaurus.config.js
```js title="/website/docusaurus.config.js"
themes: [
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      /** @type {import("@easyops-cn/docusaurus-search-local").PluginOptions} */
      ({
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        indexPages: true,
        language: ["fr", "en"],
      }),
    ],
```

Pour les options, benh [RTFM](https://github.com/easyops-cn/docusaurus-search-local#Theme-Options) !
