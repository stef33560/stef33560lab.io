// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Modops\', notes et autres élucubrations IT',
  tagline: 'De tout et de rien... mais surtout pas de M$',
  url: 'https://stef33560.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'GnuMT01', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'/*, 'en'*/],
    localeConfigs: {
      fr: {
        htmlLang: 'fr-FR',
      },
    },
  },

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Gitlab Pages de Stef',
        logo: {
          alt: 'Gitlab Pages de Stef',
          src: 'img/avatar.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'home',
            position: 'left',
            label: 'Modops',
          },
          {to: '/blog', label: 'Pense-bête', position: 'left'},
          {
            href: 'https://gitlab.com/stef33560',
            label: 'Mon GitLab',
            position: 'right',
          },/*
          {
            type: 'localeDropdown',
          },*/
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Tips',
            items: [
              {
                label: 'Modops',
                to: '/docs/home',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Stack Overflow',
                href: 'https://stackoverflow.com/users/14637068/stef',
              },
              {
                label: 'GitHub',
                href: 'ttps://github.com/Stef-33560',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/stef33560',
              },
              {
                label: 'Docker',
                href: 'https://hub.docker.com/u/stef33560',
              },
            ],
          },
          {
            title: '...& tricks',
            items: [
              {
                label: 'Notes',
                to: '/blog',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Stef33560, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/-/ide/project/stef33560/stef33560.gitlab.io/edit/master/-/website/',
        },
        blog: {
          showReadingTime: true,
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/-/ide/project/stef33560/stef33560.gitlab.io/edit/master/-/website/blog/',
          postsPerPage: 'ALL',
          blogSidebarTitle: 'All posts',
          blogSidebarCount: 'ALL',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  markdown: {
    mermaid: true,
  },
  stylesheets: [
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css'
  ],

  themes: [
    '@docusaurus/theme-mermaid',
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      /** @type {import("@easyops-cn/docusaurus-search-local").PluginOptions} */
      ({
        hashed: true,
        indexPages: true,
        language: ["fr"/*, "en"*/],
      }),
    ],
  ]
};

module.exports = config;
