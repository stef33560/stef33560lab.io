---
title: Commandes Docker
author: Stef
author_url: https://gitlab.com/stef33560
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/2841608/avatar.png
tags: [docker, commandes, synthèse]
---

Un petit résumé des commandes principales :

## Gestion des conteneurs

- Afficher les conteneurs : `docker ps` ou `docker ps -a` pour voir également les conteneurs arrétés 
- Récupérer une image : `docker pull`
- Exécuter un conteneur de façon détachée : `docker run -di --name <NomDuConteneur>`
- Exécuter une commande dans un conteneur :  `docker exec -it --name <NomDuConteneur> <commande>`
- Voir ce qui s'est passé dans un conteneur : `docker logs <NomDuConteneur>`
- Exposer des ports : `-p p_hote:p_conteneur`, d'un coté le port ouvert sur l'hôte et de l'autre celui du conteneur
- Etat réseau du conteneur :
  - ports exposés : `docker port <nomDuConteneur>`
  - IP : `docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <nomDuConteneur>`
- Gestion des données (volumes) :
  - partage avec le host : directive `-v <dossierDuHost>:<DossierDuConteneur` à insérer dans le `docker run`
  - partage par volume : `docker volume` avec `create <nomDuVolume>` pour le créer
- Fournir une ou des variables au conteneur :
  - par ligne de commande : directive `--env <clé>=<valeur>` à insérer dans le `docker run` 
  - par fichier de configuration : directive `--env-file <fichierDeVariables>` à insérer dans le `docker run`
- Supprimer
  - un conteneur : `docker rm`, le cas échéant avec un `-f` si il est toujours en fonction
  - tous ❗️ les conteneurs :  `docker rm -f $(docker ps -aq)`

## Gestion des images

- Afficher les images : `docker images` (ou `docker image ls`)
- Construire une image : `docker build -t <nomDeLimage:version> .`
- Consulter l'historique
  - de création d'une image : `docker history <image>:<version>`
  - d'un conteneur : `docker diff <conteneur>`
- Inspecter une image
  - En entier : `docker inspect <image>`
  - Les tags d'une image : `docker inspect -f '{{.RepoTags}}' <image>`
- Sauvegarder un conteneur modifié comme une nouvelle image `docker commit -m "commentaire" <conteneur> <image>:<version>`
- Publier une image sur un registre `registreDestination` :
  - Se logguer : `docker login -u <username>`
  - Versionner l'image : `docker tag <image-source>:<version> <registreDestination>/<projetGitlab>:<tagname>`
  - Pousser l'image : `docker push <registreDestination>/<projetGitlab>:<tagname>`
- Echanger une image via fichier
  - Extraire une image : `docker save -o /tmp/fichier.tar <image>`
  - Charger une image : `docker load -i /tmp/fichier.tar`
- Supprimer une image : `docker rmi <image>`

## Gestion des volumes

- Affichage : `docker volume ls`
- Inspection : `docker volume inspect <volume>`
- Suppression
  - d'un volumes : `docker rm <volume>`
  - de tous les volumes inutilisés : `docker volumes prune`

## Gestion des réseaux

- Lister les réseaux existants : `docker network ls`
- Consulter la configuration IP d'un réseau `docker network inspect -f '{{.IPAM.Config}}' <reseau>`
- Créer un réseau : `docker network create -d <type> --subnet <IP>/<masque> <nom>`
  Drivers disponibles
  - `bridge` pour nater la passerelle Docker de l'hôte
  - `host` pour ne voir que l'ôte
  - `none` pour l'isoler de tout
  - `macvlan` ou `ipvlan` pour exposer le conteneur "devant" la couche docker
- Gérer le raccordement d'un conteneur
  - Attacher : `docker network connect <reseau> <conteneur>`
  - Détacher : `docker network disconnect <reseau> <conteneur>`

## Nuke

- Suppression de tout ce qui est inutilisé (conteneurs arrétés, networks, images, cache de build) : `docker system prune`
- ❗️ Suppression de tout tout : `docker system prune -a`

## Dockerfile

- Dockerfile RTFM [ici](https://docs.docker.com/engine/reference/builder/) !