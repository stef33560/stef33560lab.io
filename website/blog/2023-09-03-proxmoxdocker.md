---
title: Docker dans LXC
author: Stef
author_url: https://gitlab.com/stef33560
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/2841608/avatar.png
tags: [docker, proxmox, oci, homelab]
---

## Plus différents que pas pareil
Proxmox a beau être beau de profil, et pas que [si on les écoute](https://www.proxmox.com/en/about/stories/story/compination?highlight=WyJkb2NrZXIiLCJkb2NrZXJpemVkIl0=), l'install de docker sur Proxmox dans tous les tutos se résume à instancier un conteneur LXC dans lequel y fourrer Docker. Même si LXC est un container qui rox du poney pour une fraction du kernel, on peut se poser légitiment la question de pourquoi procéder ainsi, et comment faire autrement.

![Docker in LXC](/blog/proxmoxdocker/inception.png 'InLXCeption')

LXC n’est pas la techno de containerisation la plus hype, et il y a plein de bonnes raisons pour avoir envie de lancer des containers Docker sur son infra Proxmox. Le Rasoir d'Ockham de ces raisons est que même si beaucoup d’applications conteneurisées auraient bien plus leur place sur un cluster de virtualisation que dans une boite docker, nombre d’éditeurs de logiciels ne gèrent plus les processus d’installation et fournissent juste une image Docker qu’ils maintiennent mise à jour. Même si [turnkey](https://www.turnkeylinux.org/) propose plusieurs tétrachiées de templates cela ne résoudra pas le problème initial, et transformera même vos adminsys tâtillons clônes de [la fameuse adjointe X. de Léonce](https://www.fix-dessinateur.com/) :

![Les trucs tout fait, ça sent le napalm](/blog/proxmoxdocker/cybersécurisé.png 'Crédit : Fix Dessinateur')
Crédits : [Fix Dessinateur, Chief Bullshit Officer Tome 2](https://www.fix-dessinateur.com/livres/)

Installer Docker directement sur la stack Proxmox n'étant pas vraiment une bonne idée (s'exécute en root !) d'autant que nous n’aurons pas ainsi la facilité d’usage de LXC avec la GUI de Proxmox, une recherche alternative s'impose donc. Nous allons voir comment lancer des images Docker comme on le ferait d’un container LXC et ainsi disposer de Docker pilotés par Proxmox. 

Si le comportement ne sera pas identique à un bin vieux `docker run` des familles, on y gagne un container complètement identique aux autres containers LXC de votre cluster, mais lancé avec une image Docker 😎.

## OCI ... iiiiiiiih
Sous la houlette de la Linux Foundation, L'[Open Container Initiatitive (OCI)](https://opencontainers.org/about/overview/) a été fondée par Docker et des acteurs majeurs de l'industrie du conteneur en juin 2015 dans le but de standardiser. Docker a donné son runtime de conteneurs [runc](https://github.com/opencontainers/runc) ainsi que toutes les bases des standards actuels, ce qui a permis d'aboutir à ce jour à deux specifications : le [runtime](https://github.com/opencontainers/runtime-spec), et la [spécification d'image](https://www.agaetis.fr/blogpost/docker-les-conteneurs-et-lopen-container-initiative-oci).

Le truc cool, c'est qu'une image OCI contient donc un bundle "filesystem" qui peut être extrait, et ensuite lancée par un outil implémentant l’OCI Runtime specification...

## Modop
### La recette

La recette décite ici pour lancer depuis l'interface Proxmox un conteneur Docker comme un conteneur LXC est fonctionnelle, mais méritera d'être adoubée par un vrai expert et pas un bidouilleur de homelab. Elle se résume ainsi :

1. Ajouter quelques dépendances
1. Allumer le Bifröst... heu, le bridge
1. Compléter jusqu'au trait avec un conteneur
1. Secouer la conf de Proxmox

et voilà, c'est prêt !

### Ajout des dépendances

Pour que la fonction OCI de LXC soit utilisable sous Proxmox VE, il manque quelques modules:
```bash
sudo apt install skopeo umoci jq
```

### Under the Bifröst

Les containers LXC s’attachent par défaut à un bridge linux qui s’appelle lxcbr0. Cependant ce bridge n’existe probablement pas sur votre installation de Proxmox VE car pas installé par défaut.

Si c'est le cas, La première étape tourne autour de `lxc-net` qui utilise `dnsmasq` pour gérer le DHCP et le DNS.

:::danger Pas le package complet DNSmasq !
`lxc-net` gère sont propre process dnsmasq, ne créez pas de confilt avec lui !

N'installez donc pas le package `dnsmasq`, mais préférez lui `dnsmasq-base` qui contient juste le binaire et la doc (et pas le service).

Seulement si vous n'avez pas ce paquet sur votre distro, rabattez-vous sur ce premier, mais pensez bien à lui couper la tête une fois installé avec un `systemctl stop dnsmasq && systemctl disable dnsmasq`
:::

Procédez donc à l'install et redémarrez le serivce lxc-net :

```bash
apt install dnsmasq-base
```

Vérifiez aussi que `/etc/lxc/default.conf` contient bien le contenu attendu concernant le bridge

> lxc.net.0.type = veth
> lxc.net.0.link = lxcbr0
> lxc.net.0.flags = up
> lxc.net.0.hwaddr = 00:16:3e:xx:xx:xx

Encore faut-il dire à LXC d'utliser ce bridge, ce qui se fait en le demandant dans le fichier `/etc/default/lxc-net` 

```bash
echo 'USE_LXC_BRIDGE="true"'>/etc/default/lxc-net
```

Et on redémarre le bouzin pour prendre en compte les modifs
```bash
systemctl restart lxc-net
systemctl status lxc-net
ip -4 -o a show lxcbr0
```

Cette dernière commande devrait vous confirmer l'existance du bridge, ici dans mon cas en 10.0.3.1/24 :

> 13: lxcbr0    inet 10.0.3.1/24 brd 10.0.3.255 scope global lxcbr0\       valid_lft forever preferred_lft forever


:::note le rézo, l'est pas bô
Je dois revenir sur ce billet et parler de la gestion de la carte réseau avec ProxMox (par défaut, pas de carte !).
:::

### Compléter jusqu'au trait

L'étape précédent était la plus compliquée, là c'est du velour grâce à OCI pour par exemple rajouter un conteneur Docker Alpine que j'appelerais laconiquement '500'.

```bash
lxc-create 500 -t oci -- --url docker://alpine:latest
```

> Getting image source signatures
> Copying blob 7264a8db6415 done
> Copying config 913cf3a39d done
> Writing manifest to image destination
> Storing signatures
> Unpacking the rootfs

Ayé, c'est prêt, vous pouvez regarder ce qu'il y a dedans `/var/lib/lxc/500/rootfs` et même déjà gouter avec ```lxc-execute 500``` :

>~ # cat /etc/os-release
>NAME="Alpine Linux"
>ID=alpine
>VERSION_ID=3.18.3
>PRETTY_NAME="Alpine Linux v3.18"
>HOME_URL="https://alpinelinux.org/"
>BUG_REPORT_URL="https://gitlab.alpinelinux.org/alpine/aports/-/issues"

![Alive](/blog/proxmoxdocker/frankenstein.jpeg 'Il est vivant !')

Alors, c'est bon ? il manque un truc non ? Oui ! L'IHM de proxmox, c'est un peu le vide sidéral... Ca fonctionne, certes, mais Proxmox ne sait pas que notre container existe et n’apparaitra donc dans l’UI !

### Shake it

On va faire croire à Proxmox que c’est un container LXC qu’il a créé lui-même en créant un fichier ad hoc dans `/etc/pve/lxc/500.conf` contenant ceci :

>arch: amd64
>cores: 1
>hostname: docker-alpine
>memory: 128
>ostype: alpine
>rootfs: /var/lib/lxc/500/rootfs

Immédiatement, il apparait :

![Magie](/blog/proxmoxdocker/proxmox-docker.png 'Magie')