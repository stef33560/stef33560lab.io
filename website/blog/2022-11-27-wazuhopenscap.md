---
title: Audit sécurité Linux
author: Stef
author_url: https://gitlab.com/stef33560
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/2841608/avatar.png
tags: [linux, oval, openscap, wazuh]
---

## Génèse

Aprés le packaging de services autour de [Trivy, Syft et Grype](/docs/docker/outillage3), m'est venue l'idée d'en packager un autre relatif au scan de nos vieilles VM Linux non conteneurisées. C'est au détour [d'un tweet](https://twitter.com/RobertStphane19/status/1590712398914408452?s=20&t=vffI09n04BWdBJtLLdu4AQ) que j'ai initié ce chantier en mode découverte.

## OpenScap

### Présentation

[OpenScap](https://www.open-scap.org/) permet de réaliser des audits de conformités, un processus permettant usuellement de déterminer si un objet donné, ici une VM, respecte bien toutes les règles énoncées dans une politique de conformité, autrement nommée [PSSI](https://www.ssi.gouv.fr/guide/pssi-guide-delaboration-de-politiques-de-securite-des-systemes-dinformation/). La politique de conformité est définie par un métier ad hoc, un éditeur, en partenariat avec des agences par exemple [STIG DISA](https://public.cyber.mil/stigs/scap/), [CIS](https://oval.cisecurity.org/), [MITRE](https://oval.mitre.org/) et le NIST ou l'[ANSSI](https://www.ssi.gouv.fr/guide/recommandations-de-securite-relatives-a-un-systeme-gnulinux/), qui implémentent des paramètres recommandés sous forme d'une liste de contrôle. Cette liste varie d'ailleurs assez considérablement d'une organisation à l'autre et même d'un système à l'autre au sein d'une même organisation ; elle pourra également être fournie au travers d'outils tiers comme [Suse Manager](https://documentation.suse.com/external-tree/en-us/suma/4.0/suse-manager/reference/audit/audit-openscap-overview.html) ou [Wazuh](https://documentation.wazuh.com/current/user-manual/capabilities/policy-monitoring/openscap/oscap-configuration.html).

Ce produit open source est en fait une collection d'outils permettant de mettre en en oeuvre et se conformer au protocole Security Content Automation Protocol ([SCAP](https://fr.wikipedia.org/wiki/Security_Content_Automation_Protocol)), protocole accesoirement certifié par le NIST (National Institute of Standards and Technology). Ce protocole vocation à standardiser certains problèmes liés à la sécurité et de fournir les moyens d'automatiser (dans une certaine mesure) :

- la recherche de vulnérabilités (CVE) ou (et surtout ?) de mauvaises configurations,
- l'évaluation de leurs impacts possibles (rapport ARF ou HTML),
- d'évaluer les politiques à adopter,
- et surtout de les remédier.

Pour illustrer, une exigence de sécurité peut par exemple imposer de ne stocker aucun mot de passe en texte brut dans un fichier, exigence qui pourra être implémentée sous forme d'une règle au format Open Vulnerability and Assessment Language (OVAL).

On retrouve notamment dans cette collection d'outils :

- un [workbench](https://www.open-scap.org/tools/scap-workbench/), un GUI permettant de créer ses propres règles de sécurité
- un [démon](https://www.open-scap.org/tools/openscap-daemon/), un scanner certifié NIST permettant d'auditer en continue ses environnements
- un [outil en ligne de commande](https://www.open-scap.org/tools/openscap-base/), `oscap`, permettant de lancer des benchmark au format Extensible Configuration Checklist Description Format ([XCCDF](https://fr.wikipedia.org/wiki/Extensible_Configuration_Checklist_Description_Format)) ou Open Vulnerability and Assessment Language ([OVAL](https://en.wikipedia.org/wiki/Open_Vulnerability_and_Assessment_Language))
- un [guide de sécurisation](https://www.open-scap.org/security-policies/scap-security-guide/) consistant en une série de templates XCCDF
- une [base de données](https://www.open-scap.org/tools/scaptimony/), SCAPTimony, permettant d'agréger tous les tests réalisés dans un écosystème donné.

Ce produit a hélas été retiré des repositories Debian et Ubuntu il y a quelques années, mais à l'heure où j'écris ces lignes il devrait faire son retour [avec BookWorm](https://packages.debian.org/search?keywords=openscap-scanner)

### Utilisation

Sur des distributions antérieures à Bookworm, il faudra repartir [des sources](https://github.com/OpenSCAP/openscap/releases) ; Partant d'une bookwork, il est aisé d'installer le produit :

```bash
apt install openscap-scanner ssg-base ssg-debderived ssg-debian ssg-nondebian ssg-applications
```

Si on comprendra aisément à quoi va servir `openscap-scanner`, les librairies `ssg-*` sont relatives aux Security Guide fourni par SCAP, dont la complétude restera à valider par l'initié (...) et qu'on pourra retrouver dans `/usr/share/xml/scap/ssg/content/`. Dans ce dossier, on retrouvera une collection de fichiers XML implémentant une série de profils pour une JRE, Fuse, Firefox, Fedora, Debian [9-11], Firefox, Chromium, CentOS [7-8].

Les différents profils disponibles peuvent être retrouvé à l'aide de `oscap info`. Par exemple pour `/usr/share/xml/scap/ssg/content/ssg-debian11-ds-1.2.xml`, seulement 4 profils référençant plus ou mois de checks (il y en a plus sur les profils RedHat par exemple):

>oscap info /usr/share/xml/scap/ssg/content/ssg-debian11-ds-1.2.xml
Document type: Source Data Stream
Imported: 2022-08-16T19:59:52  
Stream: scap_org.open-scap_datastream_from_xccdf_ssg-debian11-xccdf-1.2.xml
Generated: (null)
Version: 1.2
Checklists:
	Ref-Id: scap_org.open-scap_cref_ssg-debian11-xccdf-1.2.xml
		Status: draft
		Generated: 2022-08-16
		Resolved: true
		Profiles:
			Title: Profile for ANSSI DAT-NT28 Average (Intermediate) Level
				Id: xccdf_org.ssgproject.content_profile_anssi_np_nt28_average
			Title: Profile for ANSSI DAT-NT28 High (Enforced) Level
				Id: xccdf_org.ssgproject.content_profile_anssi_np_nt28_high
			Title: Profile for ANSSI DAT-NT28 Minimal Level
				Id: xccdf_org.ssgproject.content_profile_anssi_np_nt28_minimal
			Title: Profile for ANSSI DAT-NT28 Restrictive Level
				Id: xccdf_org.ssgproject.content_profile_anssi_np_nt28_restrictive
			Title: Standard System Security Profile for Debian 11
				Id: xccdf_org.ssgproject.content_profile_standard
		Referenced check files:
			ssg-debian11-oval.xml
				system: http://oval.mitre.org/XMLSchema/oval-definitions-5
			ssg-debian11-ocil.xml
				system: http://scap.nist.gov/schema/ocil/2
Checks:
	Ref-Id: scap_org.open-scap_cref_ssg-debian11-oval.xml
	Ref-Id: scap_org.open-scap_cref_ssg-debian11-ocil.xml
	Ref-Id: scap_org.open-scap_cref_ssg-debian11-cpe-oval.xml
Dictionaries:
	Ref-Id: scap_org.open-scap_cref_ssg-debian11-cpe-dictionary.xml

Il sera donc ainsi simple de lancer une analyse de conformité sur la base d'un profil à l'aide de `oscap xccdf eval` pour en faire une sortie dans un rapport humainement compréhensible :

```bash
oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_anssi_np_nt28_high --report rapport.html /usr/share/xml/scap/ssg/content/ssg-debian11-ds-1.2.xml 
```

A noter :

1. Ces règles étant appliquable à un OS particulier, dans une version particulière, elles ne sont pas applicables dans le cas de ma bookworm ; il faudra donc créer un [ensemble de règles spécifiques](/blog/openscap-wazuh/ssg-debian12.tbz2) à cette version de distribution si l'on veut voir les check passer du statut `notapplicable` au statut appliqué
1. Par défaut cette distro n'est pas encore stable, donc non labellisée Debian 12. En guise de sandbox, il faudra donc modifier en conséquence le fichier `/etc/debian_version` pour voir ces règles s'appliquer

Le rapport ainsi produit fourni un certain nombre d'écarts à corriger. Exemple [sur une Debian 9](/blog/openscap-wazuh/report-d9.html) :
![Open Scap Report Debian 9](/blog/openscap-wazuh/oscap-report.png)

Pour chaque check en écart, des remédiations sont proposées via un script shell, Puppet ou encore Ansible ; exemple ici pour l'installation du démon syslog-ng :
![OpenScap Remediation](/blog/openscap-wazuh/remediation.png)

Une fois sécurisé, le rapport pourra toujours afficher des faux positifs qu'il conviendra de justifier. Exemple sur [sur une Debian 12](/blog/openscap-wazuh/report-d12.html) ; normal dans ce cas, nous ne sommes pas dans la situation où le serveur reçoit des logs d'autres serveurs.

## Wazuh

### Waz is ist ?

Wazuh est une plateforme open source complète utilisée pour la prévention, la détection et la réponse aux menaces. Elle sécurise tout type d'environnements qu'ils soient virtualisés ou conteneurisés, en cloud ou non. Wazuh est largement utilisé par des milliers d’organisations à travers le monde, de la petite entreprise à la grande entreprise et si elle est vraiment déconcertante tellement elle est [simple à installer](https://documentation.wazuh.com/current/installation-guide/index.html), elle fournit nombre d'information qu'il conviendra d'avoir les moyens d'exploiter.

Wazuh se déploie comme Agent sur chaque environnement Windows, Linux, MacOS, HP-UX, Solaris et AIX de l'infra, qu'il s'agisse d'une workstation, d'un serveur conteneurisé ou non. Cet agent va collecte les informations et les remonter à un central.

Ce noeud central, le serveur de gestion, collecte et analyse les données recueillies par les agents. Il traite ces données au moyen de décodeurs et de règles, et utilise le renseignement sur les menaces pour rechercher des indicateurs de compromission bien connus (IOC). Un seul serveur peut analyser les données de centaines ou de milliers d’agents, et si ça ne suffit toujours pas il est scalable horizontalement (configuration en grappe), et il va également gérer les agents (application de configuration et mise à jour à distance si nécessaire).

Wazuh a entièrement intégré à Elastic Stack, fournissant ainsi un moteur de recherche et un outil de visualisation des données (dashboard) qui permet aux utilisateurs de naviguer à travers leurs alertes de sécurité. Ses princiaples fonctons sont les suivantes :

- Analyse de la sécurité globale
- Détection d’intrusion
- Analyse des données du journal (type de ce que fait SPLUNK)
- Contrôle de l’intégrité des fichiers
- Détection des vulnérabilités
- Evaluation des (mauvaises !) configurations

Exemple ci-dessous avec deux agents déployés sur mes 2 VM Debian 9 (non sécurisée par OpenSCAP) et Debian 12 (sécurisée) :
![Wazuh dashboard](/blog/openscap-wazuh/Wazuh-dashboard.png 'Dashboard Wazuh')

Et l'analyse de l'agent Debian 9 :
![Wazuh dashboard](/blog/openscap-wazuh/Wazuh-agentd9.png 'Dashboard Agent Debian 9')

L'intégration d'OpenScap Wazuh était possible, mais le [passage en v4](https://documentation.wazuh.com/4.0/release-notes/release_4_0_0.html#breaking-changes) a enlevé ce support et supprimé [le script python](https://github.com/wazuh/wazuh/tree/4.0/wodles/oscap) qui permettait de l'exploiter. Wazuh recommande désormais d'utiliser SCA à sa place, SCA étant leur solution opensource de durcissement... Exemple sur ma VM Debian sur laquelle j'ai appliqué le profil "ANSSI High"

![Wazuh SCA](/blog/openscap-wazuh/Wazuh-sca.png)
![Wazuh SCA2](/blog/openscap-wazuh/Wazuh-sca2.png)

La policy du profil employé par défaut est décrit dans `/var/ossec/ruleset/scan/cis_debian10.yml`. Tout fichier rajouté dans ce dossier sera pris en compte, aussi il est possible d'y implémenter [sa propre PSSI](https://wazuh.com/blog/security-configuration-assessment/).

A la lecture du résultat, on pourrait s'interroger sur la complétude du profil employé avec OpenScap ... En effet :

- en ouvrant le workbench, on pourra se rendre compte que nombre de règles n'ont pas été activées
- en utilisant la dernière version des fichiers OVAL delivrée dans le dépôt Compliance As Code, de nouveaux check sont implémentés

Référentiels :

- [Compliance As Code](https://github.com/ComplianceAsCode/content)
- [Debian](https://www.debian.org/security/oval/)
- [RedHat](https://ubuntu.com/security/oval)
